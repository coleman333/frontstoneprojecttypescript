import { createAction } from 'redux-actions';
import { createActionAsync } from 'redux-act-async';
import axios from 'axios';
import { getCookie, setCookie } from 'app/utils';
import * as _ from 'lodash';

let timer: any = null;

export namespace AuthActions {
	export const refreshTries = createAction('REFRESH_TRIES');

	export const login = createActionAsync('LOGIN', (data: any) => {
		return axios({
			method: 'post',
			url: 'http://194.87.147.5:8100/api/v1/admin/users/auth/',
			data: data
		})

		// return Promise.reject('error')
			.then(response => {

				setCookie('triesLeft', '', {
					expires: -1
				});

				return response;
			})
			.catch((response) => {

				let triesLeft = +_.defaultTo(getCookie('triesLeft'), 5);
				if (triesLeft > 0) {
					triesLeft--;
					setCookie('triesLeft', triesLeft.toString(), {
						expires: 60 * 5
					});
				}

				throw response;
			});
	}, {
		noRethrow: true,
		ok: {
			callback: (dispatch: any, getState: any) => {
				if (!!timer) {
					clearTimeout(timer);
					timer = null;
				}

			}
		},
		error: {
			callback: (dispatch: any, getState: any) => {
				if (!!timer) {
					clearTimeout(timer);
				}
				timer = setTimeout(() => {
					dispatch(refreshTries());
					timer = null;
				}, 1000 * 60 * 5)
			},
		}
	});
}
