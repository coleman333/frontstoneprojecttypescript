// import { createAction } from 'redux-actions';
import { createActionAsync } from 'redux-act-async';
import axios from 'axios';

export namespace BasketActions {
	export const getAllBasket = createActionAsync('GET_ALL_BASKET', (filter: string, limit: number, offset: number) => {
		return axios({
			method: 'get',
			url: `https://jsonplaceholder.typicode.com/users?filter=${filter}&limit=${limit}&offset=${offset}`,
		});
	}, { noRethrow: true, });

	export const fetchPartners = createActionAsync('GET_PARTNERS_IN_THE_BASKET', () => {
		return axios({
			method: 'get',
			url: `https://jsonplaceholder.typicode.com/users`,
		});
	}, { noRethrow: true, });
	export const deleteAllBasket = createActionAsync('DELETE_ALL_BASKET',() =>{
		return axios({
			method:'get',
			url:`https://jsonplaceholder.typicode.com/users`
		})
	})

}
