// import { createAction } from 'redux-actions';
import { createActionAsync } from 'redux-act-async';
import axios from 'axios';

export namespace PartnersActions {
	export const create = createActionAsync('ADD_PARTNER', (data: any) => {
		return axios({
			method: 'get',
			url: 'https://jsonplaceholder.typicode.com/users',
			data: data
		});
	}, { noRethrow: true, });

	export const addNewUser = createActionAsync('ADD_PARTNERS_USER', (data: any) => {
		// let user=new FormData();
		// user.append('logo',data.files);
		// user.append('name',data.name);
		// user.append('role',data.role);
		// user.append('phone',data.phone);
		// user.append('email',data.email);
		return axios({
			method: 'get',
			url: 'https://jsonplaceholder.typicode.com/users',
			data: data
		});
	}, { noRethrow: true, });

	export const editNewUser = createActionAsync('EDIT_PARTNERS_USER', (data: any) => {
		// let user=new FormData();
		// user.append('logo',data.files);
		// user.append('name',data.name);
		// user.append('role',data.role);
		// user.append('phone',data.phone);
		// user.append('email',data.email);
		return axios({
			method: 'get',
			url: 'https://jsonplaceholder.typicode.com/users',
			data: data
		});
	}, { noRethrow: true, });
}
