import * as React from "react";
// import  './style.scss';
import { connect } from "react-redux";
import { Dispatch } from "redux";
// bindActionCreators,
import { RouteComponentProps } from "react-router";
// , Link
import { RootState } from "app/reducers/index";
import * as _ from 'lodash';

// import MainTemplate from '../MainTemplate';

// const { TextField, MenuItem, SelectField } = require("material-ui");

//const $ = require("jquery");

// const supportIcon = require("app/../assets/images/support.svg") as string;
// const editIcon = require("app/../assets/images/icon_edit.svg") as string;
// const editProfileIcon = require("app/../assets/images/icon_profile_edit.svg") as string;
// const addPhotoIcon = require("app/../assets/images/icon_add_photo.svg") as string;
// const deleteUserIcon = require("app/../assets/images/icon_delete.svg") as string;
// const passwdEditIcon = require("app/../assets/images/icon_password_edit.svg") as string;

// const officeAvatar = require("app/../assets/images/officeAvatar.jpg") as string;
// const searchBlue = require("app/../assets/images/searchBlue.svg") as string;
// const adminRoleIcon = require("app/../assets/images/adminRole.svg") as string;
const hershyesIcon = require("app/../assets/images/hershes.png") as string;
const recycleIconWhite = require("app/../assets/images/recycle_icon_white.svg") as string;
const closeCrossBlack = require("app/../assets/images/close_button_search_black.svg") as string;


namespace PartnerSidePanelBasket {
	export interface Props extends RouteComponentProps<any, any> {

	}
}

@connect(
	(state: RootState): Pick<PartnerSidePanelBasket.Props, any> => {
		return {};
	},
	(dispatch: Dispatch<any, any>): Pick<PartnerSidePanelBasket.Props, any> => ({})
)

class PartnerSidePanelBasket extends React.Component<any, any> {
	static defaultProps: Partial<PartnerSidePanelBasket.Props> = {};

	constructor(props: PartnerSidePanelBasket.Props, context?: any) {
		super(props, context);
	}

	state = {
		// openEditWindow:false
		value: 3,
		// 7.2_Partners_User_Edit
		user: {
			firstName: "Дмитрий",
			secondName: "Заруба",
			role: "superAdmin",
			phone: "+7 902 563 50 45",
			email: "dzaruba@gmail.com"
		},
		searchEnableField: false,
	};


	onClose() {
		if (_.isFunction(this.props.onClose)) {
			this.props.onClose(...arguments);
		}
	}

	render() {
		const { item, open } = this.props;

		return <div className={`inner-panel ${open ? 'show' : 'hide'}`}>
			<div className="inner-top">
				<div>
					<img src={closeCrossBlack} style={{ width: '25px' }}
							 {...{ onTouchTap: this.onClose.bind(this , false) }} alt=""
					/>
				</div>
			</div>
			<div className="inner-profile">
				<div style={{ display: 'flex' }}>
					<div style={{ flex: 1, flexDirection: 'column', justifyContent: "start", marginLeft: '81px' }}>
						<div className="labelForTitleSidePanel">
							<span>Тип профиля удаленного обьекта</span>
						</div>
						<div className="textForSidePanel">
							<span>{item.organizationName}</span>
						</div>
						<div style={{ marginTop: "21px" }}>
							<div className="labelForTitleSidePanel">
								<span>Дата удаления</span>
							</div>
							<div className="textForSidePanel">
								<span> {item.date}</span>
							</div>
						</div>
					</div>
				</div>
				<div>
					<div className="divButtonSuccess">
						<button className="btn-xs btn-success buttonSuccess">
							<img src={recycleIconWhite} style={{ width: '24px' }} alt=""/><span>Восстановить</span></button>
					</div>
				</div>
			</div>

			<div className="blackLineSidePanel">
				<span>Детально</span>
			</div>

			<div className="inner-content">
				{/*<div className="blackLineSidePanel">*/}
				{/*<span style={{}}>Детально</span>*/}
				{/*</div>*/}
				<div style={{ display: 'flex', marginLeft: '66px', marginTop: '49px' }}>

					<div style={{ flex: 1, }}>
						<img src={hershyesIcon} style={{ width: '104px' }} alt=""/>
						{/*style={{width:'15%',marginLeft:'8%',marginTop:"10%"}}*/}
					</div>
					<div style={{ flex: 3, }}>
						<div>
							<div className="labelForTitleSidePanel">
								<span>Наименование организации</span>
							</div>
							<div className="textForSidePanel">
								<span>{item.organizationName}</span>
							</div>
						</div>
						<div style={{ marginTop: "21px" }}>
							<div className="labelForTitleSidePanel">
								<span>ФИО представителя</span>
							</div>
							<div className="textForSidePanel">
								<span>nameFromRequest</span>
							</div>
						</div>
						<div style={{ marginTop: "21px" }}>
							<div className="labelForTitleSidePanel">
								<span>E-mail</span>
							</div>
							<div className="textForSidePanel">
								<span>emailFromRequest</span>
							</div>
						</div>
						<div style={{ marginTop: "21px" }}>
							<div className="labelForTitleSidePanel">
								<span>Контактный номер телефона</span>
							</div>
							<div className="textForSidePanel">
								<span>phoneFromRequest</span>
							</div>
						</div>
					</div>

				</div>
				<hr style={{ marginTop: '59px' }}/>
				<div style={{ marginLeft: '66px', marginBottom: '31px' }}>
					<div>
						<div className="labelForTitleSidePanel" style={{}}>
							<span>Город головного офиса</span>
						</div>
						<div className="textForSidePanel">
							<span>townFromRequest</span>
						</div>
					</div>
					<div style={{ marginTop: '46px' }}>
						<div className="labelForTitleSidePanel">
							<span>Бонусный счет</span>
						</div>
						<div className="textForSidePanel">
							<span>bonusFromRequest</span>
						</div>
					</div>
					<div style={{ marginTop: '46px' }}>
						<div className="labelForTitleSidePanel">
							<span>Дата создания профиля</span>
						</div>
						<div className="textForSidePanel">
							<span>dateFromRequest</span>
						</div>
					</div>
				</div>

			</div>
		</div>
	}

}

export default PartnerSidePanelBasket;