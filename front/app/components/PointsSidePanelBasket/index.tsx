import * as React from "react";
// import  './style.scss';
import { connect } from "react-redux";
import { Dispatch } from "redux";
// bindActionCreators,
import { RouteComponentProps } from "react-router";
// , Link
import { RootState } from "app/reducers/index";
import * as _ from 'lodash';

// import MainTemplate from '../MainTemplate';

// const { TextField, MenuItem, SelectField } = require("material-ui");

//const $ = require("jquery");

// const supportIcon = require("app/../assets/images/support.svg") as string;
// const editIcon = require("app/../assets/images/icon_edit.svg") as string;
// const editProfileIcon = require("app/../assets/images/icon_profile_edit.svg") as string;
// const addPhotoIcon = require("app/../assets/images/icon_add_photo.svg") as string;
// const deleteUserIcon = require("app/../assets/images/icon_delete.svg") as string;
// const passwdEditIcon = require("app/../assets/images/icon_password_edit.svg") as string;

// const officeAvatar = require("app/../assets/images/officeAvatar.jpg") as string;
// const searchBlue = require("app/../assets/images/searchBlue.svg") as string;
// const adminRoleIcon = require("app/../assets/images/adminRole.svg") as string;
const hershyesIcon = require("app/../assets/images/hershes.png") as string;
const recycleIconWhite = require("app/../assets/images/recycle_icon_white.svg") as string;
const closeCrossBlack = require("app/../assets/images/close_button_search_black.svg") as string;
const adminRoleIcon = require("app/../assets/images/adminRole.svg") as string;


namespace PartnerSidePanelBasket {
	export interface Props extends RouteComponentProps<any, any> {

	}
}

@connect(
	(state: RootState): Pick<PartnerSidePanelBasket.Props, any> => {
		return {};
	},
	(dispatch: Dispatch<any, any>): Pick<PartnerSidePanelBasket.Props, any> => ({})
)

class PartnerSidePanelBasket extends React.Component<any, any> {
	static defaultProps: Partial<PartnerSidePanelBasket.Props> = {};

	constructor(props: PartnerSidePanelBasket.Props, context?: any) {
		super(props, context);
	}

	state = {
		// openEditWindow:false
		value: 3,
		// 7.2_Partners_User_Edit
		user: {
			firstName: "Дмитрий",
			secondName: "Заруба",
			role: "superAdmin",
			phone: "+7 902 563 50 45",
			email: "dzaruba@gmail.com"
		},
		searchEnableField: false,
	};


	onClose() {
		if (_.isFunction(this.props.onClose)) {
			this.props.onClose(...arguments);
		}
	}

	render() {
		const { item, open } = this.props;

		return <div className={`inner-panel ${open ? 'show' : 'hide'}`}>
      <div className="inner-top">
         		<div>
         			<img src={closeCrossBlack} style={{ width: '25px' }}
                   {...{ onTouchTap: this.onClose.bind(this , false) }} alt=""
							/>
						</div>
					</div>
					<div className="inner-profile">
						<div style={{ display: 'flex' }}>
							<div style={{ flex: 1, flexDirection: 'column', justifyContent: "start", marginLeft: '81px' }}>
								<div className="labelForTitleSidePanel">
									<span>Тип профиля удаленного обьекта</span>
								</div>
								<div className="textForSidePanel">
									<span>{item.organizationName}</span>
								</div>
								<div style={{ marginTop: "21px" }}>
									<div className="labelForTitleSidePanel">
										<span>Дата удаления</span>
									</div>
									<div className="textForSidePanel">
										<span> {item.date}</span>
									</div>
								</div>
							</div>
						</div>
						<div>
							<div className="divButtonSuccess">
								<button className="btn-xs btn-success buttonSuccess">
									<img src={recycleIconWhite} style={{ width: '24px' }} alt=""/><span>Восстановить</span></button>
							</div>
						</div>
					</div>

					<div className="blackLineSidePanel">
						<span>Детально</span>
					</div>
					<div className="">
						<div style={{ display: 'flex', marginLeft: '80px', marginTop: '55px' }}>
							<div style={{ display: 'flex' }}>
								<div>
									<div>
										<img src={hershyesIcon} alt="" style={{ width: '144px' }}/>
									</div>
									<div className="textForSidePanel" style={{ marginTop: '24px' }}>
										<span>{item.organizationName}</span>
									</div>
								</div>

								<div style={{ marginLeft: '32px' }}>
									<div>
										<div className="labelForTitleSidePanel">
											<span>Название</span>
										</div>
										<div className="textForSidePanel">
											<span> hersheys на Прохорова</span>
										</div>
									</div>
									<div>
										<div className="labelForTitleSidePanel">
											<span>Город</span>
										</div>
										<div className="textForSidePanel">
											<span> Казань</span>
										</div>
									</div>
									<div>
										<div className="labelForTitleSidePanel">
											<span>Адрес</span>
										</div>
										<div className="textForSidePanel">
											<span> Прохорова 49</span>
										</div>
									</div>
								</div>

								<div style={{ marginLeft: '100px' }}>
									<div>
										<div className="labelForTitleSidePanel">
											<span>администратор</span>
										</div>
										<div className="textForSidePanel">
											<img src={adminRoleIcon} alt="" style={{ width: '15px' }}/>
											<span> Михаил Дача</span>
										</div>
									</div>
									<div>
										<div className="labelForTitleSidePanel">
											<span>Дата регистрации в системе</span>
										</div>
										<div className="textForSidePanel">
											<span>{item.date}</span>
										</div>
									</div>
								</div>
							</div>
							<div>

							</div>

						</div>
					</div>

				</div>}

}

export default PartnerSidePanelBasket;