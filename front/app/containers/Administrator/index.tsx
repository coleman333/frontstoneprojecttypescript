import * as React from 'react';
// import * as style from './style.scss';
import { connect } from 'react-redux';
import { Dispatch } from 'redux';
// bindActionCreators,
import { RouteComponentProps, browserHistory ,Link} from 'react-router';
import { RootState } from 'app/reducers';
import MainTemplate from '../MainTemplate';
//const $ = require("jquery");
// declare var $ : any;
// window = require('jquery')(window);

// const $ = require('jquery')(require("jsdom").jsdom().parentWindow);

const { Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn } = require("material-ui/Table");
const { TextField, MenuItem, SelectField } = require("material-ui");


// const addPhotoIcon = require("app/../assets/images/icon_add_photo.svg") as string;
const deleteUserIcon = require("app/../assets/images/icon_delete.svg") as string;
// const passwdEditIcon = require("app/../assets/images/icon_password_edit.svg") as string;
// const supportIcon = require("app/../assets/images/support.svg") as string;
const settingsIcon = require("app/../assets/images/icon_more.svg") as string;
// const markerList = require("app/../assets/images/icon_add_photo.svg") as string;
// const arrowDownIcon = require( 'app/../images/arrow down.svg') as string;
const plusIcon = require("app/../assets/images/icon_plus.svg") as string;
// const grayViewListIcon = require("app/../assets/images/view-list.svg") as string;
// const blueViewTableIcon = require("app/../assets/images/view-table.svg") as string;
// const blueViewListIcon = require("app/../assets/images/view-list-blue.svg") as string;
// const grayViewTableIcon = require("app/../assets/images/view-table-gray.svg") as string;
const buttonDelete = require("app/../assets/images/button_delete.svg") as string;
const buttonEdit = require("app/../assets/images/button_edit.svg") as string;
// const photoBox = require("app/../assets/images/photo_box.svg") as string;
const officeAvatar = require('app/../assets/images/officeAvatar.jpg') as string;
const partnersIcon = require("app/../assets/images/icon_partners.svg") as string;
const partnersEditIcon = require("app/../assets/images/icon_parnter_edit.svg") as string;
const searchBlue = require("app/../assets/images/searchBlue.svg") as string;


namespace Administrator {
	export interface Props extends RouteComponentProps<any, any> {
	}
}

@connect(
	(state: RootState): Pick<Administrator.Props, any> => {
		return {};
	},
	(dispatch: Dispatch<any, any>): Pick<Administrator.Props, any> => ({})
)

class Administrator extends MainTemplate<Administrator.Props, any> {
	static defaultProps: Partial<Administrator.Props> = {};

	state = {
		showCheckboxes: false,
		height: '100%',
		selectable: false,
		slideIndex: 0,
		toggleToEditOrDeleteFlagId: -1,
		partners: [
			{
				id: 1,
				name: 'Кравцов Михаил Сергеевич',
				partner: 'partner1',
				email: 'example1@example.com',
				phone: '123 456 789',
			},
			{
				id: 2,
				name: 'Полюшина Зинаида Александровна',
				partner: 'partner2',
				email: 'example2@example.com',
				phone: '123 456 789',
			},
			{
				id: 3,
				name: 'Астахова Регина Иванова',
				partner: 'partner3',
				email: 'example3@example.com',
				phone: '123 456 789',
			},
			{
				id: 4,
				name: 'Зарин Андрей Максимович',
				partner: 'partner4',
				email: 'example4@example.com',
				phone: '123 456 789',
			},
			{
				id: 5,
				name: 'Зарин Андрей Максимович',
				partner: 'partner5',
				email: 'example5@example.com',
				phone: '123 456 789',
			},
			{
				id: 6,
				name: 'Зарин Андрей Максимович',
				partner: 'partner6',
				email: 'example6@example.com',
				phone: '123 456 789',
			},
			{
				id: 7,
				name: 'Зарин Андрей Максимович',
				partner: 'partner7',
				email: 'example7@example.com',
				phone: '123 456 789',
			},
			{
				id: 8,
				name: 'Зарин Андрей Максимович',
				partner: 'partner4',
				email: 'example4@example.com',
				phone: '123 456 789',
			},
			{
				id: 9,
				name: 'Зарин Андрей Максимович',
				partner: 'partner5',
				email: 'example5@example.com',
				phone: '123 456 789',
			},
			{
				id: 10,
				name: 'Зарин Андрей Максимович',
				partner: 'partner6',
				email: 'example6@example.com',
				phone: '123 456 789',
			}, {
				id: 11,
				name: 'Зарин Андрей Максимович',
				partner: 'partner7',
				email: 'example7@example.com',
				phone: '123 456 789',
			},
		],
		selectValueRoleUser: 2,
		userRoleFromSelect: '',
		userForModal: {
			id: "",
			name: "",
			partner: "",
			email: "",
			phone: ""
		},
		user: {
			firstName: 'Дмитрий',
			secondName: 'Заруба',
			role: 'superAdmin',
			phone: '+7 902 563 50 45',
			email: 'dzaruba@gmail.com',
			avatar: '../../images/officeAvatar.jpg'
		},
		selectedPartner: "",
		// cities:[
		// 	'Москва','Санкт-Питербург','Казань','чсебоксары','Йош-Карола'
		// ],
		value: 1,
		searchEnableField: false

	};

	constructor(props: Administrator.Props, context?: any) {
		super(props, context);
	}

	openAddPartnerWindow(event: any) {
		$("#addUserModal").modal({ show: true });
		// $.noConflict();
	}

	openEditProfileWindow(user: any) {
		this.setState({ userForModal: user })
		// this.state.userForModal=user;
		$("#editUserModal").modal({ show: true });

		console.log(this.state.userForModal);
	}

	openChangePasswdProfileWindow() {
		$("#changePasswordModal").modal({ show: true });
	}

	openDeleteUserProfileWindow(user: any) {
		this.setState({ userForModal: user })
		$("#deleteUserModal").modal({ show: true });
		// $('#editUserModal').modal({ hide: true });
		$("#editUserModal").modal("hide");
	}

	toggleToEditOrDelete(index: number, ev: any) {
		if (ev) {
			ev.preventDefault();
			ev.stopPropagation();
		}
		if (this.state.toggleToEditOrDeleteFlagId === index) {
			index = -1;
		}
		//console.log(12343546657, this.state.toggleToEditOrDeleteFlagId);
		this.setState({ toggleToEditOrDeleteFlagId: index });

	}

	onChangePartnerSelect(ev: any) {
		let partner = ev.target.value;
		this.setState({ selectedPartner: partner });

	}

	handleChange = (event: any, index: any, value: any) => this.setState({ value });

	uploadFile() {

	}

	redirectToProfile() {
		browserHistory.push('/profile');
	}

	submitAddNewUser() {
		// console.log(1234, (this.refs.name as any).input.value);
		const newParner: any = {
			name: (this.refs.name as any).input.value,
			partner: (this.refs.partner as any).input.value, //here is select
			phone: (this.refs.phone as any).input.value,
			email: (this.refs.email as any).input.value

		};
		// console.log('sdfgsdfgsdfg', newParner);
		$("#addUserModal").modal("hide");

		console.log(123434563456, newParner);
	}

	submitEditNewUser() {

		const newParner: any = {

			name: (this.refs.name as any).input.value,
			partner: (this.refs.partner as any).input.value,    //here might be the select
			phone: (this.refs.phone as any).input.value,
			email: (this.refs.email as any).input.value

		};
		$("#editUserModal").modal("hide");

		console.log(123434563456, newParner);
	}

	searchEnableFieldMethod() {
		this.setState({ searchEnableField: true });
	}

	onBlurSearchField() {
		this.setState({ searchEnableField: false })
	}


	renderHeader() {
		return	<div >

						<div className={'header-top'}>
							<div className={'search-bar'}>
								<img src={searchBlue} className={'search-img'} alt=""
										 {...{ onTouchTap: this.searchEnableFieldMethod.bind(this) }}
								/>
								{
									this.state.searchEnableField && <TextField
										name={'headerSearch'}
										className={'search-text'}
										hintText=""
										placeholder={'Поиск...'}
										fullWidth={true}
										ref="search"
										onBlur={this.onBlurSearchField.bind(this)}
										autoFocus={true}
									/>
								}
							</div>

							<Link to="/profile">
								<img className="avatar" src={officeAvatar} alt=""/>
							</Link>
						</div>

			<div className={'header-middle'}>
				<div style={{ display: "flex", alignItems: "center", color: "#5a5a5a" }}>
					Город:
				</div>
				<select
					className="form-control" id="exampleFormControlSelect1"
					style={{ width: "20%", borderRadius: "20px" }}
					onChange={this.onChangePartnerSelect.bind(this)}
				>
					<option value="">Все города</option>
					{
						this.state.partners.map((item, index) => {
							return <option value={item.partner} key={index}>{item.partner}</option>;
						})
					}
				</select>

				<button className="btn-xs btn-primary"
								{...{ onTouchTap: this.openAddPartnerWindow.bind(this) }}
								style={{ borderRadius: "20px" ,width:'20%'}}
				>
					<img src={plusIcon} style={{ width: "25px" }} alt=""/>
					Добавить
				</button>
			</div>

						<div className={'header-bottom'}>
							<h2 className={'title'}>Администраторы</h2>
						</div>
				</div>
	}

	renderContent() {
		let partners = this.state.partners;

		if (this.state.selectedPartner) {
			partners = partners.filter(p => p.partner === this.state.selectedPartner)
		}

		return <div>
			<div className="row"
					 style={{ flex: 1, minHeight: 'calc(100% - 220px)', background: 'white' }}
				// style={{flexDirection: 'column', justifyContent: 'spaceBetween', overflow: 'auto'}}
			>
				<div className="col-lg-12"
						 style={{ backgroundColor: "white", height: '100%' }}
					// style={{backgroundColor: "white", minHeight: 'calc(100% - 203px)'}}
				>
					<div>
						<Table height={this.state.height} selectable={this.state.selectable}>
							<TableHeader displaySelectAll={this.state.showCheckboxes}
													 adjustForCheckbox={this.state.showCheckboxes}
							>
								<TableRow>
									<TableHeaderColumn>№</TableHeaderColumn>
									<TableHeaderColumn>Фамилия Имя Отчество</TableHeaderColumn>
									<TableHeaderColumn>Партнер</TableHeaderColumn>
									<TableHeaderColumn>E-mail</TableHeaderColumn>
									<TableHeaderColumn>Телефон</TableHeaderColumn>
									<TableHeaderColumn></TableHeaderColumn>
								</TableRow>
							</TableHeader>
							<TableBody displayRowCheckbox={this.state.showCheckboxes}
							>
								{partners.map((item, index) => {
									return <TableRow key={index} className="tableOnHover" style={{ height: '80px' }}>
										<TableRowColumn>{index + 1}</TableRowColumn>
										<TableRowColumn>{item.name}</TableRowColumn>
										<TableRowColumn>{item.partner}</TableRowColumn>
										<TableRowColumn>{item.email}</TableRowColumn>

										<TableRowColumn>
											{/*<img src={markerList} alt=""*/}
											{/*style={{width: '5px', marginRight: '3%'}}/>*/}
											{item.phone}
										</TableRowColumn>
										{this.state.toggleToEditOrDeleteFlagId !== index && <TableRowColumn>
											<img src={settingsIcon} alt=""
													 onClick={this.toggleToEditOrDelete.bind(this, index)}
													 style={{ width: '10%', marginLeft: '90%' }}
											/></TableRowColumn>}

										{this.state.toggleToEditOrDeleteFlagId === index && <TableRowColumn>
											<div style={{ width: '100%' }}>
												<img src={buttonEdit} alt=""
														 onClick={this.openEditProfileWindow.bind(this, item)}
														 style={{ marginLeft: '40%', width: '30%' }}
												/>
												<img src={buttonDelete} alt="" style={{ width: '30%' }}
														 onClick={this.openDeleteUserProfileWindow.bind(this, item)}
												/>
											</div>
										</TableRowColumn>}
									</TableRow>
								})}


							</TableBody>
						</Table>


					</div>
				</div>
			</div>


			<div {...{ className: "modal fade bootstrapWindows modalWindow" }as any} id="editUserModal" tabIndex="-1"
					 role="dialog"
					 aria-labelledby="editUserModalLabel" aria-hidden="true"
					 style={{ marginLeft: '40%', marginTop: '8%' }}>
				<div className="modal-dialog" role="document">
					<div className="modal-content" style={{ borderRadius: '20px' }}>
						<div className="modal-header">
							<img src={partnersEditIcon} alt="" style={{ width: '10%', marginTop: '10px' }}/>
							<h3 className="modal-title" id="editUserModalLabel"
									style={{ marginLeft: '5px', fontSize: '100%' }}>
								Редактировать <br/>данные</h3>

							<button type="button" className="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<div className="modal-body">
							<div>
								{/*<img className="imageClass" src={this.state.userForModal.logo} alt="" style={{*/}
								{/*width: '70px',*/}
								{/*height: '70px',*/}
								{/*borderRadius: '50%',*/}
								{/*backgroundSize: 'auto 70px',*/}
								{/*marginLeft: '7%',*/}
								{/*marginTop: '2%'*/}
								{/*}}/>*/}
								{/*<img src={addPhotoIcon} alt=""*/}
								{/*style={{marginLeft:'-4%',width:'6%',marginTop:'-10%',cursor:'pointer'}}*/}
								{/*onTouchTap={this.changeAvatar.bind(this)}/>*/}
								{/*<FloatingActionButton mini={true} secondary={true}*/}
								{/*style={{marginLeft:'-4%',width:'6%',marginTop:'-10%'}}>*/}
								{/*/!*<img src={addPhotoIcon} style={{width:'25px'}} alt=""/>*!/*/}
								{/*<ContentAdd />*/}
								{/*<input type="file" onChange={this.uploadFile.bind(this)}/>*/}

								{/*</FloatingActionButton>*/}
								{/*<label htmlFor="label123" style={{*/}
								{/*marginLeft: '-4%',*/}
								{/*width: '6%',*/}
								{/*marginTop: '10%',*/}
								{/*cursor: 'pointer'*/}
								{/*}}>*/}
								{/*<img src={addPhotoIcon} style={{width: '35px'}} alt=""/>*/}
								{/*<input type="file" id="label123" style={{display: 'none'}}*/}
								{/*onChange={this.uploadFile.bind(this)}/>*/}
								{/*</label>*/}
								<br/>
								<div className="offset-lg-5 col-lg-7"{...{ onTouchTap: this.openDeleteUserProfileWindow.bind(this) }}>
									<img src={deleteUserIcon} alt=""
											 style={{ width: '20%', cursor: 'pointer' }}/>
									<span style={{ fontSize: '50%', color: '#e6144a', cursor: 'pointer' }}>удалить администратора</span>
								</div>
							</div>
							<div style={{ marginLeft: '7%', marginRight: '7%', marginTop: '1%' }}>
								<TextField
									hintText={this.state.userForModal.name}
									floatingLabelText="Фамилия Имя Отчество"
									defaultValue={this.state.userForModal.name}
									fullWidth={true}
									ref="name"
								/>

								<SelectField
									floatingLabelText="Партнер"
									value={this.state.value}
									onChange={this.handleChange}
									fullWidth={true}
								>{partners.map((item, index) => {
									return <MenuItem value={index} key={index} primaryText={item.partner}/>
								})}

								</SelectField>
								<TextField
									hintText={this.state.userForModal.phone}
									floatingLabelText="Телефон..."
									fullWidth={true}
									ref="phone"
								/>
								<TextField
									hintText={this.state.userForModal.email}
									floatingLabelText="Введите E-mail..."
									fullWidth={true}
									ref="email"
								/>

								<button type="button" style={{ borderRadius: '20px', marginLeft: '75%', fontSize: '80%' }}
												className="btn-xs btn-primary"  {...{ onTouchTap: this.submitEditNewUser.bind(this) }} >
									<span className="fontButtonWindow">Сохранить</span>
								</button>
							</div>
						</div>
						{/*<div className="modal-footer">*/}
						{/*<button type="button" style={{borderRadius:'20px',width:'30%'}} className="btn btn-primary">*/}
						{/*Сохранить</button>*/}
						{/*</div>*/}
					</div>
				</div>
			</div>

			<div {...{ className: "modal fade bootstrapWindows modalWindow" }as any} id="addUserModal" tabIndex="-1"
					 role="dialog"
					 aria-labelledby="addUserModalLabel" aria-hidden="true"
					 style={{ marginLeft: '40%', marginTop: '8%' }}>
				<div className="modal-dialog" role="document">
					<div className="modal-content" style={{ borderRadius: '20px' }}>
						<div className="modal-header">
							<img src={partnersIcon} alt="" style={{ width: '10%', marginTop: '10px' }}/>
							<h3 className="modal-title" id="addUserModalLabel"
									style={{ marginLeft: '5px', fontSize: '90%' }}>
								Добавить<br/>администратора</h3>

							<button type="button" className="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<div className="modal-body">
							{/*<div style={{display:'flex',flexDirection:'row'}}>*/}
							{/*<img className="imageClass" src={photoBox} alt="" style={{*/}
							{/*width: '70px',*/}
							{/*height: '70px',*/}
							{/*borderRadius: '50%',*/}
							{/*backgroundSize: 'auto 70px',*/}
							{/*marginLeft: '7%',*/}
							{/*marginTop: '2%'*/}
							{/*}}/>*/}

							{/*<label htmlFor="label123" style={{*/}
							{/*marginLeft: '-4%',*/}
							{/*width: '6%',*/}
							{/*marginTop: '10%',*/}
							{/*cursor: 'pointer'*/}
							{/*}}>*/}

							{/*<img src={markerList} style={{width: '35px',marginTop:'-305%'}} alt=""/>*/}
							{/*<input type="file" id="label123" style={{display: 'none'}}*/}
							{/*onChange={this.uploadFile.bind(this)}/>*/}
							{/*</label>*/}
							{/*<div style={{marginTop:'4%'}}>*/}
							{/*<span style={{marginLeft: '15%',color: '#8c96a0',fontSize:'80%'}}>*/}
							{/*Логотип</span><br/>*/}
							{/*<span style={{marginLeft: '15%',color: '#8c96a0',fontSize:'60%'}}>*/}
							{/*В формате PNG или JPG, не более 5Мб </span>*/}
							{/*</div>*/}

							{/*</div>*/}
							<div style={{ marginLeft: '7%', marginRight: '7%', marginTop: '1%' }}>
								<TextField
									hintText=""
									floatingLabelText="фамилия имя отчество"
									fullWidth={true}
									ref="adminName"
								/>
								{/*<SelectField*/}
								{/*floatingLabelText="Город"*/}
								{/*value={this.state.value}*/}
								{/*onChange={this.handleChange}*/}
								{/*fullWidth="true"*/}
								{/*>*/}
								{/*{this.state.cities.map((item:any,index:number)=>{*/}
								{/*return<MenuItem value={index} primaryText={item} key={index}/>*/}
								{/*})	}*/}
								{/*</SelectField>*/}
								<SelectField
									floatingLabelText="Партнер"
									value={this.state.value}
									onChange={this.handleChange}
									fullWidth={true}
								>
									{partners.map((item, index) => {
										return <MenuItem value={index} key={index} primaryText={item.partner}/>
									})}
								</SelectField>
								{/*<SelectField*/}
								{/*floatingLabelText="Роль"*/}
								{/*value={this.state.value}*/}
								{/*onChange={this.handleChange}*/}
								{/*fullWidth={true}*/}
								{/*>*/}
								{/*<MenuItem value={1} primaryText="admin"/>*/}
								{/*<MenuItem value={2} primaryText="user"/>*/}
								{/*</SelectField>*/}
								<TextField
									hintText="Введите номер мобильго телефона..."
									floatingLabelText="Телефон"
									// floatingLabelText="Введите номер мобильго телефона"
									fullWidth={true}
									ref="phone"
								/>
								<TextField
									hintText="Введите E-mail..."
									floatingLabelText="E-mail"
									fullWidth={true}
									ref="email"
								/>

								<button type="button"
												style={{ borderRadius: '20px', width: '40%', marginLeft: '60%', marginTop: '10%' }}
												{...{ onTouchTap: this.submitAddNewUser.bind(this) }}
												className="btn-xs btn-primary">
									<span className="fontButtonWindow">Сохранить</span>
								</button>
							</div>
						</div>
						{/*<div className="modal-footer">*/}
						{/*<button type="button" style={{borderRadius:'20px',width:'30%'}} className="btn btn-primary">*/}
						{/*Сохранить</button>*/}
						{/*</div>*/}
					</div>
				</div>
			</div>

			<div {...{ className: "modal fade bootstrapWindows modalWindow" }as any} id="deleteUserModal" tabIndex="-1"
					 role="dialog"
					 aria-labelledby="deleteUserModalLabel" aria-hidden="true"
					 style={{ marginLeft: '40%', marginTop: '8%' }}>
				<div className="modal-dialog" role="document">
					<div className="modal-content" style={{ borderRadius: '20px' }}>
						<div className="modal-header">
							<img src={deleteUserIcon} alt="" style={{ width: '15%', marginTop: '1%' }}/>
							<h3 className="modal-title fontTitleWindow" id="deleteUserModalLabel"
									style={{ marginTop: '3%', marginLeft: '5px', fontWeight: 600 }}>
								Удаление<br/>Администратора</h3>

							<button type="button" className="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<div className="modal-body">
							<div style={{ marginLeft: '7%', marginRight: '7%', marginTop: '1%' }}>
								<label htmlFor="" style={{ fontSize: '0.7vw' }}>Вы действительно хотите удалить
									парнера?</label>
								<br/><br/>
								<div>
									<h5>{this.state.userForModal.name} </h5><br/>
									<h5>{this.state.userForModal.partner} </h5><br/>
									{/*<span>{user.role}</span>*/}
								</div>
								<br/><br/>
								<div style={{ display: 'flex', flexDirection: 'row', justifyContent: 'flex-end' }}>
									<div className="" style={{ width: '35%' }}>
										<button type="button"
														style={{
															borderRadius: '20px', width: '100%',
															marginTop: '10%', marginBottom: '2%'
														}}
														className="btn-xs btn-default fontButtonWindow">
											<span>Отмена</span>
										</button>
									</div>
									<div className="fontButtonWindow" style={{ width: '35%' }}>
										<button type="button" style={{
											borderRadius: '20px', marginLeft: '8%', marginTop: '10%',
											width: '100%', marginBottom: '2%'
										}} className="btn-xs btn-danger">
											<span className="fontButtonWindow">Удалить</span>

										</button>
									</div>
								</div>
							</div>

						</div>

					</div>
				</div>
			</div>
		</div>
	}

}

export default Administrator;