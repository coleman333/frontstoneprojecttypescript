import * as React from "react";
import { connect } from "react-redux";
import { Dispatch } from "redux";
// bindActionCreators,
import { RouteComponentProps } from "react-router";
// import { TodoActions } from 'app/actions';
import { RootState } from "app/reducers";
// import { TodoModel } from 'app/models';
// import { omit } from 'app/utils';
// import { Header, TodoList, Footer } from 'app/components';
// styles
import "./style.scss";
import MuiThemeProvider from "material-ui/styles/MuiThemeProvider";
import getMuiTheme from "material-ui/styles/getMuiTheme";
// const FILTER_VALUES = (Object.keys(TodoModel.Filter) as (keyof typeof TodoModel.Filter)[]).map(
// 	(key) => TodoModel.Filter[key]
// );

// const FILTER_FUNCTIONS: Record<TodoModel.Filter, (todo: TodoModel) => boolean> = {
// 	[TodoModel.Filter.SHOW_ALL]: () => true,
// 	[TodoModel.Filter.SHOW_ACTIVE]: (todo) => !todo.completed,
// 	[TodoModel.Filter.SHOW_COMPLETED]: (todo) => todo.completed
// };
import { hot } from "react-hot-loader";

namespace App {
	export interface Props extends RouteComponentProps<any, any> {
		// todos: RootState.TodoState;
		// actions: TodoActions;
		// filter: TodoModel.Filter;
	}
}

@connect(
	(state: RootState): Pick<App.Props, any> => {
		// const hash = state.router.location && state.router.location.hash.replace('#', '');
		// const filter = FILTER_VALUES.find((value) => value === hash) || TodoModel.Filter.SHOW_ALL;
		// return { todos: state.todos, filter };
		return {};
	},
	(dispatch: Dispatch<any, any>): Pick<App.Props, any> => ({
		// actions: bindActionCreators(omit(TodoActions, 'Type'), dispatch)
	})
)

class App extends React.Component<App.Props> {
	static defaultProps: Partial<App.Props> = {
		// filter: TodoModel.Filter.SHOW_ALL
	};

	constructor(props: App.Props, context?: any) {
		super(props, context);
		// this.handleClearCompleted = this.handleClearCompleted.bind(this);
		// this.handleFilterChange = this.handleFilterChange.bind(this);
	}

	// handleClearCompleted(): void {
	// 	const hasCompletedTodo = this.props.todos.some((todo) => todo.completed || false);
	// 	if (hasCompletedTodo) {
	// 		this.props.actions.clearCompleted();
	// 	}
	// }
	//
	// handleFilterChange(filter: TodoModel.Filter): void {
	// 	this.props.history.push(`#${filter}`);
	// }

	render() {
		// const { todos, actions, filter } = this.props;
		// const activeCount = todos.length - todos.filter((todo) => todo.completed).length;
		// const filteredTodos = filter ? todos.filter(FILTER_FUNCTIONS[filter]) : todos;
		// const completedCount = todos.reduce((count, todo) => (todo.completed ? count + 1 : count), 0);
		const { children } = this.props;
		const muiTheme = getMuiTheme({});

		return (
			<MuiThemeProvider muiTheme={muiTheme}>
				{children}
			</MuiThemeProvider>
		);
	}
}

export default hot(module)(App);