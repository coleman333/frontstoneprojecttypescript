import * as React from "react";
import { connect } from "react-redux";
import { Dispatch, bindActionCreators } from "redux";
// bindActionCreators,
import { RouteComponentProps, browserHistory, Link } from "react-router";
import { RootState } from "app/reducers";
import './style.scss';
import { PartnersActions } from 'app/actions/partners';
import { BasketActions } from 'app/actions/basket';
import MainTemplate from '../MainTemplate';

// import * as _ from "lodash";

const { Tabs, Tab, TextField } = require("material-ui");
const { Table, TableBody, TableHeader, TableHeaderColumn, TableRow } = require("material-ui/Table");

// , TableRowColumn

//
// const { MenuItem}= require('material-ui/MenuItem');
// const {SelectField} = require('material-ui/SelectField');


// const editIcon = require('app/../assets/images/icon_edit.svg') as string;
// const editProfileIcon = require('app/../assets/images/icon_profile_edit.svg') as string;
// const addPhotoIcon = require('app/../assets/images/icon_add_photo.svg') as string;
// const deleteUserIcon = require('app/../assets/images/icon_delete.svg') as string;
// const passwdEditIcon = require('app/../assets/images/icon_password_edit.svg') as string;
// const addPhotoIcon = require("app/../assets/images/icon_add_photo.svg") as string;
// const deleteUserIcon = require("app/../assets/images/icon_delete.svg") as string;
// const passwdEditIcon = require("app/../assets/images/icon_password_edit.svg") as string;
// const supportIcon = require("app/../assets/images/support.svg") as string;
// const settingsIcon = require("app/../assets/images/icon_more.svg") as string;
// const markerList = require("app/../assets/images/icon_add_photo.svg") as string;
// const arrowDownIcon = require( 'app/../images/arrow down.svg') as string;
// const plusIcon = require("app/../assets/images/icon_plus.svg") as string;
// const grayViewListIcon = require("app/../assets/images/view-list.svg") as string;
// const blueViewTableIcon = require("app/../assets/images/view-table.svg") as string;
// const blueViewListIcon = require("app/../assets/images/view-list-blue.svg") as string;
// const grayViewTableIcon = require("app/../assets/images/view-table-gray.svg") as string;
// const buttonDelete = require("app/../assets/images/button_delete.svg") as string;
// const buttonEdit = require("app/../assets/images/button_edit.svg") as string;
// const photoBox = require("app/../assets/images/photo_box.svg") as string;
// const officeAvatar = require( 'app/../images/officeAvatar.jpg') as string;
// const partnersIcon = require("app/../assets/images/icon_partners.svg") as string;
// const partnersEditIcon = require("app/../assets/images/icon_parnter_edit.svg") as string;

const officeAvatar = require('app/../assets/images/officeAvatar.jpg') as string;
const searchBlue = require("app/../assets/images/searchBlue.svg") as string;
// const recycleIcon = require("app/../assets/images/recycle_icon.svg") as string;


namespace Basket {
	export interface Props extends RouteComponentProps<any, any> {
		partnersActions: typeof PartnersActions,

		partners: any,
		basketActions: typeof BasketActions,
		all: any
	}
}

@connect(
	(state: RootState): Pick<Basket.Props, any> => {
		return {
			partners: state.partners.partners.items,
			all: state.basket.all
		};
	},
	(dispatch: Dispatch<any, any>): Pick<Basket.Props, any> => ({
		partnersActions: bindActionCreators(PartnersActions, dispatch),
		basketActions: bindActionCreators(BasketActions, dispatch)
	})
)

class Basket extends MainTemplate<Basket.Props, any> {
	static defaultProps: Partial<Basket.Props> = {};

	constructor(props: Basket.Props, context?: any) {
		super(props, context);
	}

	state = {
		value: 3,
		user: {
			firstName: "Дмитрий",
			secondName: "Заруба",
			role: "superAdmin",
			phone: "+7 902 563 50 45",
			email: "dzaruba@gmail.com"
		},
		showCheckboxes: false,
		height: "100%",
		selectable: false,
		slideIndex: 1,
		toggleToEditOrDeleteFlagId: -1,
		all: [
			{
				id: 1,
				organizationName: 'Hershey',
				typeProfile: 'заявка на вступление в платформу',
				Date: '25 мая 2018 10:52'
			},
			{
				id: 2,
				organizationName: 'Unilever',
				typeProfile: 'заявка на вступление в платформу',
				Date: '25 мая 2018 10:52'
			},
			{
				id: 3,
				organizationName: 'Dannon',
				typeProfile: 'заявка на пополнение бонусного счета',
				Date: '25 мая 2018 10:52'
			},
			{
				id: 4,
				organizationName: 'Unilever',
				typeProfile: 'партнер',
				Date: '25 мая 2018 10:52'
			},
			{
				id: 5,
				organizationName: 'Kraft',
				typeProfile: 'сотрудник',
				Date: '25 мая 2018 10:52'
			},
			{
				id: 6,
				organizationName: 'Hershey',
				typeProfile: 'торговая точка',
				Date: '25 мая 2018 10:52'
			},
			{
				id: 7,
				organizationName: 'Hershey',
				typeProfile: 'ERP-система',
				Date: '25 мая 2018 10:52'
			},
			{
				id: 8,
				organizationName: 'Unilever',
				typeProfile: 'партнер',
				Date: '25 мая 2018 10:52'
			},
		],
		requests: [
			{
				id: 1,
				organizationName: 'Hershey',
				typeProfile: 'заявка на вступление в платформу',
				Date: '25 мая 2018 10:52'
			},
			{
				id: 2,
				organizationName: 'Unilever',
				typeProfile: 'заявка на вступление в платформу',
				Date: '25 мая 2018 10:52'
			},
		],
		bonuses: [
			{
				id: 1,
				organizationName: 'Dannon',
				typeProfile: 'заявка на пополнение бонусного счета',
				Date: '25 мая 2018 10:52'
			},
		],
		partners: [
			{
				id: 1,
				organizationName: 'Unilever',
				typeProfile: 'партнер',
				Date: '25 мая 2018 10:52'
			},
			{
				id: 2,
				organizationName: 'Unilever',
				typeProfile: 'партнер',
				Date: '25 мая 2018 10:52'
			},
		],
		employees: [
			{
				id: 1,
				organizationName: 'Kraft',
				typeProfile: 'сотрудник',
				Date: '25 мая 2018 10:52'
			},
		],
		points: [
			{
				id: 1,
				organizationName: 'Hershey',
				typeProfile: 'торговая точка',
				Date: '25 мая 2018 10:52'
			},
		],
		ERP: [
			{
				id: 1,
				organizationName: 'Hershey',
				typeProfile: 'ERP-система',
				Date: '25 мая 2018 10:52'
			},
		],

		// partners: [
		//   {
		//     id: 1,
		//     logo: addPhotoIcon,
		//     name: "companyName1",
		//     city: "city1",
		//     contact: "name1"
		//   },
		//   {
		//     id: 2,
		//     logo: deleteUserIcon,
		//     name: "companyName2",
		//     city: "city2",
		//     contact: "name2"
		//   },
		//   {
		//     id: 3,
		//     logo: officeAvatar,
		//     name: "companyName3",
		//     city: "city3",
		//     contact: "name3"
		//   },
		//   {
		//     id: 4,
		//     logo: passwdEditIcon,
		//     name: "companyName4",
		//     city: "city4",
		//     contact: "name4"
		//   },
		//   {
		//     id: 5,
		//     logo: officeAvatar,
		//     name: "companyName5",
		//     city: "city5",
		//     contact: "name5"
		//   },
		//   {
		//     id: 6,
		//     logo: passwdEditIcon,
		//     name: "companyName6",
		//     city: "city6",
		//     contact: "name6"
		//   },
		//   {
		//     id: 7,
		//     logo: passwdEditIcon,
		//     name: "companyName7",
		//     city: "city7",
		//     contact: "name7"
		//   },
		//   {
		//     id: 4,
		//     logo: passwdEditIcon,
		//     name: "companyName4",
		//     city: "city4",
		//     contact: "name4"
		//   },
		//   {
		//     id: 5,
		//     logo: passwdEditIcon,
		//     name: "companyName5",
		//     city: "city5",
		//     contact: "name5"
		//   },
		//   {
		//     id: 6,
		//     logo: passwdEditIcon,
		//     name: "companyName6",
		//     city: "city6",
		//     contact: "name6"
		//   },
		//   {
		//     id: 7,
		//     logo: passwdEditIcon,
		//     name: "companyName7",
		//     city: "city7",
		//     contact: "name7"
		//   }
		// ],

		userForAll: {
			id: "",
			contact: "",
			city: "",
			name: ""
		},
		selectedCity: "",
		searchEnableField: false,
		// user = {
		//     firstName: 'Дмитрий',
		//     secondName: 'Заруба',
		//     role: 'superAdmin',
		//     phone: '+7 902 563 50 45',
		//     email: 'dzaruba@gmail.com',
		//     avatar: '../../images/officeAvatar.jpg'
		//
		isSidePanelOpen: false,
		limit: 50,
		skip: 0
	};

	tabChange = (value: any) => {
		this.setState({
			slideIndex: value
		});

	};

	moveToThePartner(id: any) {
		browserHistory.push(`/partners/${id}/users-partners`)
	}

	onChangeCitySelect(ev: any) {
		let city = ev.target.value;
		this.setState({ selectedCity: city });

	}

	searchEnableFieldMethod() {
		this.setState({ searchEnableField: true });
	}

	onBlurSearchField() {
		this.setState({ searchEnableField: false })
	}

	toggleSidePanel(user: any, isOpened: boolean, ev: any) {
		if (ev) {
			ev.stopPropagation();
		}
		if (this.state.isSidePanelOpen === isOpened) {
			return;
		}
		this.setState({
			userForModal: user,
			isSidePanelOpen: isOpened
		})
	}

	componentDidMount() {
		this.props.basketActions.getAllBasket(this.state.limit, this.state.skip)
			.then(() => {
				console.log('asdhfkl', this.props.all);
			})
	}


	renderHeader() {
		return <div>
			<div className={'header-top'}>
				<div className={'search-bar'}>
					<img src={searchBlue} className={'search-img'} alt=""
							 {...{ onTouchTap: this.searchEnableFieldMethod.bind(this) }}
					/>
					{
						this.state.searchEnableField && <TextField
							name={'headerSearch'}
							className={'search-text'}
							hintText=""
							placeholder={'Поиск...'}
							fullWidth={true}
							ref="search"
							onBlur={this.onBlurSearchField.bind(this)}
							autoFocus={true}
						/>
					}
				</div>

				<Link to="/profile">
					<img className="avatar" src={officeAvatar} alt=""/>
				</Link>
			</div>


			<div className={'header-middle'}>
				<div style={{ display: "flex", alignItems: "center", color: "#5a5a5a" }}>
					Фильтр по партнерам:
				</div>
				<select
					className="form-control" id="exampleFormControlSelect1"
					style={{ width: "20%", borderRadius: "20px" }}
					onChange={this.onChangeCitySelect.bind(this)}
				>
					<option value="">Все партнеры</option>
					{
						this.state.partners.map((item, index) => {
							return <option value={item.organizationName} key={index}>{item.organizationName}</option>;
						})
					}
				</select>
			</div>


			<div className={'header-bottom'}>
				<h2 style={{ marginLeft: '5%', font: 'bold' }}>Корзина</h2>
				<div style={{ width: '900px', marginLeft: "400px" }} className="">
					<Tabs
						tabItemContainerStyle={{ backgroundColor: "transparent" }}
						inkBarStyle={{ backgroundColor: "blue", height: "4px" }}
						onChange={this.tabChange} value={this.state.slideIndex}
					>
						<Tab label="Все" value={1} style={{ color: 'blue', fontSize: '9px' }}> </Tab>
						<Tab label="Заявки на вступление" value={2} style={{ color: 'blue', fontSize: '9px' }}> </Tab>
						<Tab label="Бонусные счета" value={3} style={{ color: 'blue', fontSize: '9px' }}> </Tab>
						<Tab label="Партнеры" value={4} style={{ color: 'blue', fontSize: '9px' }}> </Tab>
						<Tab label="Сотрудники" value={5} style={{ color: 'blue', fontSize: '9px' }}> </Tab>
						<Tab label="Торговые точки" value={6} style={{ color: 'blue', fontSize: '9px' }}> </Tab>
						<Tab label="ERP-системы" value={7} style={{ color: 'blue', fontSize: '9px' }}> </Tab>
					</Tabs>

				</div>
			</div>


		</div>
	}

	renderContent() {
		// let partners = this.state.partners;

		// if (this.state.selectedCity) {
		// partners = partners.filter(p => p.city === this.state.selectedCity);
		// }

		return <div>
			<div className="row"
				// style={{ flex: 1 }}
					 style={{ flex: 1, background: 'white', height: 'calc(100% - 170px - 50px)' }}
			>
				<div className="col-lg-12" style={{ backgroundColor: "white", height: "100%" }}>
					{/*{this.state.slideIndex === 1 &&*/}
						<div>
						<Table height={this.state.height}
						// selectable={this.state.selectable} multiSelectable={true}
						// onRowSelection={this.onRowSelection.bind(this)}
						>
						<TableHeader
						// displaySelectAll={this.state.selectable}
						// adjustForCheckbox={true}
						>
						<TableRow>
						<TableHeaderColumn>Организация</TableHeaderColumn>
						<TableHeaderColumn>Тип профиля удаленного обьекта</TableHeaderColumn>
						<TableHeaderColumn style={{ marginLeft: '200px' }}>Дата удаления</TableHeaderColumn>
						<TableHeaderColumn></TableHeaderColumn>
						</TableRow>
						</TableHeader>
						<TableBody displayRowCheckbox={false}
						// displayRowCheckbox={this.state.showCheckboxes}
						>
						{/*{*/}
							{/*// this.state.slideIndex === 1 && */}
							{/*this.state.all.map((item, index) => {*/}
								{/*return <TableRow key={index} className="tableOnHover" style={{ height: '80px' }}*/}
									{/*// selected={_.includes(selectedRows, index)}*/}
								{/*>*/}
									{/*<TableRowColumn className={'custom-tr'}>*/}
										{/*<div className="inner-tr-col" onClick={this.toggleSidePanel.bind(this, item, true)}>*/}
											{/*{item.organizationName}*/}
										{/*</div>*/}
									{/*</TableRowColumn>*/}
									{/*<TableRowColumn className={'custom-tr'}>*/}
										{/*<div className="inner-tr-col" onClick={this.toggleSidePanel.bind(this, item, true)}>*/}
											{/*<span>{item.typeProfile}</span>*/}
										{/*</div>*/}
									{/*</TableRowColumn>*/}
									{/*<TableRowColumn className={'custom-tr'} onClick={this.toggleSidePanel.bind(this, item, true)}>*/}
										{/*<div className="inner-tr-col" onClick={this.toggleSidePanel.bind(this, item, true)}>*/}
											{/*<span>{item.Date}</span>*/}
										{/*</div>*/}
									{/*</TableRowColumn>*/}
									{/*<TableRowColumn className={'custom-tr'} onClick={this.toggleSidePanel.bind(this, item, true)}>*/}
										{/*<div className="inner-tr-col" style={{ marginLeft: '300px' }}*/}
												 {/*onClick={this.toggleSidePanel.bind(this, item, true)}>*/}
											{/*<img src={recycleIcon} style={{ width: '40px' }} alt=""/>*/}
										{/*</div>*/}
									{/*</TableRowColumn>*/}


								{/*</TableRow>*/}
							{/*})*/}
						{/*}*/}
						{/*{*/}
							{/*this.state.slideIndex === 2 && this.state.requests.map((item, index) => {*/}
								{/*return <TableRow key={index} className="tableOnHover" style={{ height: '80px' }}*/}
									{/*// selected={_.includes(selectedRows, index)}*/}
								{/*>*/}
									{/*<TableRowColumn className={'custom-tr'}>*/}
										{/*<div className="inner-tr-col" onClick={this.toggleSidePanel.bind(this, item, true)}>*/}
											{/*{item.organizationName}*/}
										{/*</div>*/}
									{/*</TableRowColumn>*/}
									{/*<TableRowColumn className={'custom-tr'}>*/}
										{/*<div className="inner-tr-col" onClick={this.toggleSidePanel.bind(this, item, true)}>*/}
											{/*<span>{item.typeProfile}</span>*/}
										{/*</div>*/}
									{/*</TableRowColumn>*/}
									{/*<TableRowColumn className={'custom-tr'} onClick={this.toggleSidePanel.bind(this, item, true)}>*/}
										{/*<div className="inner-tr-col" onClick={this.toggleSidePanel.bind(this, item, true)}>*/}
											{/*<span>{item.Date}</span>*/}
										{/*</div>*/}
									{/*</TableRowColumn>*/}
									{/*<TableRowColumn className={'custom-tr'} onClick={this.toggleSidePanel.bind(this, item, true)}>*/}
										{/*<div className="inner-tr-col" onClick={this.toggleSidePanel.bind(this, item, true)}>*/}
											{/*<img src={recycleIcon} style={{ width: '40px' }} alt=""/>*/}
										{/*</div>*/}
									{/*</TableRowColumn>*/}


								{/*</TableRow>*/}
							{/*})*/}
						{/*}*/}
						{/*{*/}
							{/*this.state.slideIndex === 3 && this.state.bonuses.map((item, index) => {*/}
								{/*return <TableRow key={index} className="tableOnHover" style={{ height: '80px' }}*/}
									{/*// selected={_.includes(selectedRows, index)}*/}
								{/*>*/}
									{/*<TableRowColumn className={'custom-tr'}>*/}
										{/*<div className="inner-tr-col" onClick={this.toggleSidePanel.bind(this, item, true)}>*/}
											{/*{item.organizationName}*/}
										{/*</div>*/}
									{/*</TableRowColumn>*/}
									{/*<TableRowColumn className={'custom-tr'}>*/}
										{/*<div className="inner-tr-col" onClick={this.toggleSidePanel.bind(this, item, true)}>*/}
											{/*<span>{item.typeProfile}</span>*/}
										{/*</div>*/}
									{/*</TableRowColumn>*/}
									{/*<TableRowColumn className={'custom-tr'} onClick={this.toggleSidePanel.bind(this, item, true)}>*/}
										{/*<div className="inner-tr-col" onClick={this.toggleSidePanel.bind(this, item, true)}>*/}
											{/*<span>{item.Date}</span>*/}
										{/*</div>*/}
									{/*</TableRowColumn>*/}
									{/*<TableRowColumn className={'custom-tr'} onClick={this.toggleSidePanel.bind(this, item, true)}>*/}
										{/*<div className="inner-tr-col" onClick={this.toggleSidePanel.bind(this, item, true)}>*/}
											{/*<img src={recycleIcon} style={{ width: '40px' }} alt=""/>*/}
										{/*</div>*/}
									{/*</TableRowColumn>*/}


								{/*</TableRow>*/}
							{/*})*/}
						{/*}*/}

						{/*{*/}
							{/*this.state.slideIndex === 4 && this.state.partners.map((item, index) => {*/}
								{/*return <TableRow key={index} className="tableOnHover" style={{ height: '80px' }}*/}
									{/*// selected={_.includes(selectedRows, index)}*/}
								{/*>*/}
									{/*<TableRowColumn className={'custom-tr'}>*/}
										{/*<div className="inner-tr-col" onClick={this.toggleSidePanel.bind(this, item, true)}>*/}
											{/*{item.organizationName}*/}
										{/*</div>*/}
									{/*</TableRowColumn>*/}
									{/*<TableRowColumn className={'custom-tr'}>*/}
										{/*<div className="inner-tr-col" onClick={this.toggleSidePanel.bind(this, item, true)}>*/}
											{/*<span>{item.typeProfile}</span>*/}
										{/*</div>*/}
									{/*</TableRowColumn>*/}
									{/*<TableRowColumn className={'custom-tr'} onClick={this.toggleSidePanel.bind(this, item, true)}>*/}
										{/*<div className="inner-tr-col" onClick={this.toggleSidePanel.bind(this, item, true)}>*/}
											{/*<span>{item.Date}</span>*/}
										{/*</div>*/}
									{/*</TableRowColumn>*/}
									{/*<TableRowColumn className={'custom-tr'} onClick={this.toggleSidePanel.bind(this, item, true)}>*/}
										{/*<div className="inner-tr-col" onClick={this.toggleSidePanel.bind(this, item, true)}>*/}
											{/*<img src={recycleIcon} style={{ width: '40px' }} alt=""/>*/}
										{/*</div>*/}
									{/*</TableRowColumn>*/}


								{/*</TableRow>*/}
							{/*})*/}
						{/*}*/}
						{/*{*/}
							{/*this.state.slideIndex === 5 && this.state.employees.map((item, index) => {*/}
								{/*return <TableRow key={index} className="tableOnHover" style={{ height: '80px' }}*/}
									{/*// selected={_.includes(selectedRows, index)}*/}
								{/*>*/}
									{/*<TableRowColumn className={'custom-tr'}>*/}
										{/*<div className="inner-tr-col" onClick={this.toggleSidePanel.bind(this, item, true)}>*/}
											{/*{item.organizationName}*/}
										{/*</div>*/}
									{/*</TableRowColumn>*/}
									{/*<TableRowColumn className={'custom-tr'}>*/}
										{/*<div className="inner-tr-col" onClick={this.toggleSidePanel.bind(this, item, true)}>*/}
											{/*<span>{item.typeProfile}</span>*/}
										{/*</div>*/}
									{/*</TableRowColumn>*/}
									{/*<TableRowColumn className={'custom-tr'} onClick={this.toggleSidePanel.bind(this, item, true)}>*/}
										{/*<div className="inner-tr-col" onClick={this.toggleSidePanel.bind(this, item, true)}>*/}
											{/*<span>{item.Date}</span>*/}
										{/*</div>*/}
									{/*</TableRowColumn>*/}
									{/*<TableRowColumn className={'custom-tr'} onClick={this.toggleSidePanel.bind(this, item, true)}>*/}
										{/*<div className="inner-tr-col" onClick={this.toggleSidePanel.bind(this, item, true)}>*/}
											{/*<img src={recycleIcon} style={{ width: '40px' }} alt=""/>*/}
										{/*</div>*/}
									{/*</TableRowColumn>*/}


								{/*</TableRow>*/}
							{/*})*/}
						{/*}*/}
						{/*{*/}
							{/*this.state.slideIndex === 6 && this.state.points.map((item, index) => {*/}
								{/*return <TableRow key={index} className="tableOnHover" style={{ height: '80px' }}*/}
									{/*// selected={_.includes(selectedRows, index)}*/}
								{/*>*/}
									{/*<TableRowColumn className={'custom-tr'}>*/}
										{/*<div className="inner-tr-col" onClick={this.toggleSidePanel.bind(this, item, true)}>*/}
											{/*{item.organizationName}*/}
										{/*</div>*/}
									{/*</TableRowColumn>*/}
									{/*<TableRowColumn className={'custom-tr'}>*/}
										{/*<div className="inner-tr-col" onClick={this.toggleSidePanel.bind(this, item, true)}>*/}
											{/*<span>{item.typeProfile}</span>*/}
										{/*</div>*/}
									{/*</TableRowColumn>*/}
									{/*<TableRowColumn className={'custom-tr'} onClick={this.toggleSidePanel.bind(this, item, true)}>*/}
										{/*<div className="inner-tr-col" onClick={this.toggleSidePanel.bind(this, item, true)}>*/}
											{/*<span>{item.Date}</span>*/}
										{/*</div>*/}
									{/*</TableRowColumn>*/}
									{/*<TableRowColumn className={'custom-tr'} onClick={this.toggleSidePanel.bind(this, item, true)}>*/}
										{/*<div className="inner-tr-col" onClick={this.toggleSidePanel.bind(this, item, true)}>*/}
											{/*<img src={recycleIcon} style={{ width: '40px' }} alt=""/>*/}
										{/*</div>*/}
									{/*</TableRowColumn>*/}


								{/*</TableRow>*/}
							{/*})*/}
						{/*}*/}
						{/*{*/}
							{/*this.state.slideIndex === 7 && this.state.ERP.map((item, index) => {*/}
								{/*return <TableRow key={index} className="tableOnHover" style={{ height: '80px' }}*/}
									{/*// selected={_.includes(selectedRows, index)}*/}
								{/*>*/}
									{/*<TableRowColumn className={'custom-tr'}>*/}
										{/*<div className="inner-tr-col" onClick={this.toggleSidePanel.bind(this, item, true)}>*/}
											{/*{item.organizationName}*/}
										{/*</div>*/}
									{/*</TableRowColumn>*/}
									{/*<TableRowColumn className={'custom-tr'}>*/}
										{/*<div className="inner-tr-col" onClick={this.toggleSidePanel.bind(this, item, true)}>*/}
											{/*<span>{item.typeProfile}</span>*/}
										{/*</div>*/}
									{/*</TableRowColumn>*/}
									{/*<TableRowColumn className={'custom-tr'} onClick={this.toggleSidePanel.bind(this, item, true)}>*/}
										{/*<div className="inner-tr-col" onClick={this.toggleSidePanel.bind(this, item, true)}>*/}
											{/*<span>{item.Date}</span>*/}
										{/*</div>*/}
									{/*</TableRowColumn>*/}
									{/*<TableRowColumn className={'custom-tr'} onClick={this.toggleSidePanel.bind(this, item, true)}>*/}
										{/*<div className="inner-tr-col" onClick={this.toggleSidePanel.bind(this, item, true)}>*/}
											{/*<img src={recycleIcon} style={{ width: '40px' }} alt=""/>*/}
										{/*</div>*/}
									{/*</TableRowColumn>*/}


								{/*</TableRow>*/}
							{/*})*/}
						{/*}*/}
						</TableBody>
						</Table>


						</div>
					{/*}*/}


				</div>
			</div>

		</div>
	}

	renderSidePanel() {
		return <div className={'inner-panel'}>

			<div className="inner-top">
				<div>
					{/*<img src={closeCrossBlack} style={{ width: '25px' }}*/}
					{/*{...{ onTouchTap: this.toggleSidePanel.bind(this, {}, false) }} alt=""*/}
					/>
				</div>
				{/*<span style={{ color: '#1d1f26', fontSize: 12 }}>*/}
				{/*some text*/}
				{/*{this.state.userForModal.partnerName}*/}
				{/*</span>*/}
			</div>
			<div className="inner-profile">
				{/*<div>*/}
				{/*<Avatar src={this.state.userForModal.avatar} size={80} className={'avatar'}/>*/}
				{/*</div>*/}

				{/*<div>*/}
				{/*<h5 className="sidePanelMainMenuMainTitle">{this.state.userForModal.name}some text</h5>*/}

				{/*<div className={'profile-text-block'}>*/}
				{/*<img src={this.state.userForModal.roleIcon} alt=""/>*/}
				{/*<span className="sidePanelMainMenuText">{this.state.userForModal.role}some text</span>*/}
				{/*</div>*/}

				{/*<div className={'profile-text-block'}>*/}
				{/*<img src={mailIcon} alt=""/>*/}
				{/*<span className="sidePanelMainMenuText">{this.state.userForModal.email}some text</span>*/}
				{/*</div>*/}

				{/*<div className={'profile-text-block'}>*/}
				{/*<img src={phoneIcon} alt=""/>*/}
				{/*<span className="sidePanelMainMenuText">{this.state.userForModal.phone}some text</span>*/}
				{/*</div>*/}

				{/*</div>*/}

				{/*<div>*/}
				{/*<div>*/}
				{/*<img src={buttonEdit} alt=""*/}
				{/*onClick={this.openEditProfileWindow.bind(this, this.state.userForModal)}*/}
				{/*/>*/}
				{/*<img src={buttonDelete} alt=""*/}
				{/*onClick={this.openDeleteUserProfileWindow.bind(this, this.state.userForModal)}*/}
				{/*/>*/}
				{/*</div>*/}
				{/*</div>*/}
			</div>

			<div className="inner-content">
				<hr/>
				<div style={{ width: '300px' }}>
					<Tabs
						tabItemContainerStyle={{ backgroundColor: "transparent" }}
						inkBarStyle={{ backgroundColor: "blue", height: "4px" }}
						// onChange={this.tabChangeSidePanel} value={this.state.slideIndex}
					>

						<Tab label="история посещений" value={6} style={{ color: 'blue', fontSize: '10px' }}> </Tab>
						<Tab label="история действий" value={7} style={{ color: 'blue', fontSize: '10px' }}> </Tab>

					</Tabs>


				</div>
				{/*{*/}
				{/*this.state.slideIndex2 === 7 && <div>*/}
				{/*<div>*/}
				{/*<Table height={this.state.height} selectable={false}>*/}
				{/*<TableHeader displaySelectAll={false} adjustForCheckbox={false}*/}
				{/*style={{ backgroundColor: '#f8f8f8' }}*/}
				{/*>*/}
				{/*<TableRow>*/}
				{/*<TableHeaderColumn><span className="fontSidePanel">№</span></TableHeaderColumn>*/}
				{/*<TableHeaderColumn><span className="fontSidePanel">Раздел</span></TableHeaderColumn>*/}
				{/*<TableHeaderColumn><span className="fontSidePanel">Действие</span></TableHeaderColumn>*/}
				{/*<TableHeaderColumn><span className="fontSidePanel">Время активности</span></TableHeaderColumn>*/}
				{/*</TableRow>*/}
				{/*</TableHeader>*/}
				{/*<TableBody displayRowCheckbox={false}*/}
				{/*>*/}
				{/*{this.state.historyAction.map((item, index) => {*/}
				{/*return <TableRow key={index} className="tableOnHover">*/}
				{/*<TableRowColumn><span style={{ color: '#8f97bf' }} className="fontSidePanel">*/}
				{/*{index + 1}</span></TableRowColumn>*/}
				{/*<TableRowColumn><span style={{ color: '#8f97bf' }} className="fontSidePanel">*/}
				{/*{item.department}</span></TableRowColumn>*/}
				{/*<TableRowColumn><span style={{ color: '#8f97bf' }} className="fontSidePanel">*/}
				{/*{item.action}</span></TableRowColumn>*/}
				{/*<TableRowColumn><span style={{ color: '#8f97bf' }} className="fontSidePanel">*/}
				{/*{item.activityTime}</span></TableRowColumn>*/}
				{/*</TableRow>*/}
				{/*})}*/}


				{/*</TableBody>*/}
				{/*</Table>*/}


				{/*</div>*/}
				{/*</div>*/}
				{/*}*/}

				{/*{*/}
				{/*this.state.slideIndex2 === 6 && <div>*/}
				{/*<div>*/}
				{/*<Table height={this.state.height} selectable={false}>*/}
				{/*<TableHeader displaySelectAll={false} adjustForCheckbox={false}*/}
				{/*style={{ backgroundColor: '#f8f8f8' }}*/}
				{/*>*/}
				{/*<TableRow>*/}
				{/*<TableHeaderColumn><span className="fontSidePanel">№</span></TableHeaderColumn>*/}
				{/*<TableHeaderColumn><span className="fontSidePanel">Начало сессии</span></TableHeaderColumn>*/}
				{/*<TableHeaderColumn><span className="fontSidePanel">Конец сессии</span></TableHeaderColumn>*/}
				{/*<TableHeaderColumn><span className="fontSidePanel">Время сессии</span></TableHeaderColumn>*/}
				{/*<TableHeaderColumn><span className="fontSidePanel">IP-адрес</span></TableHeaderColumn>*/}
				{/*</TableRow>*/}
				{/*</TableHeader>*/}
				{/*<TableBody displayRowCheckbox={false}*/}
				{/*>*/}
				{/*{this.state.historyVisiting.map((item, index) => {*/}
				{/*return <TableRow key={index} className="tableOnHover ">*/}
				{/*<TableRowColumn><span style={{ color: '#8f97bf' }} className="fontSidePanel">*/}
				{/*{index + 1}</span></TableRowColumn>*/}
				{/*<TableRowColumn><span style={{ color: '#8f97bf' }} className="fontSidePanel">*/}
				{/*{item.startSession}</span></TableRowColumn>*/}
				{/*<TableRowColumn><span style={{ color: '#8f97bf' }} className="fontSidePanel">*/}
				{/*{item.endSession}</span></TableRowColumn>*/}
				{/*<TableRowColumn><span style={{ color: '#8f97bf' }} className="fontSidePanel">*/}
				{/*{item.sessionTime}</span></TableRowColumn>*/}
				{/*<TableRowColumn><span style={{ color: '#8f97bf' }} className="fontSidePanel">*/}
				{/*{item.ip}</span></TableRowColumn>*/}
				{/*</TableRow>*/}
				{/*})}*/}


				{/*</TableBody>*/}
				{/*</Table>*/}


				{/*</div>*/}
				{/*</div>*/}
				{/*}*/}


			</div>


		</div>
	}


}

export default Basket;