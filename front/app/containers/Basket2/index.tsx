import * as React from "react";
import { connect } from "react-redux";
import { Dispatch, bindActionCreators } from "redux";
import { RouteComponentProps, browserHistory, Link } from "react-router";
import { RootState } from "app/reducers";
import { PartnersActions } from 'app/actions/partners';
import { BasketActions } from 'app/actions/basket';
import MainTemplate from '../MainTemplate';
import * as _ from "lodash";
import FloatingActionButton from 'material-ui/FloatingActionButton';
import PartnerSidePanelBasket from '../../components/PartnerSidePanelBasket';
import RequestsSidePanelBasket from '../../components/RequestsSidePanelBasket';
import EmployeeSidePanelBasket from '../../components/EmployeeSidePanelBasket';
import BonusesSidePanelBasket from "app/components/BonusesSidePanelBasket";
import PointsSidePanelBasket from "app/components/PointsSidePanelBasket";
import ERPSidePanelBasket from "app/components/ERPSidePanelBasket";

import './style.scss';

// import  InfiniteScroll  from 'react-infinite-scroll-component';

const { TextField } = require("material-ui");

const officeAvatar = require('app/../assets/images/officeAvatar.jpg') as string;
const searchBlue = require("app/../assets/images/searchBlue.svg") as string;
const recycleIcon = require("app/../assets/images/recycle_icon.svg") as string;
// const recycleIconWhite = require("app/../assets/images/recycle_icon_white.svg") as string;
// const closeCrossBlack = require("app/../assets/images/close_button_search_black.svg") as string;
// const hershyesIcon = require("app/../assets/images/hershes.png") as string;
const broomIcon = require("app/../assets/images/broom_icon.svg") as string;
const arrowBlue = require("app/../assets/images/blueArrow.svg") as string;
// const broomIcon = require("app/../assets/images/broom_icon.svg") as string;
// const adminRoleIcon = require("app/../assets/images/adminRole.svg") as string;
// const marketologRoleIcon = require("app/../assets/images/marketologRole.svg") as string;
// const operatorRoleIcon = require("app/../assets/images/operatorRole.svg") as string;
const InfiniteScroll = require('react-infinite-scroll-component');

const fields = {
	all: [
		{
			key: 'organizationName',
			title: 'Организация',
			style: { width: 562, paddingLeft: 64 }
		},
		{
			key: 'typeProfile',
			title: <div>
				<span className="middleTableStyle">
					Тип профиля удаленного обьекта</span>
				<img src={arrowBlue} style={{ height: 9, width: 9 ,marginLeft:'15px'}} alt=""/>
			</div>,
			style: { width: 699 }
		},
		{
			key: 'date',
			title: 'Дата удаления',
			style: { width: 280 }
		}
	],
	requests: [
		{
			key: 'organizationName',
			title: 'Организация',
			style: { width: 562, paddingLeft: 64 }
		},
		{
			key: 'typeProfile',
			title: <div>
				<span className="middleTableStyle">
					Тип профиля удаленного обьекта</span>
				<img src={arrowBlue} style={{ height: 9, width: 9 ,marginLeft:'15px'}} alt=""/>
			</div>,
			style: { width: 699 }
		},
		{
			key: 'date',
			title: 'Дата удаления',
			style: { width: 280 }
		},
	],
	bonuses: [
		{
			key: 'organizationName',
			title: 'Организация',
			style: { width: 562, paddingLeft: 64 }
		},
		{
			key: 'typeProfile',
			title: <div>
				<span className="middleTableStyle">
					Тип профиля удаленного обьекта</span>
				<img src={arrowBlue} style={{ height: 9, width: 9 ,marginLeft:'15px'}} alt=""/>
			</div>,
			style: { width: 699 }
		},
		{
			key: 'date',
			title: 'Дата удаления',
			style: { width: 280 }
		},
	],
	partners: [
		{
			key: 'organizationName',
			title: 'Организация',
			style: { width: 562, paddingLeft: 64 }
		},
		{
			key: 'typeProfile',
			title: <div>
				<span className="middleTableStyle">
					Тип профиля удаленного обьекта</span>
				<img src={arrowBlue} style={{ height: 9, width: 9 ,marginLeft:'15px'}} alt=""/>
			</div>,
			style: { width: 699 }
		},
		{
			key: 'date',
			title: 'Дата удаления',
			style: { width: 280 }
		},
	],
	employees: [
		{
			key: 'organizationName',
			title: 'Организация',
			style: { width: 562, paddingLeft: 64 }
		},
		{
			key: 'typeProfile',
			title: <div>
				<span className="middleTableStyle">
					Тип профиля удаленного обьекта</span>
				<img src={arrowBlue} style={{ height: 9, width: 9 ,marginLeft:'15px'}} alt=""/>
			</div>,
			style: { width: 699 }
		},
		{
			key: 'date',
			title: 'Дата удаления',
			style: { width: 280 }
		},
	],
	points: [
		{
			key: 'organizationName',
			title: 'Организация',
			style: { width: 562, paddingLeft: 64 }
		},
		{
			key: 'typeProfile',
			title: <div>
				<span className="middleTableStyle">
					Тип профиля удаленного обьекта</span>
				<img src={arrowBlue} style={{ height: 9, width: 9 ,marginLeft:'15px'}} alt=""/>
			</div>,
			style: { width: 699 }
		},
		{
			key: 'date',
			title: 'Дата удаления',
			style: { width: 280 }
		},
	],
	ERP: [
		{
			key: 'organizationName',
			title: 'Организация',
			style: { width: 562, paddingLeft: 64 }
		},
		{
			key: 'typeProfile',
			title: <div>
				<span className="middleTableStyle">
					Тип профиля удаленного обьекта</span>
				<img src={arrowBlue} style={{ height: 9, width: 9 ,marginLeft:'15px'}} alt=""/>
			</div>,
			style: { width: 699 }
		},
		{
			key: 'date',
			title: 'Дата удаления',
			style: { width: 280 }
		},
	],

};

namespace Basket2 {
	export interface Props extends RouteComponentProps<any, any> {
		partnersActions: typeof PartnersActions,
		basketActions: typeof BasketActions,
		partners: any,
		items: any,
		totalCount: 0
	}
}

@connect(
	(state: RootState): Pick<Basket2.Props, any> => {
		return {
			partners: state.basket.partners.items,
			items: state.basket.items.items,
			totalCount: state.basket.items.count
		};
	},
	(dispatch: Dispatch<any, any>): Pick<Basket2.Props, any> => ({
		partnersActions: bindActionCreators(PartnersActions, dispatch),
		basketActions: bindActionCreators(BasketActions, dispatch)
	})
)

class Basket2 extends MainTemplate<Basket2.Props, any> {
	static defaultProps: Partial<Basket2.Props> = {};

	constructor(props: Basket2.Props, context?: any) {
		super(props, context);
	}

	state = {
		value: 3,
		user: {
			firstName: "Дмитрий",
			secondName: "Заруба",
			role: "superAdmin",
			phone: "+7 902 563 50 45",
			email: "dzaruba@gmail.com"
		},
		showCheckboxes: false,
		height: "100%",
		selectable: false,
		slideIndex: 1,
		toggleToEditOrDeleteFlagId: -1,

		// all: [
		// 	{
		// 		id: 1,
		// 		organizationName: 'Hershey',
		// 		typeProfile: 'заявка на вступление в платформу',
		// 		date: '25 мая 2018 10:52'
		// 	},
		// 	{
		// 		id: 2,
		// 		organizationName: 'Unilever',
		// 		typeProfile: 'заявка на вступление в платформу',
		// 		date: '25 мая 2018 10:52'
		// 	},
		// 	{
		// 		id: 3,
		// 		organizationName: 'Dannon',
		// 		typeProfile: 'заявка на пополнение бонусного счета',
		// 		date: '25 мая 2018 10:52'
		// 	},
		// 	{
		// 		id: 4,
		// 		organizationName: 'Unilever',
		// 		typeProfile: 'партнер',
		// 		date: '25 мая 2018 10:52'
		// 	},
		// 	{
		// 		id: 5,
		// 		organizationName: 'Kraft',
		// 		typeProfile: 'сотрудник',
		// 		date: '25 мая 2018 10:52'
		// 	},
		// 	{
		// 		id: 6,
		// 		organizationName: 'Hershey',
		// 		typeProfile: 'торговая точка',
		// 		date: '25 мая 2018 10:52'
		// 	},
		// 	{
		// 		id: 7,
		// 		organizationName: 'Hershey',
		// 		typeProfile: 'ERP-система',
		// 		date: '25 мая 2018 10:52'
		// 	},
		// 	{
		// 		id: 8,
		// 		organizationName: 'Unilever',
		// 		typeProfile: 'партнер',
		// 		date: '25 мая 2018 10:52'
		// 	},
		// 	{
		// 		id: 9,
		// 		organizationName: 'Hershey',
		// 		typeProfile: 'заявка на вступление в платформу',
		// 		date: '25 мая 2018 10:52'
		// 	},
		// 	{
		// 		id: 10,
		// 		organizationName: 'Unilever',
		// 		typeProfile: 'заявка на вступление в платформу',
		// 		date: '25 мая 2018 10:52'
		// 	},
		// ],
		// requests: [
		// 	{
		// 		id: 1,
		// 		organizationName: 'Hershey',
		// 		typeProfile: 'заявка на вступление в платформу',
		// 		date: '25 мая 2018 10:52'
		// 	},
		// 	{
		// 		id: 2,
		// 		organizationName: 'Unilever',
		// 		typeProfile: 'заявка на вступление в платформу',
		// 		date: '25 мая 2018 10:52'
		// 	},
		// ],
		// bonuses: [
		// 	{
		// 		id: 1,
		// 		organizationName: 'Dannon',
		// 		typeProfile: 'заявка на пополнение бонусного счета',
		// 		date: '25 мая 2018 10:52'
		// 	},
		// ],
		// partners: [
		// 	{
		// 		id: 1,
		// 		organizationName: 'Unilever',
		// 		typeProfile: 'партнер',
		// 		date: '25 мая 2018 10:52'
		// 	},
		// 	{
		// 		id: 2,
		// 		organizationName: 'Unilever',
		// 		typeProfile: 'партнер',
		// 		date: '25 мая 2018 10:52'
		// 	},
		// ],
		// employees: [
		// 	{
		// 		id: 1,
		// 		organizationName: 'Kraft',
		// 		typeProfile: 'сотрудник',
		// 		date: '25 мая 2018 10:52'
		// 	},
		// ],
		// points: [
		// 	{
		// 		id: 1,
		// 		organizationName: 'Hershey',
		// 		typeProfile: 'торговая точка',
		// 		date: '25 мая 2018 10:52'
		// 	},
		// ],
		// ERP: [
		// 	{
		// 		id: 1,
		// 		organizationName: 'Hershey',
		// 		typeProfile: 'ERP-система',
		// 		date: '25 мая 2018 10:52'
		// 	},
		// ],

		selectedItem: {
			item: {} as any,
			open: false
		} as any,

		selectedCity: "",
		searchEnableField: false,
		// user = {
		//     firstName: 'Дмитрий',
		//     secondName: 'Заруба',
		//     role: 'superAdmin',
		//     phone: '+7 902 563 50 45',
		//     email: 'dzaruba@gmail.com',
		//     avatar: '../../images/officeAvatar.jpg'
		//
		isSidePanelOpen: false,
		limit: 10,
		sidePanelFlag: 'all'
	};

	handleChange = (event: any, index: any, value: any) => this.setState({ value });

	// tabChange = (value: any) => {
	// 	this.setState({
	// 		slideIndex: value
	// 	});
	//
	// };

	moveToThePartner(id: any) {
		browserHistory.push(`/partners/${id}/users-partners`)
	}

	onChangeCitySelect(ev: any) {
		let city = ev.target.value;
		this.setState({ selectedCity: city });

	}

	searchEnableFieldMethod() {
		this.setState({ searchEnableField: true });
	}

	onBlurSearchField() {
		this.setState({ searchEnableField: false })
	}

	DeleteAllUsersBasketModal() {
		this.props.basketActions.deleteAllBasket();
		$("#deleteAllUsersBasketModal").modal("hide");
	}

	openDeleteAllUsersBasketModal() {
		$("#deleteAllUsersBasketModal").modal({ show: true });

	}

	closeDeleteAllUsersBasketModal() {
		$("#deleteAllUsersBasketModal").modal("hide");

	}


	toggleSidePanel(item: any, isOpened: boolean, ev: any) {
		if (ev) {
			ev.stopPropagation();
		}

		if (this.state.isSidePanelOpen === isOpened) {
			return;
		}

		this.setState({
			selectedItem: {
				item: item,
				open: false
			},
			isSidePanelOpen: isOpened
		});

		if (!isOpened) {
			setTimeout(() => {
				this.setState({
					selectedItem: _.assign({}, this.state.selectedItem, {
						item: {},
					}),
				});
			}, 1000);
		} else {
			setTimeout(() => {
				this.setState({
					selectedItem: _.assign({}, this.state.selectedItem, {
						open: true,
					}),
				});
			}, 100);
		}
	}

	loadItems(props: Basket2.Props, state: any, offset: number = 0) {
		const { location: { query: { filter = 'all' } } } = props;
		const { limit = 10 } = state;
		this.props.basketActions.getAllBasket(filter, limit, offset);
	}

	componentDidMount() {
		this.loadItems(this.props, this.state, 0);
		this.props.basketActions.fetchPartners();
	}

	componentWillReceiveProps(nextProps: Basket2.Props) {
		if (this.props.location.query.filter !== nextProps.location.query.filter) {
			this.loadItems(nextProps, this.state, 0);
			this.props.basketActions.fetchPartners();
		}
	}

	renderHeader() {
		return <div>
			<div className={'header-top'}>
				<div className={'search-bar'}>
					<img src={searchBlue} className={'search-img'} alt=""
							 {...{ onTouchTap: this.searchEnableFieldMethod.bind(this) }}
					/>
					{
						this.state.searchEnableField && <TextField
							name={'headerSearch'}
							className={'search-text'}
							hintText=""
							placeholder={'Поиск...'}
							fullWidth={true}
							ref="search"
							onBlur={this.onBlurSearchField.bind(this)}
							autoFocus={true}
						/>
					}
				</div>

				<Link to="/profile">
					<img className="avatar" src={officeAvatar} alt=""/>
				</Link>
			</div>


			<div className={'header-middle'}>
				<div style={{
					display: "flex", alignItems: "center", color: "#5a5a5a", fontFamily: 'Raleway', fontSize: '14px',
					fontWeight: 300
				}}> Фильтр по партнерам:
				</div>
				<select
					className="form-control" id="exampleFormControlSelect1"
					style={{ width: "273px", height: '40px', borderRadius: "20px" }}
					onChange={this.onChangeCitySelect.bind(this)}
				>
					<option style={{ fontSize: '14px' }} value="">Все партнеры</option>
					{
						this.props.partners.map((item: any, index: number) => {
							return <option value={item.id} key={item.id}>{item.organizationName}</option>;
						})
					}
				</select>
			</div>


			<div className={'header-bottom'}>
				<span style={{
					fontFamily: 'Raleway',
					marginLeft: '52px',
					fontSize: '36px',
					fontWeight: 600,
					color: '#1d1f26'
				}}>Корзина</span>
				<div className="tabs basketTabWidth">
					<Link className={'tab-item'} style={{}} activeClassName={'active-tab'} to={'/basket?filter=all'}>
						<div><span>ВСЕ</span></div>
					</Link>
					<Link className={'tab-item'} activeClassName={'active-tab'} to={'/basket?filter=requests'}> Заявки на
						вступление</Link>
					<Link className={'tab-item'} activeClassName={'active-tab'} to={'/basket?filter=bonuses'}> Бонусный
						счет</Link>
					<Link className={'tab-item'} activeClassName={'active-tab'} to={'/basket?filter=partners'}> Партнеры</Link>
					<Link className={'tab-item'} activeClassName={'active-tab'} to={'/basket?filter=employees'}> Сотрудники</Link>
					<Link className={'tab-item'} activeClassName={'active-tab'} to={'/basket?filter=points'}> Торговые
						точки</Link>
					<Link className={'tab-item'} activeClassName={'active-tab'} to={'/basket?filter=ERP'}> ERP-системы</Link>

				</div>
			</div>


		</div>
	}

	loadMore(offset: number) {
		console.log(`infinity event`);
		this.loadItems(this.props, this.state, offset);
		this.props.basketActions.fetchPartners();
	}

	renderContent() {
		const { location: { query: { filter = 'all' } } } = this.props;
		const tableFields = _.get(fields, filter, []);

		// const items = _.get(this.state, filter, []);
		const { items = [], totalCount = 0 } = this.props;
		// console.log(123412341234, items, tableFields, filter);
//this.props.location.query.filter

		const hasMore = totalCount > items.length;

		return <div>
			<div
				className="row"
				style={{ flex: 1, background: 'white', height: 'calc(100% - 170px - 50px)' }}
			>
				<div className="col-lg-12" style={{ backgroundColor: "white", height: "100%" }}>
					<div className={'custom-table'}>
						<div className="custom-table-header">
							{
								_.map(tableFields, (field: any, index: number) =>
								<div key={index} style={field.style}>
										<span className="custom-table-item">{field.title}</span>
									</div>
								)
							}
							<div className="actions-header" style={{ flex: 1 }}></div>
						</div>
						{/*infinity scroll*/}

						<InfiniteScroll
							dataLength={items.length} //This is important field to render the next data
							next={this.loadMore.bind(this, items.length)}
							scrollThreshold={0.9}
							scrollableTarget={'content'}
							hasMore={hasMore}
							loader={<h4>Loading...</h4>}

							// below props only if you need pull down functionality
							refreshFunction={() => console.log('Not implemented yet!')}
							// pullDownToRefresh
							pullDownToRefreshContent={
								<h3 style={{ textAlign: 'center' }}>&#8595; Pull down to refresh</h3>
							}
							releaseToRefreshContent={
								<h3 style={{ textAlign: 'center' }}>&#8593; Release to refresh</h3>
							}>

							{
								items.map((item: any, index: number) => {
									return <div key={item.id} className="custom-table-row">
										{
											_.map(tableFields, (field: any, index: number) =>
												<div key={index} style={field.style}
														 className={'custom-table-row-column'}
														 {...{ onTouchTap: this.toggleSidePanel.bind(this, item, true) }}
												>
													<span className={'custom-table-item'}>{_.get(item, field.key)}</span>
												</div>
											)
										}

										<div id={'act'} className="actions-row" style={{ flex: 1 }}>
											<img src={recycleIcon} alt="" style={{ width: '32px', height: '32px' }}/>
										</div>
									</div>
								})
							}
						</InfiniteScroll>

						<FloatingActionButton secondary={true} style={{ marginLeft: "1497px", marginTop: '35px' }}
																	iconStyle={{ width: '80px', height: '80px' }}
																	{...{ onTouchTap: this.openDeleteAllUsersBasketModal.bind(this) }}>
							{/*<ContentAdd />*/}
							<img src={broomIcon} style={{ width: '28px' }} alt=""/>
						</FloatingActionButton>

						{/**/}
					</div>

				</div>
			</div>

			{/*, marginLeft: '40%', marginTop: '20%'*/}


			<div {...{ className: "modal fade bootstrapWindows modalWindow" }as any} id="deleteAllUsersBasketModal"
					 tabIndex="-1"
					 role="dialog"
					 aria-labelledby="deleteUserModalLabel" aria-hidden="true"
					 style={{ width: '328px' }}
			>
				<div className="modal-dialog" role="document">
					<div className="modal-content" style={{ borderRadius: '20px' }}>

						<div className="modal-body">
							<div style={{ textAlign: 'center', marginTop: '35px' }}>
								<span style={{ fontSize: '16px' }}>Вы действительно хотите</span>
							</div>
							<div style={{ textAlign: 'center' }}>
								<span style={{ fontSize: '16px' }}>очистить корзину?</span>
							</div>

							<div
								style={{ display: 'flex', justifyContent: 'space-around', marginTop: '15px', paddingBottom: '15px' }}>
								<button className="btn-xs btn-secondary" style={{
									width: '132px', height: '40px', borderRadius: '25px'
									, backgroundColor: '#e6e6e6', boxShadow: '0px 6px 16px 0 #cccccc80'
								}}
												{...{ onTouchTap: this.closeDeleteAllUsersBasketModal.bind(this) }}>
									<span style={{ fontSize: '13px' }}>нет</span>
								</button>
								<button className="btn-xs btn-danger" style={{
									width: '132px', height: '40px', borderRadius: '25px',
								}}
												{...{ onTouchTap: this.DeleteAllUsersBasketModal.bind(this) }}>
									<img src={broomIcon} style={{ width: '16px' }} alt=""/>
									<span style={{ fontSize: '13px' }}>очистить</span>
								</button>
							</div>
						</div>

					</div>
				</div>
			</div>

		</div>
	}

	renderSidePanel() {

		let sidePanelComponent = <div></div>;
		switch (_.get(this.state.selectedItem, 'item.sidePanelType')) {
			case 'partner': {
				sidePanelComponent = <PartnerSidePanelBasket
					open={_.get(this.state.selectedItem, 'open', false)}
					item={_.get(this.state.selectedItem, 'item')}
					onClose={this.toggleSidePanel.bind(this, _.get(this.state.selectedItem, 'item'), false)}
				/>;
				break;
			}
      case 'requests': {
        sidePanelComponent = <RequestsSidePanelBasket
          open={_.get(this.state.selectedItem, 'open', false)}
          item={_.get(this.state.selectedItem, 'item')}
          onClose={this.toggleSidePanel.bind(this, _.get(this.state.selectedItem, 'item'), false)}
        />;
        break;
      }
      case 'bonuses': {
        sidePanelComponent = <BonusesSidePanelBasket
          open={_.get(this.state.selectedItem, 'open', false)}
          item={_.get(this.state.selectedItem, 'item')}
          onClose={this.toggleSidePanel.bind(this, _.get(this.state.selectedItem, 'item'), false)}
        />;
        break;
      }
      case 'employees': {
        sidePanelComponent = <EmployeeSidePanelBasket
          open={_.get(this.state.selectedItem, 'open', false)}
          item={_.get(this.state.selectedItem, 'item')}
          onClose={this.toggleSidePanel.bind(this, _.get(this.state.selectedItem, 'item'), false)}
        />;
        break;
      }
      case 'points': {
        sidePanelComponent = <PointsSidePanelBasket
          open={_.get(this.state.selectedItem, 'open', false)}
          item={_.get(this.state.selectedItem, 'item')}
          onClose={this.toggleSidePanel.bind(this, _.get(this.state.selectedItem, 'item'), false)}
        />;
        break;
      }
      case 'ERP': {
        sidePanelComponent = <ERPSidePanelBasket
          open={_.get(this.state.selectedItem, 'open', false)}
          item={_.get(this.state.selectedItem, 'item')}
          onClose={this.toggleSidePanel.bind(this, _.get(this.state.selectedItem, 'item'), false)}
        />;
        break;
      }
		}

		return sidePanelComponent;
	}


}

export default Basket2;
