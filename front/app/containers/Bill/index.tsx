import * as React from "react";
import { connect } from "react-redux";
import { Dispatch, bindActionCreators } from "redux";
// bindActionCreators,
import { RouteComponentProps, browserHistory, Link } from "react-router";
import { RootState } from "app/reducers";
//const $ = require("jquery");
// import * as style from './style.scss';
import './style.scss';
import { PartnersActions } from 'app/actions/partners';

import MainTemplate from '../MainTemplate';

const {  TextField } = require("material-ui");
const { Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn } = require("material-ui/Table");
// const { MenuItem}= require('material-ui/MenuItem');
// const {SelectField} = require('material-ui/SelectField');
// Tabs, Tab,

// const editIcon = require('app/../assets/images/icon_edit.svg') as string;
// const editProfileIcon = require('app/../assets/images/icon_profile_edit.svg') as string;
// const addPhotoIcon = require('app/../assets/images/icon_add_photo.svg') as string;
// const deleteUserIcon = require('app/../assets/images/icon_delete.svg') as string;
// const passwdEditIcon = require('app/../assets/images/icon_password_edit.svg') as string;

// const addPhotoIcon = require("app/../assets/images/icon_add_photo.svg") as string;
// const deleteUserIcon = require("app/../assets/images/icon_delete.svg") as string;
// const passwdEditIcon = require("app/../assets/images/icon_password_edit.svg") as string;
// const supportIcon = require("app/../assets/images/support.svg") as string;
// const settingsIcon = require("app/../assets/images/icon_more.svg") as string;
// const markerList = require("app/../assets/images/icon_add_photo.svg") as string;
// const arrowDownIcon = require( 'app/../images/arrow down.svg') as string;
// const plusIcon = require("app/../assets/images/icon_plus.svg") as string;
// const grayViewListIcon = require("app/../assets/images/view-list.svg") as string;
// const blueViewTableIcon = require("app/../assets/images/view-table.svg") as string;
// const blueViewListIcon = require("app/../assets/images/view-list-blue.svg") as string;
// const grayViewTableIcon = require("app/../assets/images/view-table-gray.svg") as string;
// const buttonDelete = require("app/../assets/images/button_delete.svg") as string;
// const buttonEdit = require("app/../assets/images/button_edit.svg") as string;
// const photoBox = require("app/../assets/images/photo_box.svg") as string;
// const officeAvatar = require( 'app/../images/officeAvatar.jpg') as string;
// const partnersIcon = require("app/../assets/images/icon_partners.svg") as string;
// const partnersEditIcon = require("app/../assets/images/icon_parnter_edit.svg") as string;
const officeAvatar = require('app/../assets/images/officeAvatar.jpg') as string;
const searchBlue = require("app/../assets/images/searchBlue.svg") as string;


namespace Points {
  export interface Props extends RouteComponentProps<any, any> {
    partnersActions: typeof PartnersActions,
    partners: any
  }
}

@connect(
  (state: RootState): Pick<Points.Props, any> => {
    return {
      partners: state.partners.partners.items,
    };
  },
  (dispatch: Dispatch<any, any>): Pick<Points.Props, any> => ({
    partnersActions: bindActionCreators(PartnersActions, dispatch)
  })
)

class Points extends MainTemplate<Points.Props, any> {
  static defaultProps: Partial<Points.Props> = {};

  constructor(props: Points.Props, context?: any) {
    super(props, context);
  }

  state = {
    value: 3,
    user: {
      firstName: "Дмитрий",
      secondName: "Заруба",
      role: "superAdmin",
      phone: "+7 902 563 50 45",
      email: "dzaruba@gmail.com"
    },
    showCheckboxes: false,
    height: "100%",
    selectable: false,
    slideIndex: 0,
    toggleToEditOrDeleteFlagId: -1,
    partners: [
      {
        id: 1,
        number:12342345643567,
        clientName: "Зинаида Полюшина",
        account: 1234.00,
        date:'24 апреля 2018 в 10:00',
        point: "Hershey в Плаза"

      },
      {
        id: 2,
        number:12342345643567,
        clientName: "Михаил Кравцов",
        account: 1234.00,
        date:'24 апреля 2018 в 10:00',
        point: "Hershey в Плаза"

      },
      {
        id: 3,
        number:12342345643567,
        clientName: "Андрей Зарин",
        account: 1234.00,
        date:'24 апреля 2018 в 10:00',
        point: "Hershey на Прохорова"

      },
      {
        id: 4,
        number:12342345643567,
        clientName: "Зинаида Полюшина",
        account: 1234.00,
        date:'24 апреля 2018 в 10:00',
        point: "Hershey в Плаза"

      },
      {
        id: 5,
        number:12342345643567,
        clientName: "Зинаида Полюшина",
        account: 1234.00,
        date:'24 апреля 2018 в 10:00',
        point: "Hershey в Плаза"

      },
      {
        id: 6,
        number:12342345643567,
        clientName: "Зинаида Полюшина",
        account: 1234.00,
        date:'24 апреля 2018 в 10:00',
        point: "Hershey в Плаза"

      },
      {
        id: 7,
        number:12342345643567,
        clientName: "Зинаида Полюшина",
        account: 1234.00,
        date:'24 апреля 2018 в 10:00',
        point: "Hershey в Плаза"

      },
      {
        id: 8,
        number:12342345643567,
        clientName: "Зинаида Полюшина",
        account: 1234.00,
        date:'24 апреля 2018 в 10:00',
        point: "Hershey в Плаза"

      },
      {
        id: 9,
        number:12342345643567,
        clientName: "Зинаида Полюшина",
        account: 1234.00,
        date:'24 апреля 2018 в 10:00',
        point: "Hershey в Плаза"

      },
      {
        id: 10,
        number:12342345643567,
        clientName: "Зинаида Полюшина",
        account: 1234.00,
        date:'24 апреля 2018 в 10:00',
        point: "Hershey в Плаза"

      },
      {
        id: 11,
        number:12342345643567,
        clientName: "Зинаида Полюшина",
        account: 1234.00,
        date:'24 апреля 2018 в 10:00',
        point: "Hershey в Плаза"

      }
    ],
    userForModal: {
      id: "",
      number: 0,
      clientName: "",
      account: 0,
      date:'',
      point:''
    },

    selectedCity: "",
    searchEnableField: false,

  };

  moveToThePartner(id: any) {
    browserHistory.push(`/partners/${id}/users-partners`)
  }

  onChangeCitySelect(ev: any) {
    let city = ev.target.value;
    this.setState({ selectedCity: city });

  }

  toggleToEditOrDelete(index: number, ev: any) {
    if (ev) {
      ev.preventDefault();
      ev.stopPropagation();
    }
    if (this.state.toggleToEditOrDeleteFlagId === index) {
      index = -1;
    }
    //console.log(12343546657, this.state.toggleToEditOrDeleteFlagId);
    this.setState({ toggleToEditOrDeleteFlagId: index });

  }


  searchEnableFieldMethod() {
    this.setState({ searchEnableField: true });
  }

  onBlurSearchField() {
    this.setState({ searchEnableField: false })
  }


  renderHeader() {
    return <div>
      <div className={'header-top'}>
        <div className={'search-bar'}>
          <img src={searchBlue} className={'search-img'} alt=""
               {...{ onTouchTap: this.searchEnableFieldMethod.bind(this) }}
          />
          {
            this.state.searchEnableField && <TextField
              name={'headerSearch'}
              className={'search-text'}
              hintText=""
              placeholder={'Поиск...'}
              fullWidth={true}
              ref="search"
              onBlur={this.onBlurSearchField.bind(this)}
              autoFocus={true}
            />
          }
        </div>

        <Link to="/profile">
          <img className="avatar" src={officeAvatar} alt=""/>
        </Link>
      </div>


      <div className={'header-middle'}>


      </div>
      <div className={'header-bottom'}>
        <h2 className={'title'}>Чеки</h2>
      </div>
    </div>
  }

  renderContent() {
    let partners = this.state.partners;

    // if (this.state.selectedCity) {
    //   partners = partners.filter(p => p.city === this.state.selectedCity);
    // }

    return <div>
      <div className="row"
				// style={{ flex: 1 }}
           style={{ flex: 1, background: 'white', height: 'calc(100% - 170px - 50px)' }}
      >
        <div className="col-lg-12" style={{ backgroundColor: "white", height: "100%" }}>
          <div>
            <Table height={this.state.height} selectable={this.state.selectable}>
              <TableHeader displaySelectAll={this.state.showCheckboxes}
                           adjustForCheckbox={this.state.showCheckboxes}
              >
                <TableRow>
                  <TableHeaderColumn>№</TableHeaderColumn>
                  <TableHeaderColumn>Номер</TableHeaderColumn>
                  <TableHeaderColumn>Клиент</TableHeaderColumn>
                  <TableHeaderColumn>Сумма</TableHeaderColumn>
                  <TableHeaderColumn>Дата</TableHeaderColumn>
                  <TableHeaderColumn>Точка</TableHeaderColumn>
                </TableRow>
              </TableHeader>
              <TableBody displayRowCheckbox={this.state.showCheckboxes}
              >
                {partners.map((item, index) => {
                  return <TableRow key={index} className="tableOnHover">
                    <TableRowColumn style={{color: "#464e56",height:'80px' }}>{index + 1}</TableRowColumn>
                    <TableRowColumn style={{color: "#464e56" }} >{item.number}</TableRowColumn>
                    <TableRowColumn style={{color: "#464e56" }}>{item.clientName}</TableRowColumn>
                    <TableRowColumn style={{color: "#464e56" }}>{item.account}</TableRowColumn>
                    <TableRowColumn style={{color: "#464e56" }} > {item.date} </TableRowColumn>
                    <TableRowColumn style={{color: "#464e56" }} > {item.point} </TableRowColumn>
                  </TableRow>;
                })}
              </TableBody>
            </Table>


          </div>



        </div>
      </div>






    </div>
  }

}

export default Points;