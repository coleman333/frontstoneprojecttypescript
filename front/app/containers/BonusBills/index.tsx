import * as React from 'react';
import  './style.scss';
import { connect } from 'react-redux';
import { Dispatch } from 'redux';
// bindActionCreators,
import { RouteComponentProps, browserHistory,Link } from 'react-router';
import { RootState } from 'app/reducers';

import MainTemplate from '../MainTemplate';
//const $ = require("jquery");
// declare var $ : any;
// window = require('jquery')(window);
// const $ = require('jquery')(require("jsdom").jsdom().parentWindow);

const { Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn } = require("material-ui/Table");
const { Tabs, Tab, TextField } = require("material-ui");
// MenuItem, SelectField, Avatar

// const addPhotoIcon = require("app/../assets/images/icon_add_photo.svg") as string;
// const deleteUserIcon = require("app/../assets/images/icon_delete.svg") as string;
// const passwdEditIcon = require("app/../assets/images/icon_password_edit.svg") as string;
// const supportIcon = require("app/../assets/images/support.svg") as string;
// const settingsIcon = require("app/../assets/images/icon_more.svg") as string;
// const markerList = require("app/../assets/images/icon_add_photo.svg") as string;
// const arrowDownIcon = require( 'app/../images/arrow down.svg') as string;
// const plusIcon = require("app/../assets/images/icon_plus.svg") as string;
// const grayViewListIcon = require("app/../assets/images/view-list.svg") as string;
// const blueViewTableIcon = require("app/../assets/images/view-table.svg") as string;
// const blueViewListIcon = require("app/../assets/images/view-list-blue.svg") as string;
// const grayViewTableIcon = require("app/../assets/images/view-table-gray.svg") as string;
// const buttonDelete = require("app/../assets/images/button_delete.svg") as string;
// const buttonEdit = require("app/../assets/images/button_edit.svg") as string;
// const photoBox = require("app/../assets/images/photo_box.svg") as string;
const officeAvatar = require('app/../assets/images/officeAvatar.jpg') as string;
// const partnersIcon = require("app/../assets/images/icon_partners.svg") as string;
// const partnersEditIcon = require("app/../assets/images/icon_parnter_edit.svg") as string;
const searchBlue = require("app/../assets/images/searchBlue.svg") as string;
const newRequestIcon = require("app/../assets/images/adminRole.svg") as string;
// const marketologRoleIcon = require("app/../assets/images/marketologRole.svg") as string;
const suspendiertIcon = require("app/../assets/images/operatorRole.svg") as string;
const hershyesIcon = require("app/../assets/images/hershes.png") as string;
const closeCrossBlack = require("app/../assets/images/close_button_search_black.svg") as string;
const checkIcon = require("app/../assets/images/check_icon.svg") as string;
const pauseIcon = require("app/../assets/images/icon_pause.svg") as string;


namespace BonusBills {
	export interface Props extends RouteComponentProps<any, any> {
	}
}

@connect(
	(state: RootState): Pick<BonusBills.Props, any> => {
		return {};
	},
	(dispatch: Dispatch<any, any>): Pick<BonusBills.Props, any> => ({})
)

class BonusBills extends MainTemplate<BonusBills.Props, any> {
	static defaultProps: Partial<BonusBills.Props> = {};

	state = {
		showCheckboxes: false,
		height: '100%',
		selectable: false,
		slideIndex: 1,

		partners: [
			{
				id: 1,
				partnerLogo: hershyesIcon,
				partnerName: 'partner1',
				date: '23 мая 2018 10:52',
				bonusRequest: '12345',
				iconStatus: newRequestIcon,
				status: 'новая'
			},
			{
				id: 2,
				partnerLogo: hershyesIcon,
				partnerName: 'partner2',
				date: '23 мая 2018 10:52',
				bonusRequest: '12345',
				iconStatus: newRequestIcon,
				status: 'новая'
			},
			{
				id: 3,
				partnerLogo: hershyesIcon,
				partnerName: 'partner3',
				date: '23 мая 2018 10:52',
				bonusRequest: '12345',
				iconStatus: suspendiertIcon,
				status: 'приостановлена'
			},
			{
				id: 4,
				partnerLogo: hershyesIcon,
				partnerName: 'partner4',
				date: '23 мая 2018 10:52',
				bonusRequest: '12345',
				iconStatus: newRequestIcon,
				status: 'новая'
			},
			{
				id: 5,
				partnerLogo: hershyesIcon,
				partnerName: 'partner5',
				date: '23 мая 2018 10:52',
				bonusRequest: '12345',
				iconStatus: suspendiertIcon,
				status: 'приостановлена'
			},
			{
				id: 6,
				partnerLogo: hershyesIcon,
				partnerName: 'partner6',
				date: '23 мая 2018 10:52',
				bonusRequest: '12345',
				iconStatus: newRequestIcon,
				status: 'новая'
			},
			{
				id: 7,
				partnerLogo: hershyesIcon,
				partnerName: 'partner7',
				date: '23 мая 2018 10:52',
				bonusRequest: '12345',
				iconStatus: newRequestIcon,
				status: 'новая'
			},
			{
				id: 8,
				partnerLogo: hershyesIcon,
				partnerName: 'partner8',
				date: '23 мая 2018 10:52',
				bonusRequest: '12345',
				iconStatus: suspendiertIcon,
				status: 'приостановлена'
			},
			{
				id: 6,
				partnerLogo: hershyesIcon,
				partnerName: 'partner6',
				date: '23 мая 2018 10:52',
				bonusRequest: '12345',
				iconStatus: newRequestIcon,
				status: 'новая'
			},
			{
				id: 7,
				partnerLogo: hershyesIcon,
				partnerName: 'partner7',
				date: '23 мая 2018 10:52',
				bonusRequest: '12345',
				iconStatus: newRequestIcon,
				status: 'новая'
			},
			{
				id: 8,
				partnerLogo: hershyesIcon,
				partnerName: 'partner8',
				date: '23 мая 2018 10:52',
				bonusRequest: '12345',
				iconStatus: suspendiertIcon,
				status: 'приостановлена'
			},

		],
		selectValueRoleUser: 2,

		userForModal: {
			id: "",
			partnerLogo: "",
			partnerName: "",
			date: "",
			bonusRequest: "",
			iconStatus: "",
			status: ""

		},
		user: {
			firstName: 'Дмитрий',
			secondName: 'Заруба',
			role: 'superAdmin',
			phone: '+7 902 563 50 45',
			email: 'dzaruba@gmail.com',
			avatar: '../../images/officeAvatar.jpg'
		},
		selectedPartner: "",
		value: 1,
		searchEnableField: false,
		showSidePanel: false,
    isSidePanelOpen:false
	};

	constructor(props: BonusBills.Props, context?: any) {
		super(props, context);
	}

	redirectToProfile() {
		browserHistory.push('/profile');
	}

	searchEnableFieldMethod() {
		this.setState({ searchEnableField: true });
	}

	onBlurSearchField() {
		this.setState({ searchEnableField: false })
	}

	tabChange = (value: any) => {
		this.setState({
			slideIndex: value
		});

	};

	openSideUserPanel(user: any) {
		// ev.stopPropagation();
		this.setState({ userForModal: user });
		this.setState({ showSidePanel: true });
		console.log(1234, user);
		console.log(12345678, this.state.userForModal);

	}

	closeSideUserPanel() {
		this.setState({ isSidePanelOpen: false });
	}

  toggleSidePanel(user: any, isOpened: boolean, ev: any) {
    if (ev) {
      ev.stopPropagation();
    }
    if (this.state.isSidePanelOpen === isOpened) {
      return;
    }
    this.setState({
      userForModal: user,
      isSidePanelOpen: isOpened
    })
  }

  conformRequest(user:any){

  }

  renderHeader() {
		return 	<div>
			<div className={'header-top'}>
				<div className={'search-bar'}>
					<img src={searchBlue} className={'search-img'} alt=""
							 {...{ onTouchTap: this.searchEnableFieldMethod.bind(this) }}
					/>
					{
						this.state.searchEnableField && <TextField
							name={'headerSearch'}
							className={'search-text'}
							hintText=""
							placeholder={'Поиск...'}
							fullWidth={true}
							ref="search"
							onBlur={this.onBlurSearchField.bind(this)}
							autoFocus={true}
						/>
					}
				</div>

				<Link to="/profile">
					<img className="avatar" src={officeAvatar} alt=""/>
				</Link>
			</div>

			<div className={'header-middle'} style={{ height: 55 }}>

			</div>

			<div className="header-bottom" style={{marginTop:'-1%'}}>
				<h2 className="title" style={{ marginLeft: '2%', font: 'bold' }}>Бонусные счета</h2>
				<div style={{ width: "330px", }} className="bonusListTabsMarginLeft">
					<Tabs
						tabItemContainerStyle={{ backgroundColor: "transparent" }}
						inkBarStyle={{ backgroundColor: "blue", height: "4px" }}
						onChange={this.tabChange} value={this.state.slideIndex}>
						<Tab label="Все" value={1} style={{ color: 'blue', fontSize: '10px' }}> </Tab>
						<Tab label="Новые" value={2} style={{ color: 'blue', fontSize: '10px' }}> </Tab>
						<Tab label="приостановленные" value={3} style={{ color: 'blue', fontSize: '10px' }}> </Tab>
					</Tabs>

				</div>
			</div>
	</div>
	}

	renderContent() {
		let partners = this.state.partners;

		return <div>
			<div className="row"
					 style={{ flex: 1, minHeight: 'calc(100% - 220px)', background: 'white' }}
				// style={{flexDirection: 'column', justifyContent: 'spaceBetween', overflow: 'auto'}}
			>
				<div className="col-lg-12"
						 style={{ backgroundColor: "white", height: '100%' }}
					// style={{backgroundColor: "white", minHeight: 'calc(100% - 203px)'}}
				>
					<div>
						<Table height={this.state.height} selectable={this.state.selectable}>
							<TableHeader displaySelectAll={this.state.showCheckboxes}
													 adjustForCheckbox={this.state.showCheckboxes}
							>
								<TableRow>
									{/*<TableHeaderColumn>№</TableHeaderColumn>*/}
									<TableHeaderColumn>Партнер</TableHeaderColumn>
									<TableHeaderColumn></TableHeaderColumn>
									<TableHeaderColumn>Дата поступления</TableHeaderColumn>
									<TableHeaderColumn>Запрос бонусов</TableHeaderColumn>
									<TableHeaderColumn>Статус</TableHeaderColumn>
								</TableRow>
							</TableHeader>
							<TableBody displayRowCheckbox={this.state.showCheckboxes}
							>
								{partners.map((item, index) => {
									return <TableRow key={index} className="tableOnHover" style={{ height: '80px' }}
                                   {...{ onTouchTap: this.toggleSidePanel.bind(this, item,true) }}>
										{/*<TableRowColumn>{index + 1}</TableRowColumn>*/}
										<TableRowColumn><img src={item.partnerLogo} style={{ width: '30%'}} alt=""/></TableRowColumn>
										<TableRowColumn>{item.partnerName}</TableRowColumn>
										<TableRowColumn>{item.date}</TableRowColumn>
										<TableRowColumn>{item.bonusRequest} </TableRowColumn>
										<TableRowColumn><img src={item.iconStatus} style={{ width: '15px' }}
																				 alt=""/>{item.status} </TableRowColumn>
									</TableRow>
								})}


							</TableBody>
						</Table>


					</div>
				</div>
			</div>




		</div>
	}
  renderSidePanel() {
    return <div className={'inner-panel'}>

        <div className="col-lg-12">
          <div className="row">
            <div className="col-lg-1" style={{ marginTop: '2%',cursor:'pointer' }}>
              <img src={closeCrossBlack} style={{ width: '25px' }}
                   {...{ onTouchTap: this.closeSideUserPanel.bind(this) }} alt=""/>
            </div>
            <div className="col-lg-4" style={{ marginTop: '3%' }}>
              <div>
                <span style={{ color: '#8c96a0', fontSize: '10px' }}>ID заявки</span>
              </div>
              <div>
                <span style={{ fontSize: '14px' }}>{this.state.userForModal.id}</span>
              </div>

              <div>
                <span style={{ color: '#8c96a0', fontSize: '10px' }}>Дата подачи заявки</span>
              </div>
              <div>
                <span style={{ fontSize: '14px' }}>{this.state.userForModal.date}</span>
              </div>

            </div>
            <div className="offset-lg-2 col-lg-4 column" style={{ marginTop: '3%' }}>
              <div>
                <button className="btn-xs btn-success" style={{ borderRadius: '25px', width: '100%' }}
                        {...{onTouchTap:this.conformRequest.bind(this,this.state.userForModal)}}>
                  <img src={checkIcon} style={{ width: '15px' }} alt=""/>
                  <span style={{ fontSize: '10px' }}>Одобрить запрос</span></button>
              </div>
              <br/>
              <div>
                <button className="btn-xs btn-secondary"
                        style={{ borderRadius: '25px', width: '100%' }}>
                  <img src={pauseIcon} style={{ width: '15px' }} alt=""/>
                  <span style={{ fontSize: '10px' }}>Приостановить</span></button>
              </div>
              <div style={{ marginTop: '5%' }}>
                <button className="btn-xs btn-secondary"
                        style={{ borderRadius: '25px', width: '100%' }}>
                  <img src={checkIcon} style={{ width: '15px' }} alt=""/>
                  <span style={{ fontSize: '10px' }}>Отклонить</span></button>
              </div>


            </div>

          </div>

          <div style={{ height: '5%', backgroundColor: 'black', marginTop: '5%' }}>
            <span style={{ marginLeft: '12%', color: '#8c96a0', fontSize: '10px' }}>Детально</span>
          </div>
          <div style={{ marginTop: '5%', marginLeft: '9%' }}>
            <img src={this.state.userForModal.partnerLogo} alt="" style={{ width: '20%' }}/>

          </div>
          <div style={{ marginTop: '5%', marginLeft: '9%' }}>
            <span>{this.state.userForModal.partnerName}</span>
          </div>
          <div style={{ marginTop: '5%', marginLeft: '9%' }}>
            <span style={{ color: '#8c96a0' }}>Количество запрашиваемых бонусов</span>
          </div>
          <div style={{ marginLeft: '9%' }}>
            <span style={{ fontSize: '30px' }}>{this.state.userForModal.bonusRequest}</span>
          </div>
        </div>

      {/*</div>*/}


    </div>
  }


}

export default BonusBills;