import * as React from 'react';
import { connect } from 'react-redux';
import { Dispatch } from 'redux';
// bindActionCreators,
import { RouteComponentProps } from 'react-router';
import { RootState } from 'app/reducers';
// styles
// import './style.scss';

// icons
// const profileIcon = require('app/../assets/images/icon_profile.svg') as string;
// const partnersIcon = require('app/../assets/images/icon_partners.svg') as string;
// const usersIcon = require('app/../assets/images/icon_users.svg') as string;
// const dmpIcon = require('app/../assets/images/icon_DMP.svg') as string;
// const menuCollapseIcon = require('app/../assets/images/burger_menu_collapse.svg') as string;
// const menuExpandIcon = require('app/../assets/images/burger_menu_expand.svg') as string;

namespace DMP {
	export interface Props extends RouteComponentProps<any, any> {
	}

	export interface State {
		toggleMenu: boolean,
	}
}

@connect(
	(state: RootState): Pick<DMP.Props, any> => {
		return {};
	},
	(dispatch: Dispatch<any, any>): Pick<DMP.Props, any> => ({})
)

class DMP extends React.Component<DMP.Props, DMP.State> {
	static defaultProps: Partial<DMP.Props> = {};

	state = {
		toggleMenu: false,
		active:1

	};

	constructor(props: DMP.Props, context?: any) {
		super(props, context);
	}



	render() {
		return(
			<div>
				this is DMP page template
			</div>
		)

	}
}

export default DMP;