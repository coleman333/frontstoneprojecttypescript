import * as React from "react";
import { connect } from "react-redux";
import { Dispatch } from "redux";
// bindActionCreators,
import { RouteComponentProps } from "react-router";
import { RootState } from "app/reducers/index";

import MainTemplate from '../MainTemplate';
//const $ = require("jquery");
// import * as style from './style.scss';
import './style.scss';


// const { Tabs, Tab, TextField } = require("material-ui");
// const { Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn } = require("material-ui/Table");
// const { MenuItem}= require('material-ui/MenuItem');
// const {SelectField} = require('material-ui/SelectField');


// const editIcon = require('app/../assets/images/icon_edit.svg') as string;
// const editProfileIcon = require('app/../assets/images/icon_profile_edit.svg') as string;
// const addPhotoIcon = require('app/../assets/images/icon_add_photo.svg') as string;
// const deleteUserIcon = require('app/../assets/images/icon_delete.svg') as string;
// const passwdEditIcon = require('app/../assets/images/icon_password_edit.svg') as string;

// const addPhotoIcon = require("app/../assets/images/icon_add_photo.svg") as string;
// const deleteUserIcon = require("app/../assets/images/icon_delete.svg") as string;
// const passwdEditIcon = require("app/../assets/images/icon_password_edit.svg") as string;
// const supportIcon = require("app/../assets/images/support.svg") as string;
// const settingsIcon = require("app/../assets/images/icon_more.svg") as string;
// const markerList = require("app/../assets/images/icon_add_photo.svg") as string;
// // const arrowDownIcon = require( 'app/../images/arrow down.svg') as string;
// const plusIcon = require("app/../assets/images/icon_plus.svg") as string;
// const grayViewListIcon = require("app/../assets/images/view-list.svg") as string;
// const blueViewTableIcon = require("app/../assets/images/view-table.svg") as string;
// const blueViewListIcon = require("app/../assets/images/view-list-blue.svg") as string;
// const grayViewTableIcon = require("app/../assets/images/view-table-gray.svg") as string;
// const buttonDelete = require("app/../assets/images/button_delete.svg") as string;
// const buttonEdit = require("app/../assets/images/button_edit.svg") as string;
// const photoBox = require("app/../assets/images/photo_box.svg") as string;
// // const officeAvatar = require( 'app/../images/officeAvatar.jpg') as string;
// const partnersIcon = require("app/../assets/images/icon_partners.svg") as string;
// const partnersEditIcon = require("app/../assets/images/icon_parnter_edit.svg") as string;


namespace ERP {
	export interface Props extends RouteComponentProps<any, any> {

	}

}

@connect(
	(state: RootState): Pick<ERP.Props, any> => {
		return {};
	},
	(dispatch: Dispatch<any, any>): Pick<ERP.Props, any> => ({})
)

class ERP extends MainTemplate<ERP.Props, any> {
	static defaultProps: Partial<ERP.Props> = {};

	constructor(props: ERP.Props, context?: any) {
		super(props, context);
		console.log('asdfasdfasdfasdf');
	}

	state = {
		value: 3,
		user: {
			firstName: "Дмитрий",
			secondName: "Заруба",
			role: "superAdmin",
			phone: "+7 902 563 50 45",
			email: "dzaruba@gmail.com"
		},
		showCheckboxes: false,
		height: "600px",
		selectable: false,
		slideIndex: 0,
		toggleToEditOrDeleteFlagId: -1,
		// partners: [
		//   {
		//     logo: addPhotoIcon,
		//     name: "companyName1",
		//     city: "city1",
		//     contact: "name1"
		//   },
		//   {
		//     logo: deleteUserIcon,
		//     name: "companyName2",
		//     city: "city2",
		//     contact: "name2"
		//   },
		//   {
		//     logo: passwdEditIcon,
		//     name: "companyName3",
		//     city: "city3",
		//     contact: "name3"
		//   },
		//   {
		//     logo: passwdEditIcon,
		//     name: "companyName4",
		//     city: "city4",
		//     contact: "name4"
		//   },
		//   {
		//     logo: passwdEditIcon,
		//     name: "companyName5",
		//     city: "city5",
		//     contact: "name5"
		//   },
		//   {
		//     logo: passwdEditIcon,
		//     name: "companyName6",
		//     city: "city6",
		//     contact: "name6"
		//   }
		// ],
		userForModal: {
			logo: "",
			contact: "",
			city: "",
			name: ""
		},
		selectedCity: ""

	};


	renderHeader() {
		return <div className={'row'} style={{ height: '100%', backgroundColor: 'white' }}>
			<div className="col-lg-12" style={{ backgroundColor: 'white' }}>
				<div style={{ marginTop: '8%' }}>
              <span
								style={{ marginLeft: '5%', color: '#5a5a5a', fontSize: '14px' }}>Простой Мир Управление</span>
				</div>
				<div>
              <span style={{
								marginLeft: '5%',
								color: '#6caaff',
								fontSize: '30px',
								fontWeight: 600
							}}>ERP-системы </span>
				</div>

			</div>
		</div>
	}

	renderContent() {
		return <div>content</div>
	}

}

export default ERP;