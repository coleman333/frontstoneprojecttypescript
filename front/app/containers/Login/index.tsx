import * as React from "react";
import { connect } from "react-redux";
import { bindActionCreators, Dispatch } from "redux";
// bindActionCreators,
import { RouteComponentProps } from "react-router";
import { RootState } from "app/reducers";

// styles
import "./style.scss";

import * as _ from "lodash" ;

import { TextField } from "material-ui";
import { AuthActions } from "app/actions/auth";
// icons
const support = require("app/../assets/images/support.svg")as string;
const enter = require("app/../assets/images/key_enter.svg")as string;

// const confirmIcon =require ('app/../assets/images/confirmation.svg') as string;
// const enter = require ('../../images/key_enter.svg')as string;
// const profileIcon = require('app/../assets/images/icon_profile.svg') as string;
// const partnersIcon = require('app/../assets/images/icon_partners.svg') as string;
// const usersIcon = require('app/../assets/images/icon_users.svg') as string;
// const dmpIcon = require('app/../assets/images/icon_DMP.svg') as string;
// const menuCollapseIcon = require('app/../assets/images/burger_menu_collapse.svg') as string;
// const menuExpandIcon = require('app/../assets/images/burger_menu_expand.svg') as string;

namespace Login {
	export interface Props extends RouteComponentProps<any, any> {
		authActions: typeof AuthActions,
		triesLeft: number,
		userId:number
	}

	// export interface State {
	//   toggleMenu: boolean,
	// }
}

@connect(
	(state: RootState): Pick<Login.Props, any> => {
		return {
			triesLeft: state.auth.triesLeft
		};
	},
	(dispatch: Dispatch<any, any>): Pick<Login.Props, any> => ({
		authActions: bindActionCreators(AuthActions, dispatch)
	})
)

class Login extends React.Component<Login.Props> {
	static defaultProps: Partial<Login.Props> = {};

	constructor(props: Login.Props, context?: any) {
		super(props, context);
	}

	state = {
		dropPasswordDialog: {
			open: false
		}as any

	};

	toggleDropPasswordDialog(value: any) {
		if (this.state.dropPasswordDialog.open === value) {
			return;
		}

		this.state.dropPasswordDialog.open = value;
		this.forceUpdate();
	}

	submitLogin() {
		// let userData = this.refs.logForm.getCurrentValues();
		// let userData2 = this.refs.email.state.value;
		// console.log(userData2);
		if (!this.props.triesLeft) {
			return;
		}

		const userData = {
			email: (this.refs.email as any).input.value,
			password: (this.refs.password as any).input.value
		};
		console.log(userData);
		this.props.authActions.login(userData);

		const {userId} = this.props;
		if(userId){
			console.log(userId);
      this.toggleDropPasswordDialog.bind(this, !_.get(this.state, "dropPasswordDialog.open"))
		}
		// console.log(12341234523456435,userData);



    // browserHistory.push("/verification");
	}

	render() {
		const animateClass = _.get(this.state, "dropPasswordDialog.open") ? "animate" : "disanimate";
		console.log('triesLeft', this.props.triesLeft);

		return (
			<div className={'row login-page'}>
				<div className={'login-dialog'}>

					<div className={'login-dialog-content-1'}>
						<div style={{ height: "22%", width: "100%", display: "flex" }}>
							<img src={enter} alt="" style={{
								width: "15%",
								height: "25%",
								marginLeft: "5%",
								marginTop: "10%"
							}}/>
							<div style={{
								marginLeft: "2%",
								marginTop: "8%",
								width: "30%"
							}}>
								<h2 className="h2FontStyle">Вход в систему</h2>
							</div>
						</div>
						<hr/>
						<div style={{ marginLeft: "12%", marginRight: "12%", marginTop: "20%" }}>
							<TextField
								hintText=""
								floatingLabelText="ЛОГИН"
								fullWidth={true}
								ref="email"
							/><br/>
							<TextField
								hintText="password"
								floatingLabelText="ПАРОЛЬ"
								fullWidth={true}
								type="password"
								ref="password"
							/><br/>
						</div>
						<div style={{ marginLeft: "12%", marginTop: "5%" }}>
							<button className="btn btnPrimary"
											disabled={!this.props.triesLeft}
											style={{ borderRadius: "20px", width: "30%" }}
											{...{ onTouchTap: this.submitLogin.bind(this) }}>
								Далее
							</button>
						</div>
						<div style={{ marginTop: "30%", marginLeft: "65%" }}>
							<a href="#"
								 >
								забыли пароль?</a>
						</div>
					</div>


					<div className={`${animateClass} login-dialog-content-2`} id="stripe" >
						<div style={{ height: "22%", width: "100%", display: "flex" }}>
							<div style={{
								marginLeft: "20%",
								marginTop: "8%",
								width: "30%",
								font: "bold"
							}}>
								<h2 className="h2FontStyle">Сброс пароля</h2>
							</div>
						</div>
						<hr/>
						<div style={{ marginLeft: "20%", marginRight: "12%", marginTop: "20%" }}>
							<TextField
								hintText=""
								floatingLabelText="Введите E-mail"
								fullWidth={true}
							/><br/>

						</div>
						<div style={{ marginLeft: "20%", marginTop: "5%" }}>
							<button className="btn btnPrimary" style={{ borderRadius: "20px" }}>Отправить</button>
						</div>
					</div>

				</div>

				{/*<div style={{marginLeft: '30%', marginTop: '2%'}}>*/}
				{/*<span style={{fontSize: '12', color: '#4e87e5'}}>Если у вас еще нет аккаунта</span>*/}
				{/*<a href="#">Зарегистрируйтесь</a>*/}
				{/*</div>*/}

				<div className={'login-text-1'}>
					<h5 style={{ color: "white" }}>Простой мир</h5>
					<h1 style={{ color: "white" }}>Управление</h1>
				</div>

				<div className={'login-text-2'}>
					<img src={support} alt="" style={{ height: 50 }}/>
					<div style={{
						display: "flex",
						flexDirection: "column",
						justifyContent: "flex-end",
						marginLeft: 10
					}}>
						<div style={{ fontSize: "12", color: "#86b9ff" }}>Техническая поддержка</div>
						<div>
							<a style={{ color: "white" }} href="#">Guardians@simpleword.com</a>
						</div>
					</div>
				</div>
			</div>

		);
	}
}

export default Login;