import * as React from "react";
import './style.scss';

class MainTemplate<P, S> extends React.Component<P, S> {

	state = {
		isSidePanelOpen: false
	} as any;

	constructor(props: P, context?: any) {
		super(props, context);
	}

	renderHeader() {
		return <div>header</div>
	}

	renderContent() {
		return <div>content</div>
	}

	renderFooter() {
		return <div className={'inner-footer'}>
			<div>
				просто мир
			</div>
			<div>
				<span>Техническая поддержка</span>
				<a href="#" style={{ marginLeft: 5 }}>Guardians@simpleword.com</a>
			</div>
		</div>
	}

	renderSidePanel() {
		return <div></div>
	}

	render() {

		const header = this.renderHeader();
		const content = this.renderContent();
		const footer = this.renderFooter();
		const sidePanel = this.renderSidePanel();

		return (
			<div className={'main-template'}>
				<div className="inner-wrapper">
					<div className={'header'}>{header}</div>
					<div className={'content'} id={'content'}>{content}</div>
					<div className={'footer'}>{footer}</div>
					<div className={`side-panel ${this.state.isSidePanelOpen ? 'show' : 'hide'}`}>
						{sidePanel}
					</div>
				</div>
			</div>
		);
	}
}

export default MainTemplate;