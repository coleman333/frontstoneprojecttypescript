import * as React from 'react';
import { connect } from 'react-redux';
import { Dispatch } from 'redux';
// bindActionCreators,
import { RouteComponentProps, browserHistory } from 'react-router';
import { RootState } from 'app/reducers';
// styles
import './style.scss';

// icons
// const profileIcon = require('app/../assets/images/icon_profile.svg') as string;
// const partnersIcon = require('app/../assets/images/icon_partners.svg') as string;
const usersIcon = require('app/../assets/images/icon_users.svg') as string;
// const dmpIcon = require('app/../assets/images/icon_DMP.svg') as string;
const menuCollapseIcon = require('app/../assets/images/burger_menu_collapse.svg') as string;
const menuExpandIcon = require('app/../assets/images/burger_menu_expand.svg') as string;
const adminIcon = require("app/../assets/images/icon-admin.svg") as string;
const bonusIcon = require("app/../assets/images/icon-bonuses.svg") as string;
const dmp = require("app/../assets/images/icon-dmp.svg") as string;
const partner = require("app/../assets/images/icon-partners.svg") as string;
const recycle = require("app/../assets/images/recycle.svg") as string;
const request = require("app/../assets/images/request.svg") as string;
const exit = require("app/../assets/images/icon_exit.svg") as string;


namespace Navigation {
	export interface Props extends RouteComponentProps<any, any> {
	}

	export interface State {
		toggleMenu: boolean,
	}
}

@connect(
	(state: RootState): Pick<Navigation.Props, any> => {
		return {};
	},
	(dispatch: Dispatch<any, any>): Pick<Navigation.Props, any> => ({})
)

class Navigation extends React.Component<Navigation.Props, Navigation.State> {
	static defaultProps: Partial<Navigation.Props> = {};

	state = {
		toggleMenu: false,
    active:1

  };

	constructor(props: Navigation.Props, context?: any) {
		super(props, context);
	}

	onToggleMenu(value: boolean) {
		if (this.state.toggleMenu === value) {
			return;
		}

		this.setState({ toggleMenu: value });
	}

	goTo(url: string) {
		browserHistory.push(url);
    switch (url) {
      case '/profile':{
      	// this.setState({active:1})
				this.state.active = 1;
      	break;
			}
      case '/partners':{
        // this.setState(active:2)
				this.state.active = 2;
        break;
      }
      case '/administrator':{
        // this.setState(active:3)
				this.state.active = 3;
        break;
      }
      case '/bonus-bills':{
        // this.setState(active:4)
				this.state.active = 4;
				break;
      }
      case '/dmp':{
        // this.setState(active:4)
        this.state.active = 5;
        break;
      }
      case '/basket?filter=all':{
        // this.setState(active:4)
        this.state.active = 6;
        break;
      }
      case '/exit':{
        // this.setState(active:4)
        this.state.active = 7;
        break;
      }
    }
	}

 	render() {
		const { children } = this.props;

		const minimized = this.state.toggleMenu;

		return (
			<div className="row" style={{ height: '100%' }}>
				<div className="left-bar-menu col-2"
						 style={{
							 backgroundColor: '#3e6fe4', maxWidth: minimized ? 64 : 312,
							 display: 'flex', flexDirection: 'column', justifyContent: 'space-between'
						 }}
				>
					<div>
						<img src={this.state.toggleMenu ? menuExpandIcon : menuCollapseIcon} alt=""
								 // style={{ height: 32, maxWidth: 32,marginTop:'16px' }}
							className="menuExpandIconStyle"	 onClick={this.onToggleMenu.bind(this, !minimized)}/>
						<div style={{ padding: '16px 0' }}>
							<div style={({display: minimized ? 'none' : 'block',} as any)} className="divLogoMenu">
								<div>Простой</div>
								<div>Mир</div>
								<img src={usersIcon} alt="" className="logoStyle"
										 />
							</div>
						</div>

						<div className={ this.state.active === 1 ? 'menu-item row active eachRowMenuStyle' :'menu-item row eachRowMenuStyle'}
								 {...{onTouchTap: this.goTo.bind(this, '/profile') }}>
							<div className="col-2 menu-item-icon"	>
								<img src={request} alt=""/>
							</div>
							<div className={`col-10 menuText`} style={{ display: minimized ? 'none' : 'flex' }}>
                <span	>Заявки</span>
							</div>
						</div>
						<div  className={ this.state.active === 2 ? 'menu-item row active eachRowMenuStyle' :'menu-item row eachRowMenuStyle'}
								 {...{ onTouchTap: this.goTo.bind(this, '/partners') }}>
							<div className="col-2 menu-item-icon" >
								<img src={partner} alt=""/>
							</div>
							<div className={`col-10 menuText`} style={{ display: minimized ? 'none' : 'flex' }}>
                <span>Партнеры</span>
							</div>
						</div>
						<div className={ this.state.active ===3 ? 'menu-item row active eachRowMenuStyle' :'menu-item row eachRowMenuStyle'}
								 {...{ onTouchTap: this.goTo.bind(this, '/administrator') }}>
							<div className="col-2 menu-item-icon">
								<img src={adminIcon} alt=""/>
							</div>
							<div className={`col-10 menuText`}  style={{ display: minimized ? 'none' : 'flex' }}>
                <span>Администраторы</span>
							</div>
						</div>
            <div className={ this.state.active === 4 ? 'menu-item row active eachRowMenuStyle' :'menu-item row eachRowMenuStyle'}
                 {...{ onTouchTap: this.goTo.bind(this, '/bonus-bills') }}>
              <div className="col-2 menu-item-icon">
                <img src={bonusIcon} alt=""/>
              </div>
              <div className={`col-10 menuText`} style={{ display: minimized ? 'none' : 'flex' }}>
                <span >Бонусные Счета</span>
              </div>
            </div>
						<div className={ this.state.active === 5 ? 'menu-item row active eachRowMenuStyle' :'menu-item row eachRowMenuStyle'}
                 {...{ onTouchTap: this.goTo.bind(this, '/dmp') }}>
							<div className="col-2 menu-item-icon" >
								<img src={dmp} alt=""/>
							</div>
							<div className={`col-10 menuText`} style={{ display: minimized ? 'none' : 'flex' }}>
                <span>DMP данные</span>
							</div>
						</div>
            <div className={ this.state.active === 6 ? 'menu-item row active eachRowMenuStyle' :'menu-item row eachRowMenuStyle'}
                 {...{ onTouchTap: this.goTo.bind(this, '/basket?filter=all') }}>
              <div className="col-2 menu-item-icon" >
                <img src={recycle} alt=""/>
              </div>
              <div className={`col-10 menuText`} style={{ display: minimized ? 'none' : 'flex' }}>
                <span >Корзина</span>
              </div>
            </div>
					</div>

					<div className={ this.state.active === 7 ? 'menu-item row active eachRowMenuStyle' :'menu-item row eachRowMenuStyle'}
               {...{ onTouchTap: this.goTo.bind(this, '/exit	') }}>
						<div className="col-2 menu-item-icon" >
							<img src={exit} alt=""/>
						</div>
						<div className={`col-10 menuText`} style={{ display: minimized ? 'none' : 'flex' }}>
              <span >Выход</span>
						</div>
					</div>


				</div>
				<div className="col-10 right-bar-extend">
					{children}
				</div>
			</div>
		);
	}
}

export default Navigation;