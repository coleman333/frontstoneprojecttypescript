import * as React from "react";
import { connect } from "react-redux";
import { Dispatch } from "redux";
import { Link } from 'react-router';
// import * as PropTypes from 'prop-types';
// bindActionCreators,
import { RouteComponentProps } from "react-router";
import { RootState } from "app/reducers";


//const $ = require("jquery");
// import * as style from './style.scss';
import './style.scss';

// const profileIcon = require('app/../assets/images/icon_profile.svg') as string;
// const partnersIcon = require('app/../assets/images/icon_partners.svg') as string;
const usersIcon = require('app/../assets/images/icon_users.svg') as string;

// const dmpIcon = require('app/../assets/images/icon_DMP.svg') as string;

// const settingsIcon = require("app/../assets/images/icon_more.svg") as string;
const hershesBrandIcon = require("app/../assets/images/hershes.png") as string;
// const menuCollapseIcon = require('app/../assets/images/burger_menu_collapse.svg') as string;
// const menuExpandIcon = require('app/../assets/images/burger_menu_expand.svg') as string;
// const adminRole = require('assets/images/adminRole.svg') as string;
// const marketologRole = require('assets/images/marketologRole.svg') as string;
const arrowBack = require('app/../assets/images/arroe_back.svg') as string;
const ERP = require('app/../assets/images/icon_ERP.svg') as string;
const points = require('app/../assets/images/icon_stores.svg') as string;
const products = require('app/../assets/images/icon_products.svg') as string;
const bills = require('app/../assets/images/icon_receipts.svg') as string;
const infoIcon = require('app/../assets/images/icon_info.svg') as string;

namespace PartnerBlackMenu {
	export interface Props extends RouteComponentProps<any, any> {

	}

}

@connect(
	(state: RootState): Pick<PartnerBlackMenu.Props, any> => {
		return {};
	},
	(dispatch: Dispatch<any, any>): Pick<PartnerBlackMenu.Props, any> => ({})
)

class PartnerBlackMenu extends React.Component<PartnerBlackMenu.Props> {
	static defaultProps: Partial<PartnerBlackMenu.Props> = {};

	constructor(props: PartnerBlackMenu.Props, context?: any) {
		super(props, context);
	}

	state = {
		toggleMenu: false,
		active: 1,
		user: {
			id: 3,
			logo: hershesBrandIcon,
			name: "Hershey`s",
			city: "Казань",
			contact: "Михаил Дача",
			registered: '17.04.2018',
		},

	};

	onToggleMenu(value: boolean) {
		if (this.state.toggleMenu === value) {
			return;
		}

		this.setState({ toggleMenu: value });
	}


	render() {
		const { children } = this.props;
		const minimized = this.state.toggleMenu;

		const currentRoutePath = `/partners/${this.state.user.id}`;

		return (
			<div className={'row'} style={{ height: '100%' }}>
				<div className="left-bar-menu col-lg-2"
						 style={{
							 backgroundColor: '#282b34', color: 'white',
							 // maxWidth: minimized ? 50 : undefined,
							 display: 'flex', flexDirection: 'column', justifyContent: 'space-between'
						 }}
				>
					<div >
						{/*<div className={'col-lg-12'} style={{ backgroundColor: '#282b34', color: 'white' }}>*/}
							{/*<img src={this.state.toggleMenu ? menuExpandIcon : menuCollapseIcon} alt=""*/}
							{/*style={{ height: 35, maxWidth: 50 }}*/}
							{/*onClick={this.onToggleMenu.bind(this, !minimized)}/>*/}
							<div style={{ padding: '40px 0', marginTop: '19%' }}>
								<div style={({
									marginLeft: '20%', color: '#fff66c',

									display: minimized ? 'none' : 'block',

									position: 'relative',
									width: '50%'
								} as any)}>

									<img src={this.state.user.logo} alt=""
											 style={{
												 // height: '100%',
												 width: '100%',
												 marginTop: '12	%',

											 }}/>
								</div>
							</div>

							<Link to={`${currentRoutePath}/users-partners`}
										activeClassName='active'
										className={'menu-item row'}
										style={{ minHeight: '10%', display: 'flex', textDecoration: 'none', color: 'inherit' }}

							>
								<div className="col-xs-2 menu-item-icon" style={{ flex: 2, padding: '4%', maxWidth: '19%' }}>
									<img src={usersIcon} alt=""/>
								</div>
								<div className={`col-xs-10`}
										 style={{
											 flex: 10,
											 paddingLeft: 10,
											 alignItems: 'center',
											 display: minimized ? 'none' : 'flex'
										 }}>
									Пользователи
								</div>
							</Link>
							<Link to={`${currentRoutePath}/products`}
										activeClassName='active'
										className={'menu-item row'}
										style={{ minHeight: '10%', display: 'flex', textDecoration: 'none', color: 'inherit' }}
							>
								<div className="col-xs-2 menu-item-icon" style={{ flex: 2, padding: 10, maxWidth: '19%' }}>
									<img src={products} alt=""/>
								</div>
								<div className={`col-xs-10`}
										 style={{
											 flex: 10,
											 paddingLeft: 10,
											 alignItems: 'center',
											 display: minimized ? 'none' : 'flex'
										 }}>
									Товары
								</div>
							</Link>
							<Link to={`${currentRoutePath}/bills`}
										activeClassName='active'
										className={`menu-item row`}
										style={{ minHeight: '10%', display: 'flex', textDecoration: 'none', color: 'inherit' }}
							>
								<div className="col-xs-2 menu-item-icon" style={{ flex: 2, padding: 10, maxWidth: '19%' }}>
									<img src={bills} alt=""/>
								</div>
								<div className={`col-xs-10`}
										 style={{
											 flex: 10,
											 paddingLeft: 10,
											 alignItems: 'center',
											 display: minimized ? 'none' : 'flex'
										 }}>
									Чеки
								</div>
							</Link>
							<Link to={`${currentRoutePath}/points`}
										activeClassName='active'
										className={'menu-item row'}
										style={{ minHeight: '10%', display: 'flex', textDecoration: 'none', color: 'inherit' }}
							>
								<div className="col-xs-2 menu-item-icon" style={{ flex: 2, padding: 10, maxWidth: '19%' }}>
									<img src={points} alt=""/>
								</div>
								<div className={`col-xs-10`}
										 style={{
											 flex: 10,
											 paddingLeft: 10,
											 alignItems: 'center',
											 display: minimized ? 'none' : 'flex'
										 }}>
									Точки
								</div>
							</Link>
							<Link to={`${currentRoutePath}/erp`}
										activeClassName='active'
										className={'menu-item row'}
										style={{ minHeight: '10%', display: 'flex', textDecoration: 'none', color: 'inherit' }}
							>
								<div className="col-xs-2 menu-item-icon" style={{ flex: 2, padding: 10, maxWidth: '19%' }}>
									<img src={ERP} alt=""/>
								</div>
								<div className={`col-xs-10`}
										 style={{
											 flex: 10,
											 paddingLeft: 10,
											 alignItems: 'center',
											 display: minimized ? 'none' : 'flex'
										 }}>
									ERP-системы
								</div>
							</Link>
							<hr/>
              <Link to={`/partners`}
                    // activeClassName='active'
                    className={'menu-item row'}
                    style={{ minHeight: '10%', display: 'flex', textDecoration: 'none', color: 'inherit' }}
              >
                <div className="col-xs-2 menu-item-icon" style={{ flex: 2, padding: 10, maxWidth: '19%' }}>
                  <img src={infoIcon} alt=""/>
                </div>
                <div className={`col-xs-10`}
                     style={{
                       flex: 10,
                       paddingLeft: 10,
                       alignItems: 'center',
                       display: minimized ? 'none' : 'flex'
                     }}>
									<div>
                    <span style={{fontSize:'100%'}}>Информация о партнере</span><br/>
									</div>
                  {/*<div>*/}
                    {/*<span></span>*/}
									{/*</div>*/}

                </div>
              </Link>


            <Link to={`/partners`}
							// activeClassName='active'
                  className={'menu-item row'}
                  style={{ minHeight: '5%', display: 'flex', textDecoration: 'none', color: 'inherit',
                  position:'absolute',bottom:0,right:'0',left:'0'
                  }}
            >
              <div className="col-xs-2 menu-item-icon" style={{ flex: 2, padding: 10,  }}>
                <img src={arrowBack} alt="" style={{maxWidth: 30,maxHeight:30,marginLeft:'30%'}}/>
              </div>
              <div className={`col-xs-10`}
                   style={{
                     flex: 10,
                     paddingLeft: 10,
                     alignItems: 'center',
                     display: minimized ? 'none' : 'flex'
                   }}>
                  <span>К списку парнёров</span>

              </div>
            </Link>

              {/*<div className="menu-item row" style={{*/}
                {/*marginBottom: 20, minHeight: 56*/}
                {/*// display: minimized ? 'flex' : 'none'*/}
              {/*}}>*/}
                {/*<div className="col-xs-2 menu-item-icon" style={{ flex: 2, padding: 10, maxWidth: 50 }}>*/}
                  {/*<img src={dmpIcon} alt=""/>*/}
                {/*</div>*/}
                {/*<div className={`col-xs-10`}*/}
                     {/*style={{*/}
                       {/*flex: 10,*/}
                       {/*paddingLeft: 10,*/}
                       {/*alignItems: 'center',*/}
                       {/*// display: minimized ? 'none' : 'flex'*/}
                     {/*}}>*/}
                  {/*Выход*/}
                {/*</div>*/}
              {/*</div>*/}


						{/*</div>*/}


						<div>
						{/*<img src={settingsIcon} style={{ width: '15%', height: '10%', marginLeft: '80%' }} alt=""/>*/}
						{/*<div style={{*/}
							{/*display: 'flex',*/}
							{/*flexDirection: 'column',*/}
							{/*justifyContent: 'center',*/}
							{/*// marginBottom: '10%',*/}
							{/*width: '100%',*/}
							{/*marginTop:'70%',*/}
							{/*marginLeft:'10%'*/}
						{/*}}>*/}
							{/*<div style={{ lineHeight: '80%' }}>*/}
								{/*<div style={{ color: '#787f93' }}>{this.state.user.name}</div>*/}
								{/*<div style={{ color: '#787f93', fontSize: '80%' }}>{this.state.user.city}</div>*/}
							{/*</div>*/}
              {/*<br/>*/}
							{/*<div style={{ color: '#787f93', fontSize: '80%' }}>Зарегистрирован</div>*/}
							{/*<div style={{ color: '#787f93', fontSize: '80%' }}>{this.state.user.registered}</div>*/}
              {/*<br/>*/}
							{/*<div style={{ color: '#787f93', fontSize: '80%' }}>Контактное лицо</div>*/}
							{/*<div>*/}
                {/*<img src={dmpIcon} style={{width:'5%'}} alt=""/>*/}
                {/*<span>{this.state.user.contact}</span>*/}
							{/*</div>*/}

						{/*</div>*/}
            </div>


						<div>
						{/*<div style={{ marginBottom: 20, display: 'flex', justifyContent: 'center' }}>*/}
						{/*<button className="btn btn-primary"*/}
						{/*style={{*/}
						{/*borderRadius: '20px',*/}
						{/*width: '60%',*/}
						{/*justifyContent: 'center',*/}
						{/*display: minimized ? 'none' : 'flex'*/}
						{/*}}*/}
						{/*>*/}
						{/*Выход*/}
						{/*</button>*/}
						{/*</div>*/}
						</div>


						{/*<div className="menu-item row" style={{*/}
						{/*marginBottom: 20, minHeight: 56,position:'absolute',bottom:0*/}
						{/*// display: minimized ? 'flex' : 'none'*/}
						{/*}}>*/}
							{/*<div className="col-xs-2 menu-item-icon" style={{ flex: 2, padding: 10, maxWidth: 50 }}>*/}
							{/*<img src={dmpIcon} alt=""/>*/}
							{/*</div>*/}
							{/*<div className={`col-xs-10`}*/}
							{/*style={{*/}
							{/*flex: 10,*/}
							{/*paddingLeft: 10,*/}
							{/*alignItems: 'center',*/}
							{/*// display: minimized ? 'none' : 'flex'*/}
							{/*}}>*/}
							{/*Выход*/}
							{/*</div>*/}
						{/*</div>*/}


					</div>
				</div>
        <div className={'col-lg-10 right-bar-extend'}>
					{children}
				</div>
			</div>
		)
	}
}

export default PartnerBlackMenu;