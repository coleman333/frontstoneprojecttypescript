import * as React from "react";
import { connect } from "react-redux";
import { Dispatch, bindActionCreators } from "redux";
// bindActionCreators,
import { RouteComponentProps, browserHistory, Link } from "react-router";
import { RootState } from "app/reducers";
//const $ = require("jquery");
// import * as style from './style.scss';
import './style.scss';
import { PartnersActions } from 'app/actions/partners';

import MainTemplate from '../MainTemplate';

const { Tabs, Tab, TextField } = require("material-ui");
const { Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn } = require("material-ui/Table");
// const { MenuItem}= require('material-ui/MenuItem');
// const {SelectField} = require('material-ui/SelectField');


// const editIcon = require('app/../assets/images/icon_edit.svg') as string;
// const editProfileIcon = require('app/../assets/images/icon_profile_edit.svg') as string;
// const addPhotoIcon = require('app/../assets/images/icon_add_photo.svg') as string;
// const deleteUserIcon = require('app/../assets/images/icon_delete.svg') as string;
// const passwdEditIcon = require('app/../assets/images/icon_password_edit.svg') as string;

const addPhotoIcon = require("app/../assets/images/icon_add_photo.svg") as string;
const deleteUserIcon = require("app/../assets/images/icon_delete.svg") as string;
const passwdEditIcon = require("app/../assets/images/icon_password_edit.svg") as string;
// const supportIcon = require("app/../assets/images/support.svg") as string;
const settingsIcon = require("app/../assets/images/icon_more.svg") as string;
const markerList = require("app/../assets/images/icon_add_photo.svg") as string;
// const arrowDownIcon = require( 'app/../images/arrow down.svg') as string;
const plusIcon = require("app/../assets/images/icon_plus.svg") as string;
const grayViewListIcon = require("app/../assets/images/view-list.svg") as string;
const blueViewTableIcon = require("app/../assets/images/view-table.svg") as string;
const blueViewListIcon = require("app/../assets/images/view-list-blue.svg") as string;
const grayViewTableIcon = require("app/../assets/images/view-table-gray.svg") as string;
const buttonDelete = require("app/../assets/images/button_delete.svg") as string;
const buttonEdit = require("app/../assets/images/button_edit.svg") as string;
const photoBox = require("app/../assets/images/photo_box.svg") as string;
// const officeAvatar = require( 'app/../images/officeAvatar.jpg') as string;
const partnersIcon = require("app/../assets/images/icon_partners.svg") as string;
const partnersEditIcon = require("app/../assets/images/icon_parnter_edit.svg") as string;
const officeAvatar = require('app/../assets/images/officeAvatar.jpg') as string;
const searchBlue = require("app/../assets/images/searchBlue.svg") as string;


namespace Partners {
	export interface Props extends RouteComponentProps<any, any> {
		partnersActions: typeof PartnersActions,
		partners: any
	}
}

@connect(
	(state: RootState): Pick<Partners.Props, any> => {
		return {
			partners: state.partners.partners.items,
		};
	},
	(dispatch: Dispatch<any, any>): Pick<Partners.Props, any> => ({
		partnersActions: bindActionCreators(PartnersActions, dispatch)
	})
)

class Partners extends MainTemplate<Partners.Props, any> {
	static defaultProps: Partial<Partners.Props> = {};

	constructor(props: Partners.Props, context?: any) {
		super(props, context);
	}

	state = {
		value: 3,
		user: {
			firstName: "Дмитрий",
			secondName: "Заруба",
			role: "superAdmin",
			phone: "+7 902 563 50 45",
			email: "dzaruba@gmail.com"
		},
		showCheckboxes: false,
		height: "100%",
		selectable: false,
		slideIndex: 0,
		toggleToEditOrDeleteFlagId: -1,
		partners: [
			{
				id: 1,
				logo: addPhotoIcon,
				name: "companyName1",
				city: "city1",
				contact: "name1"
			},
			{
				id: 2,
				logo: deleteUserIcon,
				name: "companyName2",
				city: "city2",
				contact: "name2"
			},
			{
				id: 3,
				logo: officeAvatar,
				name: "companyName3",
				city: "city3",
				contact: "name3"
			},
			{
				id: 4,
				logo: passwdEditIcon,
				name: "companyName4",
				city: "city4",
				contact: "name4"
			},
			{
				id: 5,
				logo: officeAvatar,
				name: "companyName5",
				city: "city5",
				contact: "name5"
			},
			{
				id: 6,
				logo: passwdEditIcon,
				name: "companyName6",
				city: "city6",
				contact: "name6"
			},
			{
				id: 7,
				logo: passwdEditIcon,
				name: "companyName7",
				city: "city7",
				contact: "name7"
			},
			{
				id: 4,
				logo: passwdEditIcon,
				name: "companyName4",
				city: "city4",
				contact: "name4"
			},
			{
				id: 5,
				logo: passwdEditIcon,
				name: "companyName5",
				city: "city5",
				contact: "name5"
			},
			{
				id: 6,
				logo: passwdEditIcon,
				name: "companyName6",
				city: "city6",
				contact: "name6"
			},
			{
				id: 7,
				logo: passwdEditIcon,
				name: "companyName7",
				city: "city7",
				contact: "name7"
			}
		],
		userForModal: {
			logo: "",
			contact: "",
			city: "",
			name: ""
		},
		selectedCity: "",
		searchEnableField: false,
		// user = {
		//     firstName: 'Дмитрий',
		//     secondName: 'Заруба',
		//     role: 'superAdmin',
		//     phone: '+7 902 563 50 45',
		//     email: 'dzaruba@gmail.com',
		//     avatar: '../../images/officeAvatar.jpg'
		//   }
	};

	tabChange = (value: any) => {
		this.setState({
			slideIndex: value
		});

	};

	moveToThePartner(id: any) {
		browserHistory.push(`/partners/${id}/users-partners`)
	}

	onChangeCitySelect(ev: any) {
		let city = ev.target.value;
		this.setState({ selectedCity: city });

	}


	openAddPartnerWindow() {
		($("#addUserModal") as any).modal({ show: true });
	}

	openEditPartnerWindow(index: number) {
		$("#editUserModal").modal({ show: true });

		this.setState({ userForModal: this.state.partners[index] });
	}

	openDeletePartnerWindow(index: number) {
		$("#deleteUserModal").modal({ show: true });
		this.setState({ userForModal: this.state.partners[index] });
	}

	openDeleteUserProfileWindow() {
		$("#deleteUserModal").modal({ show: true });
		$("#editUserModal").modal("hide");
	}

	toggleToEditOrDelete(index: number, ev: any) {
		if (ev) {
			ev.preventDefault();
			ev.stopPropagation();
		}
		if (this.state.toggleToEditOrDeleteFlagId === index) {
			index = -1;
		}
		//console.log(12343546657, this.state.toggleToEditOrDeleteFlagId);
		this.setState({ toggleToEditOrDeleteFlagId: index });

	}

	submitAddNewPartner() {
		const newPartner: any = {
			name: (this.refs.companyName as any).input.value,
			contact: (this.refs.contact as any).input.value,
			// city: (this.refs.city as any).input.value,
			phone: (this.refs.phone as any).input.value,
			email: (this.refs.email as any).input.value

		};
		$("#addUserModal").modal("hide");

		console.log(123434563456, newPartner);
		this.props.partnersActions.create(newPartner);
	}

	uploadFile() {

	}

	searchEnableFieldMethod() {
		this.setState({ searchEnableField: true });
	}

	onBlurSearchField() {
		this.setState({ searchEnableField: false })
	}


	renderHeader() {
		return <div>
			<div className={'header-top'}>
				<div className={'search-bar'}>
					<img src={searchBlue} className={'search-img'} alt=""
							 {...{ onTouchTap: this.searchEnableFieldMethod.bind(this) }}
					/>
					{
						this.state.searchEnableField && <TextField
							name={'headerSearch'}
							className={'search-text'}
							hintText=""
							placeholder={'Поиск...'}
							fullWidth={true}
							ref="search"
							onBlur={this.onBlurSearchField.bind(this)}
							autoFocus={true}
						/>
					}
				</div>

				<Link to="/profile">
					<img className="avatar" src={officeAvatar} alt=""/>
				</Link>
			</div>


			<div className={'header-middle'}>
				<div style={{ display: "flex", alignItems: "center", color: "#5a5a5a" }}>
					Город:
				</div>
				<select
					className="form-control" id="exampleFormControlSelect1"
					style={{ width: "20%", borderRadius: "20px" }}
					onChange={this.onChangeCitySelect.bind(this)}
				>
					<option value="">Все города</option>
					{
						this.state.partners.map((item, index) => {
							return <option value={item.city} key={index}>{item.city}</option>;
						})
					}
				</select>

				<button className="btn-xs btn-primary"
								{...{ onTouchTap: this.openAddPartnerWindow.bind(this) }}
								style={{ borderRadius: "20px"}}
				>
					<img src={plusIcon} style={{ width: "25px" }} alt=""/>
					Добавить партнеров
				</button>
			</div>


			<div className={'header-bottom'}>
				<h2 className={'title'}>Партнеры</h2>
				<div style={{ width: 100 }}>
					<Tabs
						tabItemContainerStyle={{ backgroundColor: "transparent" }}
						inkBarStyle={{ backgroundColor: "blue", height: "4px" }}
						onChange={this.tabChange} value={this.state.slideIndex}
					>
						{this.state.slideIndex === 1 &&
						<Tab icon={<img src={grayViewListIcon}/>} value={0}>
						</Tab>}
						{this.state.slideIndex === 0 &&
						<Tab icon={<img src={blueViewListIcon}/>} value={0}>
						</Tab>}

						{this.state.slideIndex === 1 &&
						<Tab icon={<img src={blueViewTableIcon}/>} value={1}>
						</Tab>}
						{this.state.slideIndex === 0 &&
						<Tab icon={<img src={grayViewTableIcon}/>} value={1}>
						</Tab>}
					</Tabs>
				</div>
			</div>


		</div>
	}

	renderContent() {
		let partners = this.state.partners;

		if (this.state.selectedCity) {
			partners = partners.filter(p => p.city === this.state.selectedCity);
		}

		return <div>
			<div className="row"
				// style={{ flex: 1 }}
					 style={{ flex: 1, background: 'white', height: 'calc(100% - 170px - 50px)' }}
			>
				<div className="col-lg-12" style={{ backgroundColor: "white", height: "100%" }}>
					{this.state.slideIndex === 0 &&
					<div>
						<Table height={this.state.height} selectable={this.state.selectable}>
							<TableHeader displaySelectAll={this.state.showCheckboxes}
													 adjustForCheckbox={this.state.showCheckboxes}
							>

								<TableRow>
									<TableHeaderColumn>№</TableHeaderColumn>
									<TableHeaderColumn>Лого</TableHeaderColumn>
									<TableHeaderColumn>Наименование</TableHeaderColumn>
									<TableHeaderColumn>Город</TableHeaderColumn>
									<TableHeaderColumn>Контактное лицо</TableHeaderColumn>
									<TableHeaderColumn></TableHeaderColumn>
								</TableRow>
							</TableHeader>
							<TableBody displayRowCheckbox={this.state.showCheckboxes}
							>
								{partners.map((item, index) => {
									return <TableRow key={index} className="tableOnHover">
										<TableRowColumn style={{ height: "80px" }}
																		{...{ onTouchTap: this.moveToThePartner.bind(this, item.id) }}
										>{index + 1}</TableRowColumn>
										<TableRowColumn
											{...{ onTouchTap: this.moveToThePartner.bind(this, item.id) }}>
											<img src={item.logo} alt="" style={{ width: "25px" }}/>
										</TableRowColumn>
										<TableRowColumn {...{ onTouchTap: this.moveToThePartner.bind(this, item.id) }}
										>{item.name}</TableRowColumn>
										<TableRowColumn  {...{ onTouchTap: this.moveToThePartner.bind(this, item.id) }}
										>{item.city}</TableRowColumn>
										<TableRowColumn {...{ onTouchTap: this.moveToThePartner.bind(this, item.id) }} >
											<img src={markerList} alt=""
													 style={{ width: "5px", marginRight: "3%" }}/>
											{item.contact}
										</TableRowColumn>
										{this.state.toggleToEditOrDeleteFlagId !== index && <TableRowColumn>
											<img src={settingsIcon} alt=""
													 onClick={this.toggleToEditOrDelete.bind(this, index)}
													 style={{ width: "25px", marginLeft: "90%" }}
											/></TableRowColumn>}

										{this.state.toggleToEditOrDeleteFlagId === index && <TableRowColumn>
											<div style={{ width: "100%" }}>
												<img src={buttonEdit} alt=""
														 onClick={this.openEditPartnerWindow.bind(this, index)}
														 style={{ marginLeft: '40%', width: '30%' }}
												/>
												<img src={buttonDelete} alt=""
														 onClick={this.openDeletePartnerWindow.bind(this, index)}
														 style={{ width: '30%' }}

												/>
											</div>
										</TableRowColumn>}
									</TableRow>;
								})}


							</TableBody>
						</Table>


					</div>
					}

					{/*myBricks1*/}

					{
						this.state.slideIndex === 1 && <div className="" style={{display:"flex",flexDirection:'row'
							,flexWrap:"wrap",marginLeft:'5%'}}>
							{this.state.partners.map((item, index) => {
								return <div key={index} style={{
									border: "solid 0.5px #ebebeb",boxShadow: "0 0 40px rgba(0,0,0,0.4)"

								}} className="slideUp threed bricksSize">
									<img src={item.logo} alt="" className="imgBricksSize" />
									<hr/>

									<div>
                      <span
											// 	style={{
											// 	display: "table-cell", verticalAlign: "middle",
											// 	textAlign: "center"
											// }}
											>{item.name}</span>
									</div>
									<div>
                      <span style={{ display: "block", margin: "0 auto", color: "#5a5a5a" }}>
                          {item.city}</span>
									</div>
									<div>
										<span style={{ display: "block", margin: "0 auto" }}>{item.contact}</span>
									</div>

								</div>
							})}
						</div>
					}

				</div>
			</div>


			<div {...{ className: "modal fade modalWindow" }as any} id="editUserModal" tabIndex="-1"
					 role="dialog"
					 aria-labelledby="editUserModalLabel" aria-hidden="true" >
				<div className="modal-dialog" role="document">
					<div className="modal-content" style={{ borderRadius: "20px" }}>
						<div className="modal-header">
							<img src={partnersEditIcon} alt="" style={{ width: "10%", marginTop: "10px" }}/>
							<h3 className="modal-title" id="editUserModalLabel"
                  style={{ marginLeft: '5px', fontWeight: 600, fontSize: '100%' }}>
								Редактировать <br/>партнера</h3>

							<button type="button" className="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<div className="modal-body">
							<div>
								<img className="imageClass" src={this.state.userForModal.logo} alt="" style={{
									width: "70px",
									height: "70px",
									borderRadius: "50%",
									backgroundSize: "auto 70px",
									marginLeft: "7%",
									marginTop: "2%"
								}}/>

								<label htmlFor="label123" style={{
									marginLeft: "-4%",
									width: "6%",
									marginTop: "10%",
									cursor: "pointer"
								}}>
									<img src={addPhotoIcon} style={{ width: "35px" }} alt=""/>
									<input type="file" id="label123" style={{ display: "none" }}
												 onChange={this.uploadFile.bind(this)}/>
								</label>

								<img src={deleteUserIcon} alt=""
                     style={{ width: '10%', cursor: 'pointer',marginLeft:'10%' }}/>
								<span style={{ fontSize: '60%', color: '#e6144a', cursor: 'pointer' }}
											{...{ onTouchTap: this.openDeleteUserProfileWindow.bind(this) }}>удалить партнера</span>
							</div>
							<div style={{ marginLeft: "7%", marginRight: "7%", marginTop: "1%" }}>
								<TextField
									hintText={this.state.userForModal.name}
									floatingLabelText="название компании"
									fullWidth={true}
									ref="name"
								/>

								<TextField
									hintText={this.state.userForModal.contact}
									floatingLabelText="Контактное лицо"
									fullWidth={true}
									ref="contact"
								/>
								<TextField
									hintText={this.state.userForModal.city}
									floatingLabelText="Город"
									fullWidth={true}
									ref="city"
								/>
                <button type="button" style={{ borderRadius: '20px', marginLeft: '70%', fontSize: '80%' }}
                        className="btn-xs btn-primary">
                  <span style={{ fontSize: '80%' }}>Сохранить</span>
								</button>
							</div>
						</div>
						{/*<div className="modal-footer">*/}
						{/*<button type="button" style={{borderRadius:'20px',width:'30%'}} className="btn btn-primary">*/}
						{/*Сохранить</button>*/}
						{/*</div>*/}
					</div>
				</div>
			</div>

			<div {...{ className: "modal fade bootstrapWindows modalWindow" }as any} id="addUserModal" tabIndex="-1"
					 role="dialog"
					 aria-labelledby="addUserModalLabel" aria-hidden="true" style={{ marginLeft: '40%', marginTop: '8%' }}>
				<div className="modal-dialog" role="document">
					<div className="modal-content" style={{ borderRadius: "20px" }}>
						<div className="modal-header">
							<img src={partnersIcon} alt="" style={{ width: "10%", marginTop: "10px" }}/>
							<h3 className="modal-title" id="addUserModalLabel"
									style={{ marginLeft: '5px', fontSize: '90%', fontWeight: 600 }}>
								Добавить<br/>пользователя</h3>

							<button type="button" className="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<div className="modal-body">
							<div style={{ display: "flex", flexDirection: "row" }}>
								<img className="imageClass" src={photoBox} alt="" style={{
									width: "70px",
									height: "70px",
									borderRadius: "50%",
									backgroundSize: "auto 70px",
									marginLeft: "7%",
									marginTop: "2%"
								}}/>

								<label htmlFor="label123" style={{
									marginLeft: "-4%",
									width: "6%",
									marginTop: "10%",
									cursor: "pointer"
								}}>

									<img src={markerList} style={{ width: "35px", marginTop: "-305%" }} alt=""/>
									<input type="file" id="label123" style={{ display: "none" }}
												 onChange={this.uploadFile.bind(this)}/>
								</label>
								<div style={{ marginTop: "4%" }}>
                                        <span style={{ marginLeft: "15%", color: "#8c96a0", fontSize: "80%" }}>
                                            Логотип</span><br/>
									<span style={{ marginLeft: "15%", color: "#8c96a0", fontSize: "60%" }}>
                                            В формате PNG или JPG, не более 5Мб </span>
								</div>

							</div>
							<div style={{ marginLeft: "7%", marginRight: "7%", marginTop: "1%" }}>
								<TextField
									hintText=""
									floatingLabelText="Название компании"
									fullWidth={true}
									ref="companyName"
								/>
								<TextField
									hintText=""
									floatingLabelText="Контактное лицо"
									fullWidth={true}
									ref="contact"
								/>
								<TextField
									hintText=""
									floatingLabelText="Телефон"
									// floatingLabelText="Введите номер мобильго телефона"
									fullWidth={true}
									ref="phone"
								/>
								<TextField
									hintText=""
									floatingLabelText="E-mail"
									// floatingLabelText="Введите номер мобильго телефона"
									fullWidth={true}
									ref="email"
								/>
								<button type="button"
                        style={{ borderRadius: '20px', width: '40%', marginLeft: '60%', marginTop: '10%' }}
												{...{ onTouchTap: this.submitAddNewPartner.bind(this) }}
												className="btn-xs btn-primary">
                  <span style={{ fontSize: '80%' }}>Сохранить</span>
								</button>
							</div>
						</div>
						{/*<div className="modal-footer">*/}
						{/*<button type="button" style={{borderRadius:'20px',width:'30%'}} className="btn btn-primary">*/}
						{/*Сохранить</button>*/}
						{/*</div>*/}
					</div>
				</div>
			</div>

			<div {...{ className: "modal fade bootstrapWindows modalWindow" }as any} id="deleteUserModal" tabIndex="-1"
					 role="dialog"
					 aria-labelledby="deleteUserModalLabel" aria-hidden="true" style={{ marginLeft: '40%', marginTop: '8%' }}>
				<div className="modal-dialog" role="document">
					<div className="modal-content" style={{ borderRadius: "20px" }}>
						<div className="modal-header">
							<img src={deleteUserIcon} alt=""  style={{ width: '10%', marginTop: '10px' }}/>
							<h3 className="modal-title" id="deleteUserModalLabel"
                  style={{ marginLeft: '5px', fontWeight: 600, fontSize: '100%' }}>
								Удаление<br/>пользователя</h3>

							<button type="button" className="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<div className="modal-body">
							<div style={{ marginLeft: "7%", marginRight: "7%", marginTop: "1%" }}>

								<label htmlFor="" style={{ fontSize: "0.7vw" }}>Вы действительно хотите удалить
									парнера?</label>

								<div>
									<h3>{this.state.userForModal.name} </h3><br/>
									<h3>{this.state.userForModal.contact} </h3><br/>
									{/*<span>{user.role}</span>*/}
								</div>
								<br/><br/><br/>
								<div style={{ display: "flex", flexDirection: "row", justifyContent: "flex-end" }}>
									<div className="" style={{ width: "35%" }}>
										<button type="button"
														style={{
															borderRadius: "20px", width: "100%",
															marginTop: "10%", marginBottom: "2%"
														}}
														className="btn-xs btn-secondary">
											Отмена
										</button>
									</div>
									<div className="" style={{ width: "35%" }}>
										<button type="button"
														style={{
															borderRadius: "20px", marginLeft: "8%", marginTop: "10%"
															, width: "100%", marginBottom: "2%"
														}}
														className="btn-xs btn-danger">
											Удалить
										</button>
									</div>
								</div>
							</div>

						</div>

					</div>
				</div>
			</div>
		</div>
	}

}

export default Partners;