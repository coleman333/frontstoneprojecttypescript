import * as React from 'react';
import './style.scss';
import { connect } from 'react-redux';
import { bindActionCreators, Dispatch } from "redux";
// bindActionCreators,
import { browserHistory, RouteComponentProps, Link } from "react-router";
import { RootState } from 'app/reducers';
import { PartnersActions } from 'app/actions/partners';
// import {Avatar} from 'material-ui/Avatar';
import * as _ from 'lodash';
//const $ = require("jquery");
// declare var $ : any;
// window = require('jquery')(window);

// const $ = require('jquery')(require("jsdom").jsdom().parentWindow);
import MainTemplate from '../MainTemplate';

const { Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn } = require("material-ui/Table");
const { Tabs, Tab, TextField, MenuItem, SelectField, Avatar } = require("material-ui");
// Tabs, Tab,

const addPhotoIcon = require("app/../assets/images/icon_add_photo.svg") as string;
const deleteUserIcon = require("app/../assets/images/icon_delete.svg") as string;
// const passwdEditIcon = require("app/../assets/images/icon_password_edit.svg") as string;
// const supportIcon = require("app/../assets/images/support.svg") as string;
const settingsIcon = require("app/../assets/images/icon_more.svg") as string;
const markerList = require("app/../assets/images/icon_add_photo.svg") as string;
// const arrowDownIcon = require( 'app/../images/arrow down.svg') as string;
const plusIcon = require("app/../assets/images/icon_plus.svg") as string;
// const grayViewListIcon = require("app/../assets/images/view-list.svg") as string;
// const blueViewTableIcon = require("app/../assets/images/view-table.svg") as string;
// const blueViewListIcon = require("app/../assets/images/view-list-blue.svg") as string;
// const grayViewTableIcon = require("app/../assets/images/view-table-gray.svg") as string;
const buttonDelete = require("app/../assets/images/button_delete.svg") as string;
const buttonEdit = require("app/../assets/images/button_edit.svg") as string;
const photoBox = require("app/../assets/images/photo_box.svg") as string;
const officeAvatar = require('app/../assets/images/officeAvatar.jpg') as string;
const partnersIcon = require("app/../assets/images/icon_partners.svg") as string;
const partnersEditIcon = require("app/../assets/images/icon_parnter_edit.svg") as string;
const searchBlue = require("app/../assets/images/searchBlue.svg") as string;
const whiteBasket = require("app/../assets/images/icon_delete_white.svg") as string;
const closeCross = require("app/../assets/images/close_button_search.svg") as string;
const closeCrossBlack = require("app/../assets/images/close_button_search_black.svg") as string;
const mailIcon = require("app/../assets/images/mail_icon.svg") as string;
const phoneIcon = require("app/../assets/images/phone_icon.svg") as string;
const adminRoleIcon = require("app/../assets/images/adminRole.svg") as string;
const marketologRoleIcon = require("app/../assets/images/marketologRole.svg") as string;
const operatorRoleIcon = require("app/../assets/images/operatorRole.svg") as string;


namespace PartnersUser {
	export interface Props extends RouteComponentProps<any, any> {
		partnersActions: typeof PartnersActions
		partnersUser: any
	}
}

@connect(
	(state: RootState): Pick<PartnersUser.Props, any> => {
		return {};
	},
	(dispatch: Dispatch<any, any>): Pick<PartnersUser.Props, any> => ({
		partnersActions: bindActionCreators(PartnersActions, dispatch)
	})
)

class PartnersUser extends MainTemplate<PartnersUser.Props, any> {
	static defaultProps: Partial<PartnersUser.Props> = {};

	state = {
		showCheckboxes: true,
		height: '100%',
		selectable: true,
		slideIndex: 1,
		slideIndex2: 7,
		toggleToEditOrDeleteFlagId: -1,
		partners: [
			{
				id: 1,
				avatar: officeAvatar,
				name: 'Кравцов Михаил Сергеевич',
				role: 'admin',
				roleIcon: '',
				phone: '123 456 789',
				email: 'example1@example.com',
				partnerName: 'Hershey`s'
			},
			{
				id: 2,
				avatar: plusIcon,
				name: 'Полюшина Зинаида Александровна',
				role: 'marketolog',
				roleIcon: '',
				phone: '123 456 789',
				email: 'example2@example.com',
				partnerName: 'Hershey`s'
			},
			{
				id: 3,
				avatar: officeAvatar,
				name: 'Астахова Регина Иванова',
				role: 'marketolog',
				roleIcon: '',
				phone: '123 456 789',
				email: 'example3@example.com',
				partnerName: 'Hershey`s'
			},
			{
				id: 4,
				avatar: officeAvatar,
				name: 'Зарин Андрей Максимович',
				role: 'admin',
				roleIcon: '',
				phone: '123 456 789',
				email: 'example4@example.com',
				partnerName: 'Hershey`s'
			},
			{
				id: 5,
				avatar: officeAvatar,
				name: 'Зарин Андрей Максимович',
				role: 'operator',
				roleIcon: '',
				phone: '123 456 789',
				email: 'example5@example.com',
				partnerName: 'Hershey`s'
			},
			{
				id: 6,
				avatar: searchBlue,
				name: 'Зарин Андрей Максимович',
				role: 'operator',
				roleIcon: '',
				phone: '123 456 789',
				email: 'example6@example.com',
				partnerName: 'Hershey`s'
			},
			{
				id: 7,
				avatar: partnersEditIcon,
				name: 'Зарин Андрей Максимович',
				role: 'marketolog',
				roleIcon: '',
				phone: '123 456 789',
				email: 'example7@example.com',
				partnerName: 'Hershey`s'
			},
			{
				id: 2,
				avatar: partnersIcon,
				name: 'Полюшина Зинаида Александровна',
				role: 'operator',
				roleIcon: '',
				phone: '123 456 789',
				email: 'example2@example.com',
				partnerName: 'Hershey`s'
			},
			{
				id: 3,
				avatar: officeAvatar,
				name: 'Астахова Регина Иванова',
				role: 'operator',
				roleIcon: '',
				phone: '123 456 789',
				email: 'example3@example.com',
				partnerName: 'Hershey`s'
			},
			{
				id: 4,
				avatar: photoBox,
				name: 'Зарин Андрей Максимович',
				role: 'marketolog',
				roleIcon: '',
				phone: '123 456 789',
				email: 'example4@example.com',
				partnerName: 'Hershey`s'
			},
			{
				id: 5,
				avatar: photoBox,
				name: 'Зарин Андрей Максимович',
				role: 'marketolog',
				roleIcon: '',
				phone: '123 456 789',
				email: 'example5@example.com',
				partnerName: 'Hershey`s'
			},
			{
				id: 6,
				avatar: photoBox,
				name: 'Зарин Андрей Максимович',
				role: 'operator',
				roleIcon: '',
				phone: '123 456 789',
				email: 'example6@example.com',
				partnerName: 'Hershey`s'
			},
			{
				id: 7,
				avatar: photoBox,
				name: 'Зарин Андрей Максимович',
				role: 'operator',
				roleIcon: '',
				phone: '123 456 789',
				email: 'example7@example.com',
				partnerName: 'Hershey`s'
			},
		],
		selectValueRoleUser: 2,
		userRoleFromSelect: '',
		userForModal: {
			id: "",
			avatar: "",
			name: "",
			roleIcon: "",
			role: "",
			phone: "",
			email: "",
			partnerName: ""
		},
		selectedPartner: "",
		value: 1,
		searchEnableField: false,
		// tabValue: 1
		isAnyUserChecked: false,
		selectedRows: [],
		historyAction: [
			{
				id: 1,
				department: 'Аналитика',
				action: 'Сформирован отсчет',
				activityTime: '12.01.2018 10:32:12'
			},
			{
				id: 2,
				department: 'Сотрудники',
				action: 'Изменение профиля',
				activityTime: '12.01.2018 10:32:12'
			},
			{
				id: 3,
				department: 'Акции',
				action: 'Новая акция',
				activityTime: '12.01.2018 10:32:12'
			},
			{
				id: 4,
				department: 'Сценарии',
				action: 'Новый сценарий',
				activityTime: '12.01.2018 10:32:12'
			},
			{
				id: 5,
				department: 'Аналитика',
				action: 'Сформирован отсчет',
				activityTime: '12.01.2018 10:32:12'
			},
		],
		historyVisiting: [
			{
				startSession: '12.05.2018 10:32:12',
				endSession: '12.05.2018 12:14:53',
				sessionTime: '01:42:41',
				ip: '91.134.203.170'
			},
			{
				startSession: '12.05.2018 10:32:12',
				endSession: '12.05.2018 12:14:53',
				sessionTime: '01:42:41',
				ip: '91.134.203.170'
			},
			{
				startSession: '12.05.2018 10:32:12',
				endSession: '12.05.2018 12:14:53',
				sessionTime: '01:42:41',
				ip: '91.134.203.170'
			},
			{
				startSession: '12.05.2018 10:32:12',
				endSession: '12.05.2018 12:14:53',
				sessionTime: '01:42:41',
				ip: '91.134.203.170'
			}
		],
		showSidePanel: false,
		dropPasswordDialog: {
			open: false
		}as any,
		isSidePanelOpen: false
	};

	constructor(props: PartnersUser.Props, context?: any) {
		super(props, context);
		this.state.partners.map((item, index) => {
			switch (item.role) {
				case'admin': {
					item.roleIcon = adminRoleIcon;
					break;
				}
				case'marketolog': {
					item.roleIcon = marketologRoleIcon;
					break;
				}
				case'operator': {
					item.roleIcon = operatorRoleIcon;
					break;
				}
			}

			return this.setState({ partners: this.state.partners });
		})
	}

	openAddPartnerWindow(event: any) {
		$("#addUserModal").modal({ show: true });
		// $.noConflict();
	}

	openEditProfileWindow(user: any) {
		$("#editUserModal").modal({ show: true });
		this.setState({ userForModal: user });

	}

	openChangePasswdProfileWindow() {
		$("#changePasswordModal").modal({ show: true });
	}

	openDeleteUserProfileWindow(user: any) {
		$("#deleteUserModal").modal({ show: true });
		// $('#editUserModal').modal({ hide: true });
		this.setState({ userForModal: user });
		$("#editUserModal").modal("hide");
	}

	toggleToEditOrDelete(index: number, ev: any) {
		if (ev) {
			ev.preventDefault();
			ev.stopPropagation();
		}
		if (this.state.toggleToEditOrDeleteFlagId === index) {
			index = -1;
		}
		//console.log(12343546657, this.state.toggleToEditOrDeleteFlagId);
		this.setState({ toggleToEditOrDeleteFlagId: index });

	}

	onChangePartnerSelect(ev: any) {
		let partner = ev.target.value;
		this.setState({ selectedPartner: partner });

	}

	handleChange = (event: any, index: any, value: any) => this.setState({ value });

	uploadFile() {

	}

	submitAddNewUser() {
		const newUser: any = {
			name: (this.refs.name as any).input.value,
			phone: (this.refs.phone as any).input.value,
			email: (this.refs.email as any).input.value,
			// logo:  (this.refs.logo as any).input.value
		};
		if (this.state.value === 1) {
			newUser.role = 'admin';
		}
		if (this.state.value === 2) {
			newUser.role = 'user';
		}
		$("#addUserModal").modal("hide");

		console.log(123434563456, newUser);
		this.props.partnersActions.addNewUser(newUser);
	}

	submitEditUser() {
		const newUser: any = {};
		if ((this.refs.name as any).input.value) {
			newUser.name = (this.refs.name as any).input.value;
		}
		if (this.state.value === 1) {
			newUser.role = 'admin';
		}
		if (this.state.value === 2) {
			newUser.role = 'user';
		}
		if ((this.refs.phone as any).input.value) {
			newUser.phone = (this.refs.phone as any).input.value;
		}
		if ((this.refs.email as any).input.value) {
			newUser.email = (this.refs.email as any).input.value;
		}
		// logo:  (this.refs.logo as any).input.value

		$("#addUserModal").modal("hide");

		console.log(123434563456, newUser);
		this.props.partnersActions.editNewUser(newUser);
	}

	submitDeleteUser(idUser: any) {
		// const user = this.state.userForModal.id;
		this.props.partnersActions.editNewUser(idUser);
	}

	redirectToProfile() {
		browserHistory.push('/profile');
	}

	searchEnableFieldMethod() {
		this.setState({ searchEnableField: true });
	}

	onBlurSearchField() {
		this.setState({ searchEnableField: false })
	}

	tabChange = (value: any) => {
		this.setState({
			slideIndex: value
		});

	};

	tabChangeSidePanel = (value: any) => {
		this.setState({
			slideIndex2: value
		});

	};

	deleteChakedUsers() {
// this.state.selectedRows
// 		this.state.partners
	}

	onRowSelection(selectedIndexes: any) {
		this.setState({ selectedRows: selectedIndexes })
	}

	toggleSidePanel(user: any, isOpened: boolean, ev: any) {
		if (ev) {
			ev.stopPropagation();
		}
		if (this.state.isSidePanelOpen === isOpened) {
			return;
		}
		this.setState({
			userForModal: user,
			isSidePanelOpen: isOpened
		})
	}

	renderHeader() {
		return <div>
			<div className={'header-top'}>
				<div className={'search-bar'}>
					<img src={searchBlue} className={'search-img'} alt=""
							 {...{ onTouchTap: this.searchEnableFieldMethod.bind(this) }}
					/>
					{
						this.state.searchEnableField && <TextField
							name={'headerSearch'}
							className={'search-text'}
							hintText=""
							placeholder={'Поиск...'}
							fullWidth={true}
							ref="search"
							onBlur={this.onBlurSearchField.bind(this)}
							autoFocus={true}
						/>
					}
				</div>

				<Link to="/profile">
					<img className="avatar" src={officeAvatar} alt=""/>
				</Link>
			</div>


			<div className="header-middle"
					 style={{ display: 'flex', alignItems: 'center', color: '#5a5a5a', }}>
				<button className="btn-xs btn-primary"
								{...{ onTouchTap: this.openAddPartnerWindow.bind(this) }}
								style={{ borderRadius: '3em', width: '20%' }}>
					<img src={plusIcon} style={{ width: '10%' }} alt=""/>
					<span style={{ fontSize: '80%' }}>Добавить</span>
				</button>
			</div>


			<div className={'header-bottom'}>
				<h2 style={{ marginLeft: '5%', font: 'bold' }}>Пользователи</h2>
				<div style={{ width: "400px", }} className="marginLeftForTabsHeader">
					<Tabs
						tabItemContainerStyle={{ backgroundColor: "transparent" }}
						inkBarStyle={{ backgroundColor: "blue", height: "4px" }}
						onChange={this.tabChange} value={this.state.slideIndex}
					>
						<Tab label="Все" value={1} style={{ color: 'blue', fontSize: '10px' }}> </Tab>
						<Tab label="Администраторы" value={2} style={{ color: 'blue', fontSize: '10px' }}> </Tab>
						<Tab label="Маркетологи" value={3} style={{ color: 'blue', fontSize: '10px' }}> </Tab>
						<Tab label="Операторы" value={4} style={{ color: 'blue', fontSize: '10px' }}> </Tab>
					</Tabs>

				</div>
			</div>
		</div>

	}

	renderContent() {
		// const animateClass = _.get(this.state, "dropPasswordDialog.open") ? "animate" : "disanimate";
		let partners = this.state.partners;

		if (this.state.selectedPartner) {
			// partners = partners.filter(p => p.partner === this.state.selectedPartner)
		}
		let { selectedRows } = this.state;

		return <div>
			<div className="row"
				// style={{ flex: 1 }}
					 style={{ flex: 1, minHeight: 'calc(100% - 206px)', background: 'white' }}
			>
				<div className="col-lg-12" style={{ backgroundColor: "white", height: '100%' }}>
					<div>
						{((selectedRows as any) === 'all' || (_.isArray(selectedRows) && !_.isEmpty(selectedRows)))
						&& <div style={{ backgroundColor: 'red' }}{...{ onTouchTap: this.deleteChakedUsers.bind(this) }}>
							<img src={whiteBasket} alt="" style={{ width: '3%' }}/>
							<span style={{ fontSize: '10px', color: 'white' }}> Удалить выбранных пользователей?</span>
							<img src={closeCross} style={{ width: '30px', marginLeft: '80%' }} alt=""/>
						</div>}
						<Table height={this.state.height}
									 selectable={this.state.selectable} multiSelectable={true}
									 onRowSelection={this.onRowSelection.bind(this)}
						>
							<TableHeader
								// displaySelectAll={this.state.selectable}
								// adjustForCheckbox={true}
							>
								<TableRow>
									<TableHeaderColumn>№</TableHeaderColumn>
									<TableHeaderColumn>Фото</TableHeaderColumn>
									<TableHeaderColumn>Имя</TableHeaderColumn>
									<TableHeaderColumn>Роль</TableHeaderColumn>
									<TableHeaderColumn>Телефон</TableHeaderColumn>
									<TableHeaderColumn>E-mail</TableHeaderColumn>
									<TableHeaderColumn></TableHeaderColumn>
								</TableRow>
							</TableHeader>
							<TableBody
								// displayRowCheckbox={this.state.showCheckboxes}
							>
								{
									this.state.slideIndex === 1 && partners.map((item, index) => {
										return <TableRow key={index} className="tableOnHover" style={{ height: '80px' }}
																		 selected={_.includes(selectedRows, index)}>
											<TableRowColumn className={'custom-tr'}>
												<div className="inner-tr-col">
													{index + 1}
												</div>
											</TableRowColumn>
											<TableRowColumn className={'custom-tr'}>
												<div className="inner-tr-col" onClick={this.toggleSidePanel.bind(this, item, true)}>
													<Avatar src={item.avatar}
																	size={60}/>
												</div>
											</TableRowColumn>
											<TableRowColumn className={'custom-tr'}>
												<div className="inner-tr-col" onClick={this.toggleSidePanel.bind(this, item, true)}>
													<span>{item.name}</span>
												</div>
											</TableRowColumn>
											<TableRowColumn className={'custom-tr'} onClick={this.toggleSidePanel.bind(this, item, true)}>
												<div className="inner-tr-col" onClick={this.toggleSidePanel.bind(this, item, true)}>
													<img src={item.roleIcon} alt="" style={{ width: '15px' }}/>
													<span>{item.role}</span>
												</div>
											</TableRowColumn>
											<TableRowColumn className={'custom-tr'}>
												<div className="inner-tr-col" onClick={this.toggleSidePanel.bind(this, item, true)}>
													<span>{item.phone}</span>
												</div>
											</TableRowColumn>
											<TableRowColumn className={'custom-tr'}>
												<div className="inner-tr-col" onClick={this.toggleSidePanel.bind(this, item, true)}>
													<span>{item.email}</span>
												</div>
											</TableRowColumn>

											{this.state.toggleToEditOrDeleteFlagId !== index && <TableRowColumn className={'custom-tr'}>
												<div className="inner-tr-col">
													<img src={settingsIcon} alt="" onClick={this.toggleToEditOrDelete.bind(this, index)}
															 style={{ width: '35px',height:'35px', marginLeft: '90%' }}
													/>
												</div>
											</TableRowColumn>}

											{this.state.toggleToEditOrDeleteFlagId === index && <TableRowColumn className={'custom-tr'}>
												<div className="inner-tr-col">
													<div style={{ width: '100%' }}>
														<img src={buttonEdit} alt=""
																 onClick={this.openEditProfileWindow.bind(this, item)}
																 style={{ marginLeft: '20%', width: '40%' }}
														/>
														<img src={buttonDelete} alt=""
																 onClick={this.openDeleteUserProfileWindow.bind(this, item)}
																 style={{ width: '40%' }}
														/>
													</div>
												</div>
											</TableRowColumn>
											}
										</TableRow>
									})
								}


								{this.state.slideIndex === 2 && partners.map((item, index) => {
									if (item.role === 'admin') {
										return <TableRow key={index} className="tableOnHover" style={{ height: '80px' }}>
											<TableRowColumn>{index + 1}</TableRowColumn>
											<TableRowColumn> <Avatar src={item.avatar} onClick={this.toggleSidePanel.bind(this, item, true)}
																							 size={60}/></TableRowColumn>
											<TableRowColumn onClick={this.toggleSidePanel.bind(this, item, true)}>
												<span onClick={this.toggleSidePanel.bind(this, item, true)}>{item.name}</span></TableRowColumn>
											<TableRowColumn onClick={this.toggleSidePanel.bind(this, item, true)}>
												<img src={item.roleIcon} alt="" style={{ width: '15px' }}/>
												<span onClick={this.toggleSidePanel.bind(this, item, true)}>{item.role}</span></TableRowColumn>
											<TableRowColumn onClick={this.toggleSidePanel.bind(this, item, true)}>
												<span onClick={this.toggleSidePanel.bind(this, item, true)}>{item.phone}</span></TableRowColumn>
											<TableRowColumn onClick={this.toggleSidePanel.bind(this, item, true)}>
												<span onClick={this.toggleSidePanel.bind(this, item, true)}>{item.email}</span></TableRowColumn>


											{this.state.toggleToEditOrDeleteFlagId !== index && <TableRowColumn>
												<img src={settingsIcon} alt=""
														 onClick={this.toggleToEditOrDelete.bind(this, index)}
														 style={{ width: '25px', marginLeft: '90%' }}
												/></TableRowColumn>}

											{this.state.toggleToEditOrDeleteFlagId === index && <TableRowColumn>
												<div style={{ width: '100%' }}>
													<img src={buttonEdit} alt=""
															 onClick={this.openEditProfileWindow.bind(this, item)}
															 style={{ marginLeft: '30%', width: '40%' }}
													/>
													<img src={buttonDelete} alt=""
															 onClick={this.openDeleteUserProfileWindow.bind(this, item)}
															 style={{ width: '40%' }}
													/>
												</div>
											</TableRowColumn>}
										</TableRow>
									}
									return
								})}
								{this.state.slideIndex === 3 && partners.map((item, index) => {
									if (item.role === 'marketolog') {
										return <TableRow key={index} className="tableOnHover" style={{ height: '80px' }}>
											<TableRowColumn>{index + 1}</TableRowColumn>
											<TableRowColumn> <Avatar src={item.avatar} onClick={this.toggleSidePanel.bind(this, item, true)}
																							 size={60}/></TableRowColumn>
											<TableRowColumn onClick={this.toggleSidePanel.bind(this, item, true)}>
												<span onClick={this.toggleSidePanel.bind(this, item, true)}>{item.name}</span></TableRowColumn>
											<TableRowColumn onClick={this.toggleSidePanel.bind(this, item, true)}>
												<img src={item.roleIcon} alt="" style={{ width: '15px' }}/>
												<span onClick={this.toggleSidePanel.bind(this, item, true)}>{item.role}</span></TableRowColumn>
											<TableRowColumn onClick={this.toggleSidePanel.bind(this, item, true)}>
												<span onClick={this.toggleSidePanel.bind(this, item, true)}>{item.phone}</span></TableRowColumn>
											<TableRowColumn onClick={this.toggleSidePanel.bind(this, item, true)}>
												<span onClick={this.toggleSidePanel.bind(this, item, true)}>{item.email}</span></TableRowColumn>


											{this.state.toggleToEditOrDeleteFlagId !== index && <TableRowColumn>
												<img src={settingsIcon} alt=""
														 onClick={this.toggleToEditOrDelete.bind(this, index)}
														 style={{ width: '25px', marginLeft: '90%' }}
												/></TableRowColumn>}

											{this.state.toggleToEditOrDeleteFlagId === index && <TableRowColumn>
												<div style={{ width: '100%' }}>
													<img src={buttonEdit} alt=""
															 onClick={this.openEditProfileWindow.bind(this, item)}
															 style={{ marginLeft: '30%', width: '40%' }}
													/>
													<img src={buttonDelete} alt=""
															 onClick={this.openDeleteUserProfileWindow.bind(this, item)}
															 style={{ width: '40%' }}
													/>
												</div>
											</TableRowColumn>}
										</TableRow>
									}
									return
								})}
								{this.state.slideIndex === 4 && partners.map((item, index) => {
									if (item.role === 'operator') {
										return <TableRow key={index} className="tableOnHover" style={{ height: '80px' }}>
											<TableRowColumn>{index + 1}</TableRowColumn>
											<TableRowColumn> <Avatar src={item.avatar} onClick={this.toggleSidePanel.bind(this, item, true)}
																							 size={60}/></TableRowColumn>
											<TableRowColumn onClick={this.toggleSidePanel.bind(this, item, true)}>
												<span onClick={this.toggleSidePanel.bind(this, item, true)}>{item.name}</span></TableRowColumn>
											<TableRowColumn onClick={this.toggleSidePanel.bind(this, item, true)}>
												<img src={item.roleIcon} alt="" style={{ width: '15px' }}/>
												<span onClick={this.toggleSidePanel.bind(this, item, true)}>{item.role}</span></TableRowColumn>
											<TableRowColumn onClick={this.toggleSidePanel.bind(this, item, true)}>
												<span onClick={this.toggleSidePanel.bind(this, item, true)}>{item.phone}</span></TableRowColumn>
											<TableRowColumn onClick={this.toggleSidePanel.bind(this, item, true)}>
												<span onClick={this.toggleSidePanel.bind(this, item, true)}>{item.email}</span></TableRowColumn>


											{this.state.toggleToEditOrDeleteFlagId !== index && <TableRowColumn>
												<img src={settingsIcon} alt=""
														 onClick={this.toggleToEditOrDelete.bind(this, index)}
														 style={{ width: '25px', marginLeft: '90%' }}
												/></TableRowColumn>}

											{this.state.toggleToEditOrDeleteFlagId === index && <TableRowColumn>
												<div style={{ width: '100%' }}>
													<img src={buttonEdit} alt=""
															 onClick={this.openEditProfileWindow.bind(this, item)}
															 style={{ marginLeft: '30%', width: '40%' }}
													/>
													<img src={buttonDelete} alt=""
															 onClick={this.openDeleteUserProfileWindow.bind(this, item)}
															 style={{ width: '40%' }}
													/>
												</div>
											</TableRowColumn>}
										</TableRow>
									}
									return
								})}
							</TableBody>
						</Table>


					</div>
				</div>
			</div>

			<div {...{ className: "modal fade  bootstrapWindows modalWindow" }as any} id="editUserModal" tabIndex="-1"
					 role="dialog"
					 aria-labelledby="editUserModalLabel" aria-hidden="true"
					 style={{ marginLeft: '40%', marginTop: '8%' }}>
				<div className="modal-dialog" role="document">
					<div className="modal-content" style={{ borderRadius: '20px' }}>
						<div className="modal-header">
							<img src={partnersEditIcon} alt="" style={{ width: '10%', marginTop: '10px' }}/>
							<h3 className="modal-title" id="editUserModalLabel"
									style={{ marginLeft: '5px', fontWeight: 600, fontSize: '100%' }}>
								Редактировать <br/>данные</h3>

							<button type="button" className="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<div className="modal-body">
							<div>
								<img className="imageClass" src={this.state.userForModal.avatar} alt="" style={{
									width: '70px',
									height: '70px',
									borderRadius: '50%',
									backgroundSize: 'auto 70px',
									marginLeft: '7%',
									marginTop: '2%'
								}}/>
								{/*<img src={addPhotoIcon} alt=""*/}
								{/*style={{marginLeft:'-4%',width:'6%',marginTop:'-10%',cursor:'pointer'}}*/}
								{/*// onTouchTap={this.changeAvatar.bind(this)}*/}
								{/*/>*/}
								{/*<FloatingActionButton mini={true} secondary={true}*/}
								{/*style={{marginLeft:'-4%',width:'6%',marginTop:'-10%'}}>*/}
								{/*<img src={addPhotoIcon} style={{width:'25px'}} alt=""/>*/}
								{/*<ContentAdd />*/}
								{/*<input type="file" onChange={this.uploadFile.bind(this)}/>*/}

								{/*</FloatingActionButton>*/}
								<label htmlFor="label123" style={{
									marginLeft: '-4%',
									width: '6%',
									// marginBottom: '10%',
									cursor: 'pointer'
								}}>
									<img src={addPhotoIcon} style={{ width: '35px' }} alt=""/>
									<input type="file" id="label123" style={{ display: 'none' }}
												 onChange={this.uploadFile.bind(this)}/>
								</label>
								{/*<br/><br/>*/}
								<div className="offset-lg-5 col-lg-7"{...{ onTouchTap: this.openDeleteUserProfileWindow.bind(this) }}
								>
									<img src={deleteUserIcon} alt=""
											 style={{ width: '20%', cursor: 'pointer' }}/>
									<span style={{ fontSize: '50%', color: '#e6144a', cursor: 'pointer' }}>удалить пользователя</span>
								</div>
							</div>
							<div style={{ marginLeft: '7%', marginRight: '7%', marginTop: '1%' }}>
								<TextField
									hintText={this.state.userForModal.name}
									floatingLabelText="Фамилия Имя Отчество"
									fullWidth={true}
									ref="name"
								/>

								<SelectField
									floatingLabelText="Роль"
									value={this.state.value}
									onChange={this.handleChange}
									fullWidth={true}
								>
									<MenuItem value={1} primaryText="admin"/>
									<MenuItem value={2} primaryText="marketolog"/>
									<MenuItem value={3} primaryText="operator"/>
									<MenuItem value={4} primaryText="user"/>
								</SelectField>
								<TextField
									hintText={this.state.userForModal.phone}
									floatingLabelText="Телефон..."
									fullWidth={true}
									ref="phone"
								/>
								<TextField
									hintText={this.state.userForModal.email}
									floatingLabelText="Введите E-mail..."
									fullWidth={true}
									ref="email"
								/>

								<button type="button" style={{ borderRadius: '20px', marginLeft: '65%', fontSize: '80%' }}
												className="btn-xs btn-primary"  {...{ onTouchTap: this.submitEditUser.bind(this) }} >
									<span style={{ fontSize: '80%' }}>Сохранить</span>
								</button>
							</div>
						</div>
						{/*<div className="modal-footer">*/}
						{/*<button type="button" style={{borderRadius:'20px',width:'30%'}} className="btn btn-primary">*/}
						{/*Сохранить</button>*/}
						{/*</div>*/}
					</div>
				</div>
			</div>

			<div {...{ className: "modal fade  bootstrapWindows modalWindow" }as any} id="addUserModal" tabIndex="-1"
					 role="dialog"
					 aria-labelledby="addUserModalLabel" aria-hidden="true"
					 style={{ marginLeft: '40%', marginTop: '8%' }}>
				<div className="modal-dialog" role="document">
					<div className="modal-content" style={{ borderRadius: '20px' }}>
						<div className="modal-header">
							<img src={partnersIcon} alt="" style={{ width: '10%', marginTop: '10px' }}/>
							<h3 className="modal-title" id="addUserModalLabel"
									style={{ marginLeft: '5px', fontSize: '90%' }}>
								Добавить<br/>пользователя</h3>

							<button type="button" className="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<div className="modal-body">
							<div style={{ display: 'flex', flexDirection: 'row' }}>
								<img className="imageClass" src={photoBox} alt="" style={{
									width: '70px',
									height: '70px',
									borderRadius: '50%',
									backgroundSize: 'auto 70px',
									marginLeft: '7%',
									marginTop: '2%'
								}}/>

								<label htmlFor="label123" style={{
									marginLeft: '-4%',
									width: '6%',
									marginTop: '10%',
									cursor: 'pointer'
								}}>

									<img src={markerList} style={{ width: '35px', marginTop: '-305%' }} alt=""/>
									<input type="file" id="label123" style={{ display: 'none' }}
												 onChange={this.uploadFile.bind(this)}/>
								</label>
								{/*<div style={{marginTop:'4%'}}>*/}
								{/*<span style={{marginLeft: '15%',color: '#8c96a0',fontSize:'80%'}}>*/}
								{/*Логотип</span><br/>*/}
								{/*<span style={{marginLeft: '15%',color: '#8c96a0',fontSize:'60%'}}>*/}
								{/*В формате PNG или JPG, не более 5Мб </span>*/}
								{/*</div>*/}

							</div>
							<div style={{ marginLeft: '7%', marginRight: '7%', marginTop: '1%' }}>
								<TextField
									hintText="Введите Фамилию Имя ..."
									floatingLabelText="фамилия имя отчество"
									fullWidth={true}
									ref="name"
								/>
								{/*<SelectField*/}
								{/*floatingLabelText="Город"*/}
								{/*value={this.state.value}*/}
								{/*onChange={this.handleChange}*/}
								{/*fullWidth="true"*/}
								{/*>*/}
								{/*{this.state.cities.map((item:any,index:number)=>{*/}
								{/*return<MenuItem value={index} primaryText={item} key={index}/>*/}
								{/*})	}*/}
								{/*</SelectField>*/}
								{/*<SelectField*/}
								{/*floatingLabelText="Партнер"*/}
								{/*value={this.state.value}*/}
								{/*onChange={this.handleChange}*/}
								{/*fullWidth={true}*/}
								{/*>*/}
								{/*<MenuItem value={1} primaryText="admin"/>*/}
								{/*<MenuItem value={2} primaryText="user"/>*/}
								{/*</SelectField>*/}
								<SelectField
									floatingLabelText="Роль"
									value={this.state.value}
									onChange={this.handleChange}
									fullWidth={true}
								>
									<MenuItem value={1} primaryText="admin"/>
									<MenuItem value={2} primaryText="marketolog"/>
									<MenuItem value={3} primaryText="operator"/>
									<MenuItem value={4} primaryText="user"/>
								</SelectField>
								<TextField
									hintText="Введите номер мобильго телефона..."
									floatingLabelText="Телефон"
									// floatingLabelText="Введите номер мобильго телефона"
									fullWidth={true}
									ref="phone"
								/>
								<TextField
									hintText="Введите E-mail..."
									floatingLabelText="E-mail"
									fullWidth={true}
									ref="email"
								/>
								<button type="button"
												style={{ borderRadius: '20px', width: '40%', marginLeft: '60%', marginTop: '10%' }}
												{...{ onTouchTap: this.submitAddNewUser.bind(this) }}
												className="btn-xs btn-primary">
									<span style={{ fontSize: '80%' }}>Сохранить</span>
								</button>
							</div>
						</div>
						{/*<div className="modal-footer">*/}
						{/*<button type="button" style={{borderRadius:'20px',width:'30%'}} className="btn btn-primary">*/}
						{/*Сохранить</button>*/}
						{/*</div>*/}
					</div>
				</div>
			</div>

			<div {...{ className: "modal fade bootstrapWindows modalWindow" }as any} id="deleteUserModal" tabIndex="-1"
					 role="dialog"
					 aria-labelledby="deleteUserModalLabel" aria-hidden="true"
					 style={{ width: '300px', marginLeft: '40%', marginTop: '8%' }}>
				<div className="modal-dialog" role="document">
					<div className="modal-content" style={{ borderRadius: '20px' }}>
						<div className="modal-header">
							<img src={deleteUserIcon} alt="" style={{ width: '10%', marginTop: '10px' }}/>
							<h3 className="modal-title" id="deleteUserModalLabel"
									style={{ marginLeft: '5px', fontWeight: 600, fontSize: '100%' }}>
								Удаление<br/>Администратора</h3>

							<button type="button" className="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<div className="modal-body">
							<div style={{ marginLeft: '7%', marginRight: '7%' }}>
								<br/>
								<label htmlFor="" style={{ fontSize: '0.7vw' }}>Вы действительно хотите удалить
									пользователя?</label>
								<br/>
								<div>
									<h6>{this.state.userForModal.name} </h6><br/>
									<h6>{this.state.userForModal.role} </h6><br/>
									{/*<span>{user.role}</span>*/}
								</div>
								<br/>
								<div style={{ display: 'flex', flexDirection: 'row', justifyContent: 'flex-end' }}>
									<div className="" style={{ width: '35%' }}>
										<button type="button" style={{
											borderRadius: '20px', width: '100%',
											marginTop: '10%', marginBottom: '2%'
										}}
														className="btn-xs btn-secondary">
											<span>Отмена</span>
										</button>
									</div>
									<div className="" style={{ width: '35%' }}>
										<button type="button" style={{
											borderRadius: '20px', marginLeft: '8%', marginTop: '10%'
											, width: '100%', marginBottom: '2%'
										}} className="btn-xs btn-danger"
														{...{ onTouchTap: this.submitDeleteUser.bind(this) }}>
											<span>Удалить</span>
										</button>
									</div>
								</div>
							</div>

						</div>

					</div>
				</div>
			</div>

		</div>
	}

	renderSidePanel() {
		return <div className={'inner-panel'}>

			<div className="inner-top">
				<div>
					<img src={closeCrossBlack} style={{ width: '25px' }}
							 {...{ onTouchTap: this.toggleSidePanel.bind(this, {}, false) }} alt=""/>
				</div>
				<span style={{ color: '#1d1f26', fontSize: 12 }}>
					some text
					{this.state.userForModal.partnerName}
				</span>
			</div>
			<div className="inner-profile">
				<div>
					<Avatar src={this.state.userForModal.avatar} size={80} className={'avatar'}/>
				</div>

				<div>
					<h5 className="sidePanelMainMenuMainTitle">{this.state.userForModal.name}some text</h5>

					<div className={'profile-text-block'}>
						<img src={this.state.userForModal.roleIcon} alt=""/>
						<span className="sidePanelMainMenuText">{this.state.userForModal.role}some text</span>
					</div>

					<div className={'profile-text-block'}>
						<img src={mailIcon} alt=""/>
						<span className="sidePanelMainMenuText">{this.state.userForModal.email}some text</span>
					</div>

					<div className={'profile-text-block'}>
						<img src={phoneIcon} alt=""/>
						<span className="sidePanelMainMenuText">{this.state.userForModal.phone}some text</span>
					</div>

				</div>

				<div>
					<div>
						<img src={buttonEdit} alt=""
								 onClick={this.openEditProfileWindow.bind(this, this.state.userForModal)}
						/>
						<img src={buttonDelete} alt=""
								 onClick={this.openDeleteUserProfileWindow.bind(this, this.state.userForModal)}
						/>
					</div>
				</div>
			</div>

			<div className="inner-content">
				<hr/>
				<div style={{ width: '300px' }}>
					<Tabs
						tabItemContainerStyle={{ backgroundColor: "transparent" }}
						inkBarStyle={{ backgroundColor: "blue", height: "4px" }}
						onChange={this.tabChangeSidePanel} value={this.state.slideIndex2}
					>

						<Tab label="история посещений" value={6} style={{ color: 'blue', fontSize: '10px' }}> </Tab>
						<Tab label="история действий" value={7} style={{ color: 'blue', fontSize: '10px' }}> </Tab>

					</Tabs>


				</div>
				{
					this.state.slideIndex2 === 7 && <div>
						<div>
							<Table height={this.state.height} selectable={false}>
								<TableHeader displaySelectAll={false} adjustForCheckbox={false}
														 style={{ backgroundColor: '#f8f8f8' }}
								>
									<TableRow>
										<TableHeaderColumn><span className="fontSidePanel">№</span></TableHeaderColumn>
										<TableHeaderColumn><span className="fontSidePanel">Раздел</span></TableHeaderColumn>
										<TableHeaderColumn><span className="fontSidePanel">Действие</span></TableHeaderColumn>
										<TableHeaderColumn><span className="fontSidePanel">Время активности</span></TableHeaderColumn>
									</TableRow>
								</TableHeader>
								<TableBody displayRowCheckbox={false}
								>
									{this.state.historyAction.map((item, index) => {
										return <TableRow key={index} className="tableOnHover">
											<TableRowColumn><span style={{ color: '#8f97bf' }} className="fontSidePanel">
                                        {index + 1}</span></TableRowColumn>
											<TableRowColumn><span style={{ color: '#8f97bf' }} className="fontSidePanel">
                                        {item.department}</span></TableRowColumn>
											<TableRowColumn><span style={{ color: '#8f97bf' }} className="fontSidePanel">
                                        {item.action}</span></TableRowColumn>
											<TableRowColumn><span style={{ color: '#8f97bf' }} className="fontSidePanel">
                                        {item.activityTime}</span></TableRowColumn>
										</TableRow>
									})}


								</TableBody>
							</Table>


						</div>
					</div>
				}

				{
					this.state.slideIndex2 === 6 && <div>
						<div>
							<Table height={this.state.height} selectable={false}>
								<TableHeader displaySelectAll={false} adjustForCheckbox={false}
														 style={{ backgroundColor: '#f8f8f8' }}
								>
									<TableRow>
										<TableHeaderColumn><span className="fontSidePanel">№</span></TableHeaderColumn>
										<TableHeaderColumn><span className="fontSidePanel">Начало сессии</span></TableHeaderColumn>
										<TableHeaderColumn><span className="fontSidePanel">Конец сессии</span></TableHeaderColumn>
										<TableHeaderColumn><span className="fontSidePanel">Время сессии</span></TableHeaderColumn>
										<TableHeaderColumn><span className="fontSidePanel">IP-адрес</span></TableHeaderColumn>
									</TableRow>
								</TableHeader>
								<TableBody displayRowCheckbox={false}
								>
									{this.state.historyVisiting.map((item, index) => {
										return <TableRow key={index} className="tableOnHover ">
											<TableRowColumn><span style={{ color: '#8f97bf' }} className="fontSidePanel">
                                        {index + 1}</span></TableRowColumn>
											<TableRowColumn><span style={{ color: '#8f97bf' }} className="fontSidePanel">
                                        {item.startSession}</span></TableRowColumn>
											<TableRowColumn><span style={{ color: '#8f97bf' }} className="fontSidePanel">
                                        {item.endSession}</span></TableRowColumn>
											<TableRowColumn><span style={{ color: '#8f97bf' }} className="fontSidePanel">
                                        {item.sessionTime}</span></TableRowColumn>
											<TableRowColumn><span style={{ color: '#8f97bf' }} className="fontSidePanel">
                                        {item.ip}</span></TableRowColumn>
										</TableRow>
									})}


								</TableBody>
							</Table>


						</div>
					</div>
				}


			</div>


		</div>
	}


}

export default PartnersUser;