import * as React from "react";
import { connect } from "react-redux";
import { Dispatch, bindActionCreators } from "redux";
// bindActionCreators,
import { RouteComponentProps, browserHistory, Link } from "react-router";
import { RootState } from "app/reducers";
//const $ = require("jquery");
// import * as style from './style.scss';
import './style.scss';
import { PartnersActions } from 'app/actions/partners';

import MainTemplate from '../MainTemplate';

const {  TextField } = require("material-ui");
const { Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn } = require("material-ui/Table");
// const { MenuItem}= require('material-ui/MenuItem');
// const {SelectField} = require('material-ui/SelectField');
// Tabs, Tab,

// const editIcon = require('app/../assets/images/icon_edit.svg') as string;
// const editProfileIcon = require('app/../assets/images/icon_profile_edit.svg') as string;
// const addPhotoIcon = require('app/../assets/images/icon_add_photo.svg') as string;
// const deleteUserIcon = require('app/../assets/images/icon_delete.svg') as string;
// const passwdEditIcon = require('app/../assets/images/icon_password_edit.svg') as string;

const addPhotoIcon = require("app/../assets/images/icon_add_photo.svg") as string;
const deleteUserIcon = require("app/../assets/images/icon_delete.svg") as string;
const passwdEditIcon = require("app/../assets/images/icon_password_edit.svg") as string;
// const supportIcon = require("app/../assets/images/support.svg") as string;
const settingsIcon = require("app/../assets/images/icon_more.svg") as string;
const markerList = require("app/../assets/images/icon_add_photo.svg") as string;
// const arrowDownIcon = require( 'app/../images/arrow down.svg') as string;
// const plusIcon = require("app/../assets/images/icon_plus.svg") as string;
// const grayViewListIcon = require("app/../assets/images/view-list.svg") as string;
// const blueViewTableIcon = require("app/../assets/images/view-table.svg") as string;
// const blueViewListIcon = require("app/../assets/images/view-list-blue.svg") as string;
// const grayViewTableIcon = require("app/../assets/images/view-table-gray.svg") as string;
// const buttonDelete = require("app/../assets/images/button_delete.svg") as string;
// const buttonEdit = require("app/../assets/images/button_edit.svg") as string;
// const photoBox = require("app/../assets/images/photo_box.svg") as string;
// const officeAvatar = require( 'app/../images/officeAvatar.jpg') as string;
// const partnersIcon = require("app/../assets/images/icon_partners.svg") as string;
// const partnersEditIcon = require("app/../assets/images/icon_parnter_edit.svg") as string;
const officeAvatar = require('app/../assets/images/officeAvatar.jpg') as string;
const searchBlue = require("app/../assets/images/searchBlue.svg") as string;


namespace Points {
  export interface Props extends RouteComponentProps<any, any> {
    partnersActions: typeof PartnersActions,
    partners: any
  }
}

@connect(
  (state: RootState): Pick<Points.Props, any> => {
    return {
      partners: state.partners.partners.items,
    };
  },
  (dispatch: Dispatch<any, any>): Pick<Points.Props, any> => ({
    partnersActions: bindActionCreators(PartnersActions, dispatch)
  })
)

class Points extends MainTemplate<Points.Props, any> {
  static defaultProps: Partial<Points.Props> = {};

  constructor(props: Points.Props, context?: any) {
    super(props, context);
  }

  state = {
    value: 3,
    user: {
      firstName: "Дмитрий",
      secondName: "Заруба",
      role: "superAdmin",
      phone: "+7 902 563 50 45",
      email: "dzaruba@gmail.com"
    },
    showCheckboxes: false,
    height: "100%",
    selectable: false,
    slideIndex: 0,
    toggleToEditOrDeleteFlagId: -1,
    partners: [
      {
        id: 1,
        logo: addPhotoIcon,
        name: "companyName1",
        city: "city1",
        contact: "name1"
      },
      {
        id: 2,
        logo: deleteUserIcon,
        name: "companyName2",
        city: "city2",
        contact: "name2"
      },
      {
        id: 3,
        logo: officeAvatar,
        name: "companyName3",
        city: "city3",
        contact: "name3"
      },
      {
        id: 4,
        logo: passwdEditIcon,
        name: "companyName4",
        city: "city4",
        contact: "name4"
      },
      {
        id: 5,
        logo: officeAvatar,
        name: "companyName5",
        city: "city5",
        contact: "name5"
      },
      {
        id: 6,
        logo: passwdEditIcon,
        name: "companyName6",
        city: "city6",
        contact: "name6"
      },
      {
        id: 7,
        logo: passwdEditIcon,
        name: "companyName7",
        city: "city7",
        contact: "name7"
      },
      {
        id: 4,
        logo: passwdEditIcon,
        name: "companyName4",
        city: "city4",
        contact: "name4"
      },
      {
        id: 5,
        logo: passwdEditIcon,
        name: "companyName5",
        city: "city5",
        contact: "name5"
      },
      {
        id: 6,
        logo: passwdEditIcon,
        name: "companyName6",
        city: "city6",
        contact: "name6"
      },
      {
        id: 7,
        logo: passwdEditIcon,
        name: "companyName7",
        city: "city7",
        contact: "name7"
      }
    ],
    userForModal: {
      logo: "",
      contact: "",
      city: "",
      name: ""
    },
    selectedCity: "",
    searchEnableField: false,
    // user = {
    //     firstName: 'Дмитрий',
    //     secondName: 'Заруба',
    //     role: 'superAdmin',
    //     phone: '+7 902 563 50 45',
    //     email: 'dzaruba@gmail.com',
    //     avatar: '../../images/officeAvatar.jpg'
    //   }
  };

  tabChange = (value: any) => {
    this.setState({
      slideIndex: value
    });

  };

  moveToThePartner(id: any) {
    browserHistory.push(`/partners/${id}/users-partners`)
  }

  onChangeCitySelect(ev: any) {
    let city = ev.target.value;
    this.setState({ selectedCity: city });

  }


  // openAddPartnerWindow() {
  //   ($("#addUserModal") as any).modal({ show: true });
  // }
  //
  // openEditPartnerWindow(index: number) {
  //   $("#editUserModal").modal({ show: true });
  //
  //   this.setState({ userForModal: this.state.partners[index] });
  // }
  //
  // openDeletePartnerWindow(index: number) {
  //   $("#deleteUserModal").modal({ show: true });
  //   this.setState({ userForModal: this.state.partners[index] });
  // }

  openDeleteUserProfileWindow() {
    $("#deleteUserModal").modal({ show: true });
    $("#editUserModal").modal("hide");
  }

  toggleToEditOrDelete(index: number, ev: any) {
    if (ev) {
      ev.preventDefault();
      ev.stopPropagation();
    }
    if (this.state.toggleToEditOrDeleteFlagId === index) {
      index = -1;
    }
    //console.log(12343546657, this.state.toggleToEditOrDeleteFlagId);
    this.setState({ toggleToEditOrDeleteFlagId: index });

  }


  searchEnableFieldMethod() {
    this.setState({ searchEnableField: true });
  }

  onBlurSearchField() {
    this.setState({ searchEnableField: false })
  }


  renderHeader() {
    return <div>
      <div className={'header-top'}>
        <div className={'search-bar'}>
          <img src={searchBlue} className={'search-img'} alt=""
               {...{ onTouchTap: this.searchEnableFieldMethod.bind(this) }}
          />
          {
            this.state.searchEnableField && <TextField
              name={'headerSearch'}
              className={'search-text'}
              hintText=""
              placeholder={'Поиск...'}
              fullWidth={true}
              ref="search"
              onBlur={this.onBlurSearchField.bind(this)}
              autoFocus={true}
            />
          }
        </div>

        <Link to="/profile">
          <img className="avatar" src={officeAvatar} alt=""/>
        </Link>
      </div>


      <div className={'header-middle'}>
        <div style={{ display: "flex", alignItems: "center", color: "#5a5a5a" }}>
          Город:
        </div>
        <select
          className="form-control" id="exampleFormControlSelect1"
          style={{ width: "20%", borderRadius: "20px" }}
          onChange={this.onChangeCitySelect.bind(this)}
        >
          <option value="">Все города</option>
          {
            this.state.partners.map((item, index) => {
              return <option value={item.city} key={index}>{item.city}</option>;
            })
          }
        </select>

        {/*<button className="btn-xs btn-primary"*/}
                {/*{...{ onTouchTap: this.openAddPartnerWindow.bind(this) }}*/}
                {/*style={{ borderRadius: "20px"}}*/}
        {/*>*/}
          {/*<img src={plusIcon} style={{ width: "25px" }} alt=""/>*/}
          {/*Добавить партнеров*/}
        {/*</button>*/}
      </div>


      {/*<div className={'header-bottom'}>*/}
        {/*<h2 className={'title'}>Партнеры</h2>*/}
        {/*<div style={{ width: 100 }}>*/}
          {/*<Tabs*/}
            {/*tabItemContainerStyle={{ backgroundColor: "transparent" }}*/}
            {/*inkBarStyle={{ backgroundColor: "blue", height: "4px" }}*/}
            {/*onChange={this.tabChange} value={this.state.slideIndex}*/}
          {/*>*/}
            {/*{this.state.slideIndex === 1 &&*/}
            {/*<Tab icon={<img src={grayViewListIcon}/>} value={0}>*/}
            {/*</Tab>}*/}
            {/*{this.state.slideIndex === 0 &&*/}
            {/*<Tab icon={<img src={blueViewListIcon}/>} value={0}>*/}
            {/*</Tab>}*/}

            {/*{this.state.slideIndex === 1 &&*/}
            {/*<Tab icon={<img src={blueViewTableIcon}/>} value={1}>*/}
            {/*</Tab>}*/}
            {/*{this.state.slideIndex === 0 &&*/}
            {/*<Tab icon={<img src={grayViewTableIcon}/>} value={1}>*/}
            {/*</Tab>}*/}
          {/*</Tabs>*/}
        {/*</div>*/}
      {/*</div>*/}


    </div>
  }

  renderContent() {
    let partners = this.state.partners;

    if (this.state.selectedCity) {
      partners = partners.filter(p => p.city === this.state.selectedCity);
    }

    return <div>
      <div className="row"
				// style={{ flex: 1 }}
           style={{ flex: 1, background: 'white', height: 'calc(100% - 170px - 50px)' }}
      >
        <div className="col-lg-12" style={{ backgroundColor: "white", height: "100%" }}>
          <div>
            <Table height={this.state.height} selectable={this.state.selectable}>
              <TableHeader displaySelectAll={this.state.showCheckboxes}
                           adjustForCheckbox={this.state.showCheckboxes}
              >

                <TableRow>
                  <TableHeaderColumn>№</TableHeaderColumn>
                  <TableHeaderColumn>Лого</TableHeaderColumn>
                  <TableHeaderColumn>Наименование</TableHeaderColumn>
                  <TableHeaderColumn>Город</TableHeaderColumn>
                  <TableHeaderColumn>Контактное лицо</TableHeaderColumn>
                  <TableHeaderColumn></TableHeaderColumn>
                </TableRow>
              </TableHeader>
              <TableBody displayRowCheckbox={this.state.showCheckboxes}
              >
                {partners.map((item, index) => {
                  return <TableRow key={index} className="tableOnHover">
                    <TableRowColumn style={{ height: "80px" }}
                                    {...{ onTouchTap: this.moveToThePartner.bind(this, item.id) }}
                    >{index + 1}</TableRowColumn>
                    <TableRowColumn
                      {...{ onTouchTap: this.moveToThePartner.bind(this, item.id) }}>
                      <img src={item.logo} alt="" style={{ width: "25px" }}/>
                    </TableRowColumn>
                    <TableRowColumn {...{ onTouchTap: this.moveToThePartner.bind(this, item.id) }}
                    >{item.name}</TableRowColumn>
                    <TableRowColumn  {...{ onTouchTap: this.moveToThePartner.bind(this, item.id) }}
                    >{item.city}</TableRowColumn>
                    <TableRowColumn {...{ onTouchTap: this.moveToThePartner.bind(this, item.id) }} >
                      <img src={markerList} alt=""
                           style={{ width: "5px", marginRight: "3%" }}/>
                      {item.contact}
                    </TableRowColumn>
                    {this.state.toggleToEditOrDeleteFlagId !== index && <TableRowColumn>
                      <img src={settingsIcon} alt=""
                           onClick={this.toggleToEditOrDelete.bind(this, index)}
                           style={{ width: "25px", marginLeft: "90%" }}
                      /></TableRowColumn>}

                    {/*{this.state.toggleToEditOrDeleteFlagId === index && <TableRowColumn>*/}
                      {/*<div style={{ width: "100%" }}>*/}
                        {/*<img src={buttonEdit} alt=""*/}
                             {/*onClick={this.openEditPartnerWindow.bind(this, index)}*/}
                             {/*style={{ marginLeft: '40%', width: '30%' }}*/}
                        {/*/>*/}
                        {/*<img src={buttonDelete} alt=""*/}
                             {/*onClick={this.openDeletePartnerWindow.bind(this, index)}*/}
                             {/*style={{ width: '30%' }}*/}

                        {/*/>*/}
                      {/*</div>*/}
                    {/*</TableRowColumn>}*/}
                  </TableRow>;
                })}


              </TableBody>
            </Table>


          </div>



        </div>
      </div>






    </div>
  }

}

export default Points;