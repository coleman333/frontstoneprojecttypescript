import * as React from "react";
import { connect } from "react-redux";
import { Dispatch, bindActionCreators } from "redux";
// bindActionCreators,
import { RouteComponentProps, browserHistory ,Link} from "react-router";
import { RootState } from "app/reducers";
//const $ = require("jquery");
// import * as style from './style.scss';
import './style.scss';
import { PartnersActions } from 'app/actions/partners';

import MainTemplate from '../MainTemplate';
import * as _ from "lodash" ;

const { Tabs, Tab, TextField } = require("material-ui");
const { Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn } = require("material-ui/Table");
// const { MenuItem}= require('material-ui/MenuItem');
// const {SelectField} = require('material-ui/SelectField');


const addPhotoIcon = require("app/../assets/images/icon_add_photo.svg") as string;
const grayViewListIcon = require("app/../assets/images/view-list.svg") as string;
const blueViewTableIcon = require("app/../assets/images/view-table.svg") as string;
const blueViewListIcon = require("app/../assets/images/view-list-blue.svg") as string;
const grayViewTableIcon = require("app/../assets/images/view-table-gray.svg") as string;
const officeAvatar = require('app/../assets/images/officeAvatar.jpg') as string;
const searchBlue = require("app/../assets/images/searchBlue.svg") as string;
const closeCrossBlack = require("app/../assets/images/close_button_search_black.svg") as string;
const hershey = require("app/../assets/images/hershes.png") as string;
// const infoIcon = require("app/../assets/images/icon_info.svg") as string;
const placeIcon = require("app/../assets/images/icon_place.svg") as string;

namespace Products {
	export interface Props extends RouteComponentProps<any, any> {
		// partnersActions: typeof PartnersActions,
		// partners: any
	}
}

@connect(
	(state: RootState): Pick<Products.Props, any> => {
		return {
			partners: state.partners.partners.items,
		};
	},
	(dispatch: Dispatch<any, any>): Pick<Products.Props, any> => ({
		partnersActions: bindActionCreators(PartnersActions, dispatch)
	})
)

class Products extends MainTemplate<Products.Props, any> {
	static defaultProps: Partial<Products.Props> = {};

	constructor(props: Products.Props, context?: any) {
		super(props, context);
	}

	state = {
		value: 3,
		user: {
			firstName: "Дмитрий",
			secondName: "Заруба",
			role: "superAdmin",
			phone: "+7 902 563 50 45",
			email: "dzaruba@gmail.com"
		},
		showCheckboxes: false,
		height: "100%",
		selectable: false,
		slideIndex: 0,
		toggleToEditOrDeleteFlagId: -1,
		partners: [
			{
				id:1,
				number: 1,
				logo: hershey,
				name: "companyName1",
				description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit." +
				" Error id illo officia quia quod sequi similique. Ab aperiam debitis " +
				"earum eius exercitationem, explicabo id iusto, possimus qui ratione sequi voluptatem.",
				price: 123,
				remain:2
			},
      {
        id:2,
        number: 2,
        logo: hershey,
        name: "companyName2",
        description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit." +
        " Error id illo officia quia quod sequi similique. Ab aperiam debitis " +
        "earum eius exercitationem, explicabo id iusto, possimus qui ratione sequi voluptatem.",
				price: 234,
        remain:23
      },
      {
        id:3,
        number: 3,
        logo: addPhotoIcon,
        name: "companyName3",
        description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit." +
        " Error id illo officia quia quod sequi similique. Ab aperiam debitis " +
        "earum eius exercitationem, explicabo id iusto, possimus qui ratione sequi voluptatem.",
				price: 4564,
        remain:34
      },
      {
        id:4,
        number: 4,
        logo: addPhotoIcon,
        name: "companyName4",
        description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit." +
        " Error id illo officia quia quod sequi similique. Ab aperiam debitis " +
        "earum eius exercitationem, explicabo id iusto, possimus qui ratione sequi voluptatem.",
				price: 1234,
        remain:12
      },
      {
        id:5,
        number: 5,
        logo: hershey,
        name: "companyName5",
        description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit." +
        " Error id illo officia quia quod sequi similique. Ab aperiam debitis " +
        "earum eius exercitationem, explicabo id iusto, possimus qui ratione sequi voluptatem.",
				price: 3456,
        remain:5467
      },
      {
        id:6,
        number: 6,
        logo: hershey,
        name: "companyName6",
        description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit." +
        " Error id illo officia quia quod sequi similique. Ab aperiam debitis " +
        "earum eius exercitationem, explicabo id iusto, possimus qui ratione sequi voluptatem.",
				price: 42,
        remain:23
      },
      {
        id:7,
        number: 7,
        logo: addPhotoIcon,
        name: "companyName7",
        description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit." +
        " Error id illo officia quia quod sequi similique. Ab aperiam debitis " +
        "earum eius exercitationem, explicabo id iusto, possimus qui ratione sequi voluptatem.",
				price: 2345,
        remain:768
      },
      {
        id:8,
        number: 8,
        logo: addPhotoIcon,
        name: "companyName8",
        description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit." +
        " Error id illo officia quia quod sequi similique. Ab aperiam debitis " +
        "earum eius exercitationem, explicabo id iusto, possimus qui ratione sequi voluptatem.",
				price: 345,
        remain:123
      },

      {
        id:9,
        number: 9,
        logo: hershey,
        name: "companyName9",
        description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit." +
        " Error id illo officia quia quod sequi similique. Ab aperiam debitis " +
        "earum eius exercitationem, explicabo id iusto, possimus qui ratione sequi voluptatem.",
				price: 12345,
        remain:234
      },
      {
        id:10,
        number: 10,
        logo: hershey,
        name: "companyName10",
        description: "Lorem ipsum dolor sit amet, consectetur adipisicing elit." +
        " Error id illo officia quia quod sequi similique. Ab aperiam debitis " +
        "earum eius exercitationem, explicabo id iusto, possimus qui ratione sequi voluptatem.",
				price: 4567,
        remain:345
      },
		],
		userForModal: {
			id: 0,
      number: 0,
      logo: "",
      name: "",
      description: "",
      price: 0,
      remain:0
		},
		searchEnableField: false,
		// user = {
		//     firstName: 'Дмитрий',
		//     secondName: 'Заруба',
		//     role: 'superAdmin',
		//     phone: '+7 902 563 50 45',
		//     email: 'dzaruba@gmail.com',
		//     avatar: '../../images/officeAvatar.jpg'
		//   }
    isSidePanelOpen:false,
    points:[
      {
        id:1,
        remains:10,
        place:'hersheys в Плаза'
      },
      {
        id:2,
        remains:20,
        place:'hersheys на Прохорова'
      },
      {
        id:3,
        remains:20,
        place:'hersheys на Кутузовском'
      },
    ],
    dropPasswordDialog: {
      open: false
    }as any
  };

	tabChange = (value: any) => {
		this.setState({
			slideIndex: value
		});

	};

	moveToThePartner(id: any) {
		browserHistory.push(`/partners/${id}/users-partners`)
	}

	onChangeCitySelect(ev: any) {
		let city = ev.target.value;
		this.setState({ selectedCity: city });

	}


	openAddPartnerWindow() {
		($("#addUserModal") as any).modal({ show: true });
	}

	openEditPartnerWindow(index: number) {
		$("#editUserModal").modal({ show: true });

		this.setState({ userForModal: this.state.partners[index] });
	}

	openDeletePartnerWindow(index: number) {
		$("#deleteUserModal").modal({ show: true });
		this.setState({ userForModal: this.state.partners[index] });
	}

	openDeleteUserProfileWindow() {
		$("#deleteUserModal").modal({ show: true });
		$("#editUserModal").modal("hide");
	}

	toggleToEditOrDelete(index: number, ev: any) {
		if (ev) {
			ev.preventDefault();
			ev.stopPropagation();
		}
		if (this.state.toggleToEditOrDeleteFlagId === index) {
			index = -1;
		}
		//console.log(12343546657, this.state.toggleToEditOrDeleteFlagId);
		this.setState({ toggleToEditOrDeleteFlagId: index });

	}

	submitAddNewPartner() {
		const newPartner: any = {
			name: (this.refs.companyName as any).input.value,
			contact: (this.refs.contact as any).input.value,
			// city: (this.refs.city as any).input.value,
			phone: (this.refs.phone as any).input.value,
			email: (this.refs.email as any).input.value

		};
		$("#addUserModal").modal("hide");

		console.log(123434563456, newPartner);
		// this.props.partnersActions.create(newPartner);
	}

	uploadFile() {

	}

	searchEnableFieldMethod() {
		this.setState({ searchEnableField: true });
	}

	onBlurSearchField() {
		this.setState({ searchEnableField: false })
	}

	redirectToProfile() {
		browserHistory.push('/profile');
	}

  closeSideUserPanel() {
    this.setState({ isSidePanelOpen: false });
  }

  toggleSidePanel(user: any, isOpened: boolean, ev: any) {
    // this.state.dropPasswordDialog.open = true;
    // this.forceUpdate();
    // console.log('qwerert',this.state.dropPasswordDialog);

    if (ev) {
      ev.stopPropagation();
    }
    if (this.state.isSidePanelOpen === isOpened) {
      return;
    }
    this.setState({
      userForModal: user,
      isSidePanelOpen: isOpened,
      // dropPasswordDialog:true
    })
    // console.log('asdfsdfgh',this.state.dropPasswordDialog);

  }




  renderHeader() {
    return <div>
      <div className={'header-top'}>
        <div className={'search-bar'}>
          <img src={searchBlue} className={'search-img'} alt=""
               {...{ onTouchTap: this.searchEnableFieldMethod.bind(this) }}
          />
          {
            this.state.searchEnableField && <TextField
              name={'headerSearch'}
              className={'search-text'}
              hintText=""
              placeholder={'Поиск...'}
              fullWidth={true}
              ref="search"
              onBlur={this.onBlurSearchField.bind(this)}
              autoFocus={true}
            />
          }
        </div>

        <Link to="/profile">
          <img className="avatar" src={officeAvatar} alt=""/>
        </Link>
      </div>


      <div className={'header-middle'}>

      </div>


      <div className={'header-bottom'}>
        <h2 className={'title'}>Товары</h2>
        <div style={{ width: 100 }}>
          <Tabs
            tabItemContainerStyle={{ backgroundColor: "transparent" }}
            inkBarStyle={{ backgroundColor: "blue", height: "4px" }}
            onChange={this.tabChange} value={this.state.slideIndex}
          >
            {this.state.slideIndex === 1 &&
            <Tab icon={<img src={grayViewListIcon}/>} value={0}>
            </Tab>}
            {this.state.slideIndex === 0 &&
            <Tab icon={<img src={blueViewListIcon}/>} value={0}>
            </Tab>}

            {this.state.slideIndex === 1 &&
            <Tab icon={<img src={blueViewTableIcon}/>} value={1}>
            </Tab>}
            {this.state.slideIndex === 0 &&
            <Tab icon={<img src={grayViewTableIcon}/>} value={1}>
            </Tab>}
          </Tabs>
        </div>
      </div>


    </div>
  }

	renderContent() {
		let partners = this.state.partners;

		return <div className="row" style={{ flex: 1, background: 'white' }}	>
				<div className="col-lg-12" style={{ backgroundColor: "white", height: "100%" }}>
					{this.state.slideIndex === 0 &&
					<div>
						<Table height={this.state.height} selectable={this.state.selectable}>
							<TableHeader displaySelectAll={this.state.showCheckboxes}
													 adjustForCheckbox={this.state.showCheckboxes}
							>
								<TableRow>
									<TableHeaderColumn>№</TableHeaderColumn>
									<TableHeaderColumn>Лого</TableHeaderColumn>
									<TableHeaderColumn>Наименование</TableHeaderColumn>
									<TableHeaderColumn>Описание</TableHeaderColumn>
									<TableHeaderColumn>Цена</TableHeaderColumn>
									<TableHeaderColumn>Остаток</TableHeaderColumn>
								</TableRow>
							</TableHeader>
							<TableBody displayRowCheckbox={this.state.showCheckboxes}
							>
								{partners.map((item, index) => {
									return <TableRow key={index} className="tableOnHover">
										<TableRowColumn style={{ height: "80px" }}
																		{...{ onTouchTap: this.toggleSidePanel.bind(this, item) }}
										>{index + 1}</TableRowColumn>
										<TableRowColumn	{...{ onTouchTap: this.toggleSidePanel.bind(this, item,!_.get(this.state, "dropPasswordDialog.open")) }}>
											<img src={item.logo} alt="" style={{ width: "70px" }}/>
										</TableRowColumn>
										<TableRowColumn {...{ onTouchTap: this.toggleSidePanel.bind(this, item) }}
										>{item.name}</TableRowColumn>
										<TableRowColumn  {...{ onTouchTap: this.toggleSidePanel.bind(this, item) }}
										>{item.description}</TableRowColumn>
										<TableRowColumn {...{ onTouchTap: this.toggleSidePanel.bind(this, item) }} >
											{item.price}	</TableRowColumn>
                    <TableRowColumn {...{ onTouchTap: this.toggleSidePanel.bind(this, item) }} >
                      {item.remain}	</TableRowColumn>
									</TableRow>;
								})}
							</TableBody>
						</Table>
					</div>}


          {this.state.slideIndex === 1 &&
            <div className="" style={{display:"flex",flexDirection:'row'
              ,flexWrap:"wrap",marginLeft:'5%'}}
            >
              {this.state.partners.map((item, index) => {
                return <div key={index} style={{
                  boxShadow: "0 0 40px rgba(0,0,0,0.4)",border: "solid 0.5px #ebebeb" }}
                    {...{ onTouchTap: this.toggleSidePanel.bind(this, item) }}
                            className="slideUp threed bricksSize">
                  <img src={item.logo} alt="" className="imgBricksSize" />
                  <hr/>

                  <div style={{display:'flex',flexDirection:'column',justifyContent:'space-between'}}>
                    <span style={{textAlign:'center',fontWeight:600}}>{item.name}</span>
                  </div>
                  <div style={{display:'flex',flexDirection:'row',justifyContent:'space-between',marginTop:'5%' }}>
                    <span style={{color: "#5a5a5a",fontSize: '80%',	lineHeight: '90%',marginLeft:'5%',marginRight:"5%"}}
                    >Остаток </span><span>{item.remain}</span>
                  </div>
                  <div style={{display:'flex',flexDirection:'row',justifyContent:'space-between'}}>
                    <span style={{color: "#5a5a5a",fontSize: '80%',	lineHeight: '90%',marginLeft:'5%',marginRight:"5%"}}
                    >Цена </span><span>{item.price}</span>
                  </div>
                </div>})}
            </div>
          }
        </div>
		  </div>
	}




  renderSidePanel() {
    // const animateClass = _.get(this.state, "dropPasswordDialog.open") ? "animate" : "disanimate";
    // ${animateClass}

    return <div className={`inner-panel `} id="stripe">

      <div className="inner-top">
        <div>
          <img src={closeCrossBlack} style={{ width: '25px' }}
               {...{ onTouchTap: this.toggleSidePanel.bind(this, {}, false) }} alt=""/>
        </div>

      </div>
      <div className=""style={{display:'flex'}}>
        <div style={{flex:2,marginLeft:"6%",marginTop:'3%',marginBottom:'3%'}}>
          <img src={hershey} alt=""  style={{ width:"100%" }} />
        </div>

        <div style={{flex:5,marginLeft:'6%',marginRight:'6%'}}>
					<span style={{fontSize:'70%',color: '#8c96a0'}}>Наименование</span>
          <h3 >{this.state.userForModal.name}</h3>
          <span style={{fontSize:'70%',color: '#8c96a0'}}>Описание</span>
					<div>
            <span>{this.state.userForModal.description}</span>
					</div>
					<div>
						<span style={{fontSize:'70%',color: '#8c96a0'}}>цена</span>
					</div>
					<div>
						<span>{this.state.userForModal.price}</span>
					</div>

					<div>
						<span style={{fontSize:'70%',color: '#8c96a0'}}>Остаток</span>
						<span style={{fontSize:'70%',color: '#8c96a0',marginLeft:'15%'}}>Точка</span>
            <hr/>

					</div>
          {this.state.points.map((item,index)=>{
            return<div>
              <span key={index}>{item.remains}</span>
              <img src={placeIcon} alt="" style={{ width: '7%', marginLeft: '20%' }}/>
              <span style={{fontSize:'70%',color: '#8c96a0'}}>{item.place}</span>
              <hr/>
            </div>
          })

          }
        </div>


      </div>




    </div>
  }


}

export default Products;