import * as React from "react";
import  './style.scss';
import { connect } from "react-redux";
import { Dispatch } from "redux";
// bindActionCreators,
import { RouteComponentProps, Link } from "react-router";
import { RootState } from "app/reducers";


import MainTemplate from '../MainTemplate';

const { TextField, MenuItem, SelectField } = require("material-ui");

//const $ = require("jquery");

// const supportIcon = require("app/../assets/images/support.svg") as string;
const editIcon = require("app/../assets/images/icon_edit.svg") as string;
const editProfileIcon = require("app/../assets/images/icon_profile_edit.svg") as string;
const addPhotoIcon = require("app/../assets/images/icon_add_photo.svg") as string;
const deleteUserIcon = require("app/../assets/images/icon_delete.svg") as string;
const passwdEditIcon = require("app/../assets/images/icon_password_edit.svg") as string;

const officeAvatar = require("app/../assets/images/officeAvatar.jpg") as string;
const searchBlue = require("app/../assets/images/searchBlue.svg") as string;
const adminRoleIcon = require("app/../assets/images/adminRole.svg") as string;

namespace Profile {
	export interface Props extends RouteComponentProps<any, any> {
	}
}

@connect(
	(state: RootState): Pick<Profile.Props, any> => {
		return {};
	},
	(dispatch: Dispatch<any, any>): Pick<Profile.Props, any> => ({})
)

class Profile extends MainTemplate<Profile.Props, any> {
	static defaultProps: Partial<Profile.Props> = {};

	constructor(props: Profile.Props, context?: any) {
		super(props, context);
	}

	state = {
		// openEditWindow:false
		value: 3,
		// 7.2_Partners_User_Edit
		user: {
			firstName: "Дмитрий",
			secondName: "Заруба",
			role: "superAdmin",
			phone: "+7 902 563 50 45",
			email: "dzaruba@gmail.com"
		},
		searchEnableField: false

	};

	openEditProfileWindow() {
		$("#editUserModal").modal({ show: true });
	}

	openChangePasswdProfileWindow() {
		$("#changePasswordModal").modal({ show: true });
	}

	openDeleteUserProfileWindow() {
		$("#deleteUserModal").modal({ show: true });
		// $('#editUserModal').modal({ hide: true });
		$("#editUserModal").modal("hide");
	}

	handleChange = (event: any, index: any, value: any) => this.setState({ value });

	uploadFile() {

	}


	searchEnableFieldMethod() {
		this.setState({ searchEnableField: true });
	}

	onBlurSearchField() {
		this.setState({ searchEnableField: false })
	}

	renderHeader() {
		return <div>
			<div className={'header-top'}>
				<div className={'search-bar'}>
					<img src={searchBlue} className={'search-img'} alt=""
							 {...{ onTouchTap: this.searchEnableFieldMethod.bind(this) }}
					/>
					{
						this.state.searchEnableField && <TextField
							name={'headerSearch'}
							className={'search-text'}
							hintText=""
							placeholder={'Поиск...'}
							fullWidth={true}
							ref="search"
							onBlur={this.onBlurSearchField.bind(this)}
							autoFocus={true}
						/>
					}
				</div>

				<Link to="/profile">
					<img className="avatar" src={officeAvatar} alt=""/>
				</Link>
			</div>

			<div className={'header-middle'} style={{height:55}}>

			</div>

			<div className={'header-bottom'}>
				<h2 className={'title'}> Мой Профиль</h2>
			</div>

		</div>
	}

	renderContent() {
		return <div>
			<div className="row" style={{ flex: 1, background: 'white', height: 'calc(100% - 170px - 50px)' }}>
				<div className='col-lg-12' style={{ minHeight: '100%' }}>
					<div className="row" style={{ marginLeft: "5%", marginTop: "5%" }}>
						<div className="">
							<img className="imageClass" src={officeAvatar} alt="" style={{
								width: "120px",
								height: "120px",
								borderRadius: "50%",
								backgroundSize: "auto 120px"
							}}/>
						</div>
						<div style={{ marginLeft: "5%" }}>
							<div>
								<span style={{color: "#a6b1c0",fontSize: '14px'}}>C возвращением</span>
							</div>
							<div>
								<span className="userName">{`${this.state.user.firstName} ${this.state.user.secondName}`}</span>
							</div>
							<div>
                <img src={adminRoleIcon} style={{width:'3%'}} alt=""/>
								<span>{this.state.user.role}</span>
							</div>
							<div>
								<span className="userPhone">{this.state.user.phone} | {this.state.user.email}</span>
							</div>
							<div className="row">
								<button style={{ borderRadius: "20px", marginTop: "7%" }}
												className="btn btn-success"
												{...{ onTouchTap: this.openEditProfileWindow.bind(this) }}>
									<img src={editIcon} style={{ width: "25px" }} alt=""/>
									Редактировать профиль
								</button>
								<button style={{ borderRadius: "20px", marginTop: "7%", marginLeft: "10px", width: "250px" }}
												className="btn btn-default"
												{...{ onTouchTap: this.openChangePasswdProfileWindow.bind(this) }}
								>
									Изменить пароль
								</button>
							</div>

						</div>
					</div>
				</div>
			</div>


			<div {...{ className: "modal fade modalWindow" }as any} id="editUserModal"
					 tabIndex="-1" role="dialog"
					 aria-labelledby="editUserModalLabel" aria-hidden="true"
					 style={{overflow:'hidden'}}
           >
				<div className="modal-dialog" role="document">
					<div className="modal-content" style={{ borderRadius: "20px" }}>
						<div className="modal-header">
							<img src={editProfileIcon} alt="" style={{ width: '45px', marginTop: '35px' ,marginLeft:'35px'}}/>
							<span className="modal-title fontTitleWindow" id="editUserModalLabel"
							>	Редактировать <br/>данные</span>

							<button type="button" className="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<div className="modal-body" >
							<div>
								<img className="imageClass" src={officeAvatar} alt="" style={{
									width: "80px",
									height: "80px",
									borderRadius: "50%",
									backgroundSize: "auto 80px",
									marginLeft: "32px",
									marginTop: "2%"
								}}/>
								{/*<img src={addPhotoIcon} alt=""*/}
								{/*style={{marginLeft:'-4%',width:'6%',marginTop:'-10%',cursor:'pointer'}}*/}
								{/*onTouchTap={this.changeAvatar.bind(this)}/>*/}
								{/*<FloatingActionButton mini={true} secondary={true}*/}
								{/*style={{marginLeft:'-4%',width:'6%',marginTop:'-10%'}}>*/}
								{/*/!*<img src={addPhotoIcon} style={{width:'25px'}} alt=""/>*!/*/}
								{/*<ContentAdd />*/}
								{/*<input type="file" onChange={this.uploadFile.bind(this)}/>*/}

								{/*</FloatingActionButton>*/}
								<label htmlFor="label123"
											 style={{ marginLeft: "-4%", width: "6%",  cursor: "pointer" }}>
									<img src={addPhotoIcon} style={{ width: "25px",marginTop: "-40px", }} alt=""/>
									<input type="file" id="label123" style={{ display: "none" }} onChange={this.uploadFile.bind(this)}/>
								</label>

								<img src={deleteUserIcon} alt="" style={{ width: '5%', marginLeft: '30%',cursor: "pointer" }}/>
								<span className="deleteUserWindowSpan"
											{...{ onTouchTap: this.openDeleteUserProfileWindow.bind(this) }}>удалить пользователя</span>
							</div>
							<div style={{ marginLeft: "7%", marginRight: "7%", marginTop: "1%" }}>
								<TextField
									hintText=""
									floatingLabelText="Имя Фамилия"
									fullWidth={true}
									ref="name"
								/><br/>

								<SelectField
									floatingLabelText="Роль"
									value={this.state.value}
									onChange={this.handleChange}
									fullWidth={true}
								>
									<MenuItem value={1} primaryText="superadmin"/>
									<MenuItem value={2} primaryText="admin"/>
									<MenuItem value={3} primaryText="user"/>

								</SelectField>

								<TextField
									hintText=""
									floatingLabelText="Телефон"
									fullWidth={true}
									ref="phone"
								/><br/>
								<TextField
									hintText=""
									floatingLabelText="E-mail"
									fullWidth={true}
									ref="email"
								/><br/>
								<button type="button"
                        style={{ borderRadius: '20px', width: '40%',height:'40px', marginLeft: '60%', marginTop: '10%',
                          backgroundColor: '#5788fd',marginBottom:'9%'}} className="btn-xs btn-primary">
                  <span style={{ fontSize: '80%' }}>Сохранить</span>
								</button>
							</div>
						</div>

					</div>
				</div>
			</div>

			<div {...{ className: "modal fade bootstrapWindows modalWindow" } as any} id="changePasswordModal" tabIndex="-1"
					 role="dialog" style={{ marginLeft: '40%', marginTop: '8%' }}
					 aria-labelledby="changePasswordModalLabel" aria-hidden="true">
				<div className="modal-dialog" role="document">
					<div className="modal-content" style={{ borderRadius: "20px" }}>
						<div className="modal-header">
							<img src={passwdEditIcon} alt="" style={{ width: '10%', marginTop: '10px' }}/>
							<h3 className="modal-title" id="changePasswordModalLabel"
									style={{ marginLeft: "5px", fontWeight: 600 ,fontSize: '90%'}}>
								Изменить<br/>пароль</h3>

							<button type="button" className="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<div className="modal-body">
							<div>

							</div>

							<div style={{ marginLeft: "7%", marginRight: "7%", marginTop: "1%" }}>
								<label htmlFor="">Пароль должен состоять из цыфр и букв,</label>
								<label htmlFor="">длинна пароля не менее 6 </label>

								<TextField
									hintText=""
									floatingLabelText="Введите старый пароль"
									fullWidth={true}
									type="password"
									ref="oldPassword"
								/><br/>
								<TextField
									hintText=""
									floatingLabelText="Введите новый пароль"
									fullWidth={true}
									type="password"
									ref="newPassword"
								/><br/>
								<TextField
									hintText=""
									floatingLabelText="Введите пароль еще раз"
									fullWidth={true}
									type="password"
									ref="newPassword"
								/><br/><br/><br/><br/>
                <button type="button"
                        style={{ borderRadius: '20px', width: '40%', marginLeft: '60%', marginTop: '10%' }}
												className="btn-xs btn-primary">
                  <span style={{ fontSize: '80%' }}>Изменить</span>
								</button>
							</div>
						</div>
						{/*<div className="modal-footer">*/}
						{/*<button type="button" style={{borderRadius:'20px',width:'30%'}} className="btn btn-primary">*/}
						{/*Сохранить</button>*/}
						{/*</div>*/}
					</div>
				</div>
			</div>

			<div {...{ className: "modal fade bootstrapWindows modalWindow" }as any} id="deleteUserModal" tabIndex="-1" role="dialog"
					 aria-labelledby="deleteUserModalLabel" aria-hidden="true"  style={{ marginLeft: '40%', marginTop: '8%' }}>
				<div className="modal-dialog" role="document">
					<div className="modal-content" style={{ borderRadius: "20px" }}>
						<div className="modal-header">
							<img src={deleteUserIcon} alt=""style={{ width: '10%', marginTop: '10px' }}/>
							<h3 className="modal-title" id="deleteUserModalLabel"
									style={{ marginLeft: "5px", fontWeight: 600 , fontSize: '90%'}}>
								Удаление<br/>пользователя</h3>

							<button type="button" className="close" data-dismiss="modal" aria-label="Close">
								<span aria-hidden="true">&times;</span>
							</button>
						</div>
						<div className="modal-body">
							<div style={{ marginLeft: "7%", marginRight: "7%", marginTop: "1%" }}>
								<br/><br/>
								<label htmlFor="" style={{ fontSize: "0.7vw" }}>Вы действительно хотите удалить пользователя?</label>
								<br/><br/><br/>
								<div>
									<h3>{this.state.user.firstName} {this.state.user.secondName}</h3><br/>
									<span>{this.state.user.role}</span>
								</div>
								<br/><br/><br/>
                <div style={{ display: 'flex', flexDirection: 'row', justifyContent: 'flex-end' }}>
                  <div className="" style={{ width: '35%' }}>
                    <button type="button" style={{
                      borderRadius: '20px', width: '100%',
                      marginTop: '10%', marginBottom: '2%'
                    }}
                            className="btn-xs btn-secondary">
                      <span>Отмена</span>
                    </button>
                  </div>
                  <div className="" style={{ width: '35%' }}>
                    <button type="button" style={{
                      borderRadius: '20px', marginLeft: '8%', marginTop: '10%'
                      , width: '100%', marginBottom: '2%'
                    }} className="btn-xs btn-danger"
                            >
                      <span>Удалить</span>
                    </button>
                  </div>
                </div>
							</div>

						</div>

					</div>
				</div>
			</div>

		</div>
	}

}

export default Profile;