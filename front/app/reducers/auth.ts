import { RootState } from './state';
import { AuthActions } from 'app/actions/auth';
import { createReducer } from 'redux-act';
import * as _ from 'lodash';
import { getCookie } from 'app/utils';

const initialState: RootState.AuthState = {
	error: {},
	user: {},
	triesLeft: +_.defaultTo(getCookie('triesLeft'), 5),
  userId:0
};

export const authReducer = createReducer(
	{
		['REFRESH_TRIES']: (state, payload) => {
			const newState = _.cloneDeep(state);
			newState.triesLeft = 5;
			//new actions with newState
			console.log('TRIES REFRESHED:=>', newState.triesLeft);
			return newState;
		},

		[AuthActions.login.request]: (state, payload) => {
			return state;
		},
		[AuthActions.login.ok]: (state, payload) => {
			// const { response: { data = [] } = {} } = payload;
			const newState = _.cloneDeep(state);
			newState.triesLeft = 5;
			//new actions with newState

			console.log('OK:=>', newState.triesLeft);
			return newState;
		},
		[AuthActions.login.error]: (state, payload) => {
			const newState = _.cloneDeep(state);
			if (newState.triesLeft > 0) {
				newState.triesLeft--;
			}
			console.log('ERROR:=>', newState.triesLeft);
			return newState;
		},
	},
	initialState
);
