// import { handleActions } from 'redux-actions';
import { RootState } from './state';
// import { PartnersActions } from 'app/actions/partners';
import { BasketActions } from 'app/actions/basket';
import { createReducer } from 'redux-act';
import * as _ from 'lodash';
// import { BasketState } from "app/reducers/state";

const initialState: RootState.BasketState = {
	error: {},
	partners: {
		items: [],
		count: 0
	},
	items: {
		items: [],
		count: 0
	},

};

export const basketReducer = createReducer(
	{
		[BasketActions.getAllBasket.request]: (state, payload) => {
			// const newState = _.cloneDeep(state);
			// console.log(123, payload);
			// //new actions with newState
			return state;
		},
		[BasketActions.getAllBasket.ok]: (state, payload) => {
			const { response: { data: { items = [], count = 0 } = {} } = {} } = payload;
			const newState = _.cloneDeep(state);
			console.log('items', items)
			// newState.items.items = items;
			newState.items.items = [
				{
					id: 1,
					organizationName: 'Hershey',
					typeProfile: 'заявка на вступление в платформу',
          sidePanelType: 'requests',
          date: '25 мая 2018 10:52'
				},
				{
					id: 2,
					organizationName: 'Unilever',
					typeProfile: 'заявка на вступление в платформу',
          sidePanelType: 'requests',
          date: '25 мая 2018 10:52'
				},
				{
					id: 3,
					organizationName: 'Dannon',
					typeProfile: 'заявка на пополнение бонусного счета',
          sidePanelType: 'bonuses',
					date: '25 мая 2018 10:52'
				},
				{
					id: 4,
					organizationName: 'Unilever',
					typeProfile: 'партнер',
					 sidePanelType: 'partner',
					date: '25 мая 2018 10:52'
				},
				{
					id: 5,
					organizationName: 'Kraft',
					typeProfile: 'сотрудник',
          sidePanelType: 'employees',
					date: '25 мая 2018 10:52'
				},
				{
					id: 6,
					organizationName: 'Hershey',
					typeProfile: 'торговая точка',
          sidePanelType: 'points',
          date: '25 мая 2018 10:52'
				},
				{
					id: 7,
					organizationName: 'Hershey',
					typeProfile: 'ERP-система',
          sidePanelType: 'ERP',
					date: '25 мая 2018 10:52'
				},
				{
					id: 8,
					organizationName: 'Unilever',
					typeProfile: 'партнер',
           sidePanelType: 'partner',
					date: '25 мая 2018 10:52'
				},
				{
					id: 9,
					organizationName: 'Hershey',
					typeProfile: 'заявка на вступление в платформу',
          sidePanelType: 'requests',
					date: '25 мая 2018 10:52'
				},
				{
					id: 10,
					organizationName: 'Unilever',
					typeProfile: 'заявка на вступление в платформу',
          sidePanelType: 'requests',
					date: '25 мая 2018 10:52'
				},
			];
			newState.items.count = count;
			//new actions with newState
			return newState;
		},
		[BasketActions.getAllBasket.error]: (state, payload) => {
			return state;
		},


		[BasketActions.fetchPartners.request]: (state, payload) => {
			// const newState = _.cloneDeep(state);
			// console.log(123, payload);
			// //new actions with newState
			return state;
		},
		[BasketActions.fetchPartners.ok]: (state, payload) => {
			const { response: { data: { items = [], count = 0 } = {} } = {} } = payload;
			const newState = _.cloneDeep(state);
			console.log('items', items);
			// newState.partners.items = items;
			newState.partners.items = [
				{
					id: 1,
					organizationName: 'Unilever1',
					typeProfile: 'партнер',
					date: '25 мая 2018 10:52'
				},
				{
					id: 2,
					organizationName: 'Unilever2',
					typeProfile: 'партнер',
					date: '25 мая 2018 10:52'
				},
			];
			newState.partners.count = count;
			//new actions with newState
			return newState;
		},
		[BasketActions.fetchPartners.error]: (state, payload) => {
			return state;
		},


		[BasketActions.deleteAllBasket.request]: (state, payload) => {
			// const newState = _.cloneDeep(state);
			// console.log(123, payload);
			// //new actions with newState
			return state;
		},
		[BasketActions.deleteAllBasket.ok]: (state, payload) => {
			// const { response: { data: { items = [], count = 0 } = {} } = {} } = payload;
			const newState = _.cloneDeep(state);
			// console.log('items', items);
			// newState.partners.items = items;
			newState.items.items = [];
			newState.items.count = 0;
			//new actions with newState
			return newState;
		},
		[BasketActions.fetchPartners.error]: (state, payload) => {
			return state;
		},

	},
	initialState
);
