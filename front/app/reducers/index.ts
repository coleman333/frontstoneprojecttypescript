import { combineReducers } from 'redux';
import { RootState } from './state';
import { partnersReducer } from './partners';
import { basketReducer } from './basket';
import { authReducer } from './auth';
import { routerReducer, RouterState } from 'react-router-redux';

export { RootState, RouterState };

// NOTE: current type definition of Reducer in 'react-router-redux' and 'redux-actions' module
// doesn't go well with redux@4
export const rootReducer = combineReducers<RootState>({
	auth: authReducer as any,
	partners: partnersReducer as any,
	basket: basketReducer as any,
	router: routerReducer as any
});
