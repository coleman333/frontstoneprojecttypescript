// import { handleActions } from 'redux-actions';
import { RootState } from './state';
import { PartnersActions } from 'app/actions/partners';
import { createReducer } from 'redux-act';
import * as _ from 'lodash';

const initialState: RootState.PartnersState = {
	error: {},
	partners: {
		items: [],
		count: 0
	},
	users: {
		items: [],
		count: 0
	},
};

export const partnersReducer = createReducer(
	{
		[PartnersActions.create.request]: (state, payload) => {
			// const newState = _.cloneDeep(state);
			// console.log(123, payload);
			// //new actions with newState
			return state;
		},
		[PartnersActions.create.ok]: (state, payload) => {
			const { response: { data = [] } = {} } = payload;
			const newState = _.cloneDeep(state);
			newState.partners.items = data;
			//new actions with newState
			return newState;
		},
		[PartnersActions.create.error]: (state, payload) => {
			return state;
		},


		[PartnersActions.addNewUser.request]: (state, payload) => {
			// const newState = _.cloneDeep(state);
			// console.log(123, payload);
			// //new actions with newState
			return state;
		},
		[PartnersActions.addNewUser.ok]: (state, payload) => {
			const { response: { data = [] } = {} } = payload;
			const newState = _.cloneDeep(state);
			newState.users.items = data;
			//new actions with newState
			return newState;
		},
		[PartnersActions.addNewUser.error]: (state, payload) => {
			return state;
		},



		[PartnersActions.editNewUser.request]: (state, payload) => {
			// const newState = _.cloneDeep(state);
			// console.log(123, payload);
			// //new actions with newState
			return state;
		},
		[PartnersActions.editNewUser.ok]: (state, payload) => {
			const { response: { data = [] } = {} } = payload;
			const newState = _.cloneDeep(state);
			newState.users.items = data;
			//new actions with newState
			return newState;
		},
		[PartnersActions.editNewUser.error]: (state, payload) => {
			return state;
		},
	},
	initialState
);
