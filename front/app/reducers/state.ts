import { RouterState } from 'react-router-redux';

export interface RootState {
	auth: any;
	partners: any;
	basket:any;
	router: RouterState;
}

export namespace RootState {
	export type PartnersState = any;
	export type BasketState = any;
	export type AuthState = any;
}
