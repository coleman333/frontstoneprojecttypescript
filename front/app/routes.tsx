import * as React from 'react';
import { Route, IndexRedirect, IndexRoute } from 'react-router';
// IndexRoute
import App from './containers/App';
import Registration from './containers/Registration';
import Login from './containers/Login';
import Navigation from './containers/Navigation';
import Profile from './containers/Profile';
import Partners from './containers/Partners';
import Products from './containers/Products';
import DMP from './containers/DMP';
import PartnerBlackMenu from './containers/PartnerBlackMenu';
import PartnersUser from './containers/PartnersUser';
import Verification from "./containers/Verification";
import Administrator from "./containers/Administrator";
import BonusBills from "./containers/BonusBills";
// import Basket from "./containers/Basket";
import Basket from "./containers/Basket2";
import Bills from "./containers/Bill";
import Points from "./containers/Points";
import ERP from "./containers/ERP";


export default (store: any) => {
	// function requireAuth(nextState, replace, callback) {
	// 	const authUser = store.getState().getIn(['auth', 'user']);
	// 	if (authUser.isEmpty()) {
	// 		replace({
	// 			pathname: '/login',
	// 			// state:{nextPathname:nextState.location.pathname}
	// 		});
	// 	}
	// 	callback();
	// }
	//
	// function redirectUser(nextState, replace, callback) {
	// 	const authUser = store.getState().getIn(['auth', 'user']);
	// 	if (!authUser.isEmpty()) {
	// 		replace({
	// 			pathname: '/'
	// 		});
	// 	}
	// 	callback();
	// }

	// onEnter={requireAuth}

	// onEnter={redirectUser}
	return (
		<Route>
			<Route path="/" component={App}>
				<IndexRedirect to="profile"/>
				<Route component={Navigation}>
					<Route path="profile" component={Profile}/>
					<Route path="dmp" component={DMP}/>
					<Route path="administrator" component={Administrator}/>
					<Route path="bonus-bills" component={BonusBills}/>
					<Route path="basket" component={Basket}/>


					<Route path="partners">
						<IndexRoute component={Partners}/>
						<Route path=":id" component={PartnerBlackMenu}>
							{/*<IndexRedirect to="users"/>*/}
							<Route path="users-partners" component={PartnersUser}/>
							<Route path="products" component={Products}/>
              <Route path="points" component={Points}/>
							<Route path="bills" component={Bills}/>
							<Route path="erp" component={ERP}/>

						</Route>
					</Route>
				</Route>

				<Route>
					<Route path="registration" component={Registration}/>
					<Route path="login" component={Login}/>
					<Route path="verification" component={Verification}/>
				</Route>
			</Route>
		</Route>

	)
}

