import * as React from 'react';
import * as ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { Router, browserHistory } from 'react-router';
import { createBrowserHistory } from 'history';
import { configureStore } from 'app/store';
import createRoutes from 'app/routes';
import * as injectTapEventPlugin from 'react-tap-event-plugin';
import './assets/styles/main.scss';

injectTapEventPlugin();

// prepare store
const history = createBrowserHistory();
const store = configureStore(history);
const routes = createRoutes(store);

ReactDOM.render(
	<Provider store={store}>
		<Router history={browserHistory}>
			{routes}
		</Router>
	</Provider>,
	document.getElementById('root')
);
